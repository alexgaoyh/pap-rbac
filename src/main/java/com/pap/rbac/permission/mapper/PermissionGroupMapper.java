package com.pap.rbac.permission.mapper;

import com.pap.base.mapper.PapBaseMapper;
import com.pap.rbac.dto.PermissionGroupTreeNodeDTO;
import com.pap.rbac.permission.entity.PermissionGroup;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

;

@Mapper
public interface PermissionGroupMapper extends PapBaseMapper<PermissionGroup> {
    int deleteByPrimaryKey(String permissionGroupId);

    int selectCountByMap(Map<Object, Object> map);

    List<PermissionGroup> selectListByMap(Map<Object, Object> map);

    PermissionGroup selectByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int deleteByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int insert(PermissionGroup record);

    int insertSelective(PermissionGroup record);

    PermissionGroup selectByPrimaryKey(String permissionGroupId);

    int updateByPrimaryKeySelective(PermissionGroup record);

    int updateByPrimaryKey(PermissionGroup record);

    // add alexgaoyh
    List<PermissionGroupTreeNodeDTO> permissionGroupTreeJson(@Param("clientLicenseId") String clientLicenseId,
                                                  @Param("globalParentId") String globalParentId);
}