package com.pap.rbac.permission.mapper;

import com.pap.base.mapper.PapBaseMapper;
import com.pap.rbac.permission.entity.PermissionGroupPermissionRel;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

;

@Mapper
public interface PermissionGroupPermissionRelMapper extends PapBaseMapper<PermissionGroupPermissionRel> {
    int deleteByPrimaryKey(String permissionGroupPermissionRelId);

    int selectCountByMap(Map<Object, Object> map);

    List<PermissionGroupPermissionRel> selectListByMap(Map<Object, Object> map);

    PermissionGroupPermissionRel selectByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int deleteByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int insert(PermissionGroupPermissionRel record);

    int insertSelective(PermissionGroupPermissionRel record);

    PermissionGroupPermissionRel selectByPrimaryKey(String permissionGroupPermissionRelId);

    int updateByPrimaryKeySelective(PermissionGroupPermissionRel record);

    int updateByPrimaryKey(PermissionGroupPermissionRel record);


    // alexgaoyh

    int deleteByPermissionGroupId(@Param("permissionGroupId") String permissionGroupId);

    /**
     * 根据权限组编号，查询包含哪些权限编号
     * @param permissionGroupId    权限组编号
     * @return
     */
    List<String> selectPermissionIdsByPermissionGroupId(@Param("permissionGroupId") String permissionGroupId);
}