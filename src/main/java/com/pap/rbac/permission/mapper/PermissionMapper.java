package com.pap.rbac.permission.mapper;

import com.pap.base.mapper.PapBaseMapper;
import com.pap.rbac.dto.PermissionResourceUrlDTO;
import com.pap.rbac.permission.entity.Permission;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;;

@Mapper
public interface PermissionMapper extends PapBaseMapper<Permission> {
    int deleteByPrimaryKey(String permissionId);

    int selectCountByMap(Map<Object, Object> map);

    List<Permission> selectListByMap(Map<Object, Object> map);

    Permission selectByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int deleteByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int insert(Permission record);

    int insertSelective(Permission record);

    Permission selectByPrimaryKey(String permissionId);

    int updateByPrimaryKeySelective(Permission record);

    int updateByPrimaryKey(Permission record);

    // alexgaoyh

    List<PermissionResourceUrlDTO> selectWithResourceUrlListByMap(Map<Object, Object> map);
}