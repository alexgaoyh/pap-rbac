package com.pap.rbac.permission.mapper;

import com.pap.base.mapper.PapBaseMapper;
import com.pap.rbac.permission.entity.ResourceUrl;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;;

@Mapper
public interface ResourceUrlMapper extends PapBaseMapper<ResourceUrl> {
    int deleteByPrimaryKey(String resourceUrlId);

    int selectCountByMap(Map<Object, Object> map);

    List<ResourceUrl> selectListByMap(Map<Object, Object> map);

    ResourceUrl selectByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int deleteByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int insert(ResourceUrl record);

    int insertSelective(ResourceUrl record);

    ResourceUrl selectByPrimaryKey(String resourceUrlId);

    int updateByPrimaryKeySelective(ResourceUrl record);

    int updateByPrimaryKey(ResourceUrl record);
}