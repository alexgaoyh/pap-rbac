package com.pap.rbac.permission.service;

import com.pap.rbac.dto.PermissionResourceUrlDTO;
import com.pap.rbac.permission.entity.Permission;

import java.util.List;
import java.util.Map;

/**
 * @Auther: alexgaoyh
 * @Date: 2019/2/14 14:51
 * @Description:
 */
public interface IPermissionResourceUrlService {


    int deleteByPrimaryKey(String id);

    int selectCountByMap(Map<Object, Object> map);

    List<PermissionResourceUrlDTO> selectListByMap(Map<Object, Object> map);

    int insert(PermissionResourceUrlDTO record);

    int insertSelective(PermissionResourceUrlDTO record);

    PermissionResourceUrlDTO selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(PermissionResourceUrlDTO record);

    int updateByPrimaryKey(PermissionResourceUrlDTO record);

}
