package com.pap.rbac.permission.service;

import com.pap.obj.vo.response.ResponseVO;
import com.pap.rbac.dto.PermissionGroupTreeNodeDTO;
import com.pap.rbac.permission.entity.PermissionGroup;

import java.util.List;
import java.util.Map;

public interface IPermissionGroupService {

    int deleteByPrimaryKey(String id);

    int selectCountByMap(Map<Object, Object> map);

    List<PermissionGroup> selectListByMap(Map<Object, Object> map);

    int insert(PermissionGroup record);

    int insertSelective(PermissionGroup record);

    PermissionGroup selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(PermissionGroup record);

    int updateByPrimaryKey(PermissionGroup record);


    /**
     * 权限组树形结构，根据 license 获取
     * @param clientLicenseId
     * @param globalParentId 全局统一的默认顶层组织架构的编码为 -1
     */
    List<PermissionGroupTreeNodeDTO> permissionGroupTreeJson(String clientLicenseId, String globalParentId);

    /**
     * 检查当前操作的权限组编码，对照新维护的上级权限组依赖，是否被循环依赖
     * @param operaPermissionGroupId
     * @param newParentPermissionGroupId
     * @return
     */
    Boolean checkTreeCircularDependency(String operaPermissionGroupId, String newParentPermissionGroupId);

    /**
     * 权限组更新操作,参数校验并且进行数据维护	pathIds pathCodes pathNames 三个字段的数据维护
     * @return
     */
    ResponseVO<PermissionGroup> updateAndCheckPermissionGroup(PermissionGroup inputPermissionGroup);

    /**
     * 维护当前权限组节点下，包含的子集的权限组的数据值,冗余字段的维护，
     * pathIds pathCodes pathNames
     * 请确保当前操作的 inputPermissionGroupId，对应的实体数据已经是最新的。
     *
     * 递归调用，维护完毕所有子类的path冗余字段
     * @param inputPermissionGroupId
     * @return
     */
    ResponseVO<PermissionGroup> updateRecursionPathColumnPermissionGroup(String inputPermissionGroupId);
}
