package com.pap.rbac.permission.service.impl;

import com.pap.rbac.dto.PermissionResourceUrlDTO;
import com.pap.rbac.permission.entity.Permission;
import com.pap.rbac.permission.entity.ResourceUrl;
import com.pap.rbac.permission.mapper.PermissionMapper;
import com.pap.rbac.permission.mapper.ResourceUrlMapper;
import com.pap.rbac.permission.service.IPermissionResourceUrlService;
import com.pap.rbac.permission.service.IPermissionService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Transactional
@Service("permissionResourceUrlService")
public class PermissionResourceUrlServiceImpl implements IPermissionResourceUrlService {

    @Resource(name = "permissionMapper")
    private PermissionMapper permissionMapper;

    @Resource(name = "resourceUrlMapper")
    private ResourceUrlMapper resourceUrlMapper;


    @Override
    public int deleteByPrimaryKey(String id) {
        permissionMapper.deleteByPrimaryKey(id);
        resourceUrlMapper.deleteByPrimaryKey(id);
        return 1;
    }

    @Override
    public int selectCountByMap(Map<Object, Object> map) {
        map.put("dynamicTableName", null);
        return permissionMapper.selectCountByMap(map);
    }

    @Override
    public List<PermissionResourceUrlDTO> selectListByMap(Map<Object, Object> map) {

        List<PermissionResourceUrlDTO> returnList = permissionMapper.selectWithResourceUrlListByMap(map);
        return returnList;
    }

    @Override
    public int insert(PermissionResourceUrlDTO record) {
        Permission permission = new Permission();
        ResourceUrl resourceUrl = new ResourceUrl();

        permission.setPermissionId(record.getPermissionId());
        permission.setPermissionCode(record.getPermissionCode());
        permission.setPermissionName(record.getPermissionName());
        permission.setRemark(record.getRemark());

        resourceUrl.setBlackUrl(record.getBlackUrl());
        resourceUrl.setHttpMethod(record.getHttpMethod());
        resourceUrl.setOperationId(record.getOperationId());
        resourceUrl.setRemark(record.getRemark());
        resourceUrl.setRequestUrl(record.getRequestUrl());
        resourceUrl.setResourceUrlId(record.getResourceUrlId());

        permissionMapper.insert(permission);
        resourceUrlMapper.insert(resourceUrl);
        return 1;
    }

    @Override
    public int insertSelective(PermissionResourceUrlDTO record) {
        Permission permission = new Permission();
        ResourceUrl resourceUrl = new ResourceUrl();

        permission.setPermissionId(record.getPermissionId());
        permission.setPermissionCode(record.getPermissionCode());
        permission.setPermissionName(record.getPermissionName());
        permission.setRemark(record.getRemark());

        resourceUrl.setBlackUrl(record.getBlackUrl());
        resourceUrl.setHttpMethod(record.getHttpMethod());
        resourceUrl.setOperationId(record.getOperationId());
        resourceUrl.setRemark(record.getRemark());
        resourceUrl.setRequestUrl(record.getRequestUrl());
        resourceUrl.setPermissionId(record.getPermissionId());
        resourceUrl.setResourceUrlId(record.getPermissionId());

        permissionMapper.insertSelective(permission);
        resourceUrlMapper.insertSelective(resourceUrl);
        return 1;
    }

    @Override
    public PermissionResourceUrlDTO selectByPrimaryKey(String id) {
        Permission permission = permissionMapper.selectByPrimaryKey(id);
        ResourceUrl resourceUrl = resourceUrlMapper.selectByPrimaryKey(id);

        PermissionResourceUrlDTO permissionResourceUrlDTO = new PermissionResourceUrlDTO();
        permissionResourceUrlDTO.setPermissionId(permission.getPermissionId());
        permissionResourceUrlDTO.setPermissionCode(permission.getPermissionCode());
        permissionResourceUrlDTO.setPermissionName(permission.getPermissionName());
        permissionResourceUrlDTO.setBlackUrl(resourceUrl.getBlackUrl());
        permissionResourceUrlDTO.setHttpMethod(resourceUrl.getHttpMethod());
        permissionResourceUrlDTO.setOperationId(resourceUrl.getOperationId());
        permissionResourceUrlDTO.setRemark(permission.getRemark());
        permissionResourceUrlDTO.setRequestUrl(resourceUrl.getRequestUrl());
        permissionResourceUrlDTO.setResourceUrlId(resourceUrl.getResourceUrlId());

        return permissionResourceUrlDTO;
    }

    @Override
    public int updateByPrimaryKeySelective(PermissionResourceUrlDTO record) {
        Permission permission = new Permission();
        ResourceUrl resourceUrl = new ResourceUrl();

        permission.setPermissionId(record.getPermissionId());
        permission.setPermissionCode(record.getPermissionCode());
        permission.setPermissionName(record.getPermissionName());
        permission.setRemark(record.getRemark());

        resourceUrl.setBlackUrl(record.getBlackUrl());
        resourceUrl.setHttpMethod(record.getHttpMethod());
        resourceUrl.setOperationId(record.getOperationId());
        resourceUrl.setRemark(record.getRemark());
        resourceUrl.setRequestUrl(record.getRequestUrl());
        resourceUrl.setResourceUrlId(record.getResourceUrlId());

        permissionMapper.updateByPrimaryKeySelective(permission);
        resourceUrlMapper.updateByPrimaryKeySelective(resourceUrl);
        return 1;
    }

    @Override
    public int updateByPrimaryKey(PermissionResourceUrlDTO record) {
        Permission permission = new Permission();
        ResourceUrl resourceUrl = new ResourceUrl();

        permission.setPermissionId(record.getPermissionId());
        permission.setPermissionCode(record.getPermissionCode());
        permission.setPermissionName(record.getPermissionName());
        permission.setRemark(record.getRemark());

        resourceUrl.setBlackUrl(record.getBlackUrl());
        resourceUrl.setHttpMethod(record.getHttpMethod());
        resourceUrl.setOperationId(record.getOperationId());
        resourceUrl.setRemark(record.getRemark());
        resourceUrl.setRequestUrl(record.getRequestUrl());
        resourceUrl.setResourceUrlId(record.getResourceUrlId());

        permissionMapper.updateByPrimaryKey(permission);
        resourceUrlMapper.updateByPrimaryKey(resourceUrl);
        return 1;
    }
}
