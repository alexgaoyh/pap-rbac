package com.pap.rbac.permission.service;

import com.pap.rbac.permission.entity.ResourceUrl;

import java.util.List;
import java.util.Map;

@Deprecated
public interface IResourceUrlService {

    int deleteByPrimaryKey(String id);

    int selectCountByMap(Map<Object, Object> map);

    List<ResourceUrl> selectListByMap(Map<Object, Object> map);

    int insert(ResourceUrl record);

    int insertSelective(ResourceUrl record);

    ResourceUrl selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(ResourceUrl record);

    int updateByPrimaryKey(ResourceUrl record);
}
