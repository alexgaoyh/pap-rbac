package com.pap.rbac.permission.service.impl;

import com.pap.rbac.permission.mapper.PermissionMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pap.rbac.permission.entity.Permission;
import com.pap.rbac.permission.service.IPermissionService;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Transactional
@Service("permissionService")
@Deprecated
public class PermissionServiceImpl implements IPermissionService {

    @Resource(name = "permissionMapper")
    private PermissionMapper mapper;

    @Override
    public int deleteByPrimaryKey(String id) {
       return mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int selectCountByMap(Map<Object, Object> map) {
      return mapper.selectCountByMap(map);
    }

    @Override
    public List<Permission> selectListByMap(Map<Object, Object> map) {
        return mapper.selectListByMap(map);
    }

    @Override
    public int insert(Permission record) {
       return mapper.insert(record);
    }

    @Override
    public int insertSelective(Permission record) {
       return mapper.insertSelective(record);
    }

    @Override
    public Permission selectByPrimaryKey(String id) {
       return mapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(Permission record) {
       return mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Permission record) {
      return mapper.updateByPrimaryKey(record);
    }
}