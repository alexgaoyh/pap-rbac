package com.pap.rbac.permission.service.impl;

import com.pap.base.util.string.StringUtilss;
import com.pap.obj.vo.response.ResponseVO;
import com.pap.rbac.dto.PermissionGroupTreeNodeDTO;
import com.pap.rbac.permission.entity.PermissionGroup;
import com.pap.rbac.permission.mapper.PermissionGroupMapper;
import com.pap.rbac.permission.service.IPermissionGroupService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Transactional
@Service("permissionGroupService")
public class PermissionGroupServiceImpl implements IPermissionGroupService {

    @Resource(name = "permissionGroupMapper")
    private PermissionGroupMapper mapper;

    @Override
    public int deleteByPrimaryKey(String id) {
       return mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int selectCountByMap(Map<Object, Object> map) {
      return mapper.selectCountByMap(map);
    }

    @Override
    public List<PermissionGroup> selectListByMap(Map<Object, Object> map) {
        return mapper.selectListByMap(map);
    }

    @Override
    public int insert(PermissionGroup record) {
       return mapper.insert(record);
    }

    @Override
    public int insertSelective(PermissionGroup record) {
       return mapper.insertSelective(record);
    }

    @Override
    public PermissionGroup selectByPrimaryKey(String id) {
       return mapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(PermissionGroup record) {
       return mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(PermissionGroup record) {
      return mapper.updateByPrimaryKey(record);
    }

    @Override
    public List<PermissionGroupTreeNodeDTO> permissionGroupTreeJson(String clientLicenseId, String globalParentId) {
        return mapper.permissionGroupTreeJson(clientLicenseId, globalParentId);
    }

    @Override
    public Boolean checkTreeCircularDependency(String operaPermissionGroupId, String newParentPermissionGroupId) {
        String parentRoleTemp = "";
        PermissionGroup newParentPermissionGroupInfo = mapper.selectByPrimaryKey(newParentPermissionGroupId);
        if(newParentPermissionGroupInfo != null) {
            if(StringUtilss.isNotEmpty(newParentPermissionGroupInfo.getPathIds())) {
                parentRoleTemp = newParentPermissionGroupInfo.getPathIds() + ",";
            }
            parentRoleTemp += newParentPermissionGroupInfo.getPermissionGroupId();
        }
        if (StringUtilss.checkArrayValue(parentRoleTemp.split(","), operaPermissionGroupId)) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public ResponseVO<PermissionGroup> updateAndCheckPermissionGroup(PermissionGroup inputPermissionGroup) {
// 检验是否循环依赖
        if(checkTreeCircularDependency(inputPermissionGroup.getPermissionGroupId(), inputPermissionGroup.getPermissionGroupParentId())) {

            // 忽略前台传递过来的数据
            inputPermissionGroup.setPathIds(null);
            inputPermissionGroup.setPathCodes(null);
            inputPermissionGroup.setPathNames(null);
            mapper.updateByPrimaryKeySelective(inputPermissionGroup);

            PermissionGroup tempForPath = mapper.selectByPrimaryKey(inputPermissionGroup.getPermissionGroupId());
            if("-1".equals(inputPermissionGroup.getPermissionGroupParentId())) {
                tempForPath.setPathCodes(null);
                tempForPath.setPathIds(null);
                tempForPath.setPathNames(null);
                mapper.updateByPrimaryKey(tempForPath);
            } else {
                PermissionGroup tempParentForPath = mapper.selectByPrimaryKey(inputPermissionGroup.getPermissionGroupParentId());
                if(tempParentForPath != null) {
                    if(StringUtilss.isNotEmpty(tempParentForPath.getPathIds())) {
                        tempForPath.setPathIds(tempParentForPath.getPathIds() + "," + tempParentForPath.getPermissionGroupId());
                    } else {
                        tempForPath.setPathIds(tempParentForPath.getPermissionGroupId());
                    }
                    if(StringUtilss.isNotEmpty(tempParentForPath.getPathCodes())) {
                        tempForPath.setPathCodes(tempParentForPath.getPathCodes() + "," + tempParentForPath.getPermissionGroupCode());
                    } else {
                        tempForPath.setPathCodes(tempParentForPath.getPermissionGroupCode());
                    }
                    if(StringUtilss.isNotEmpty(tempParentForPath.getPathNames())) {
                        tempForPath.setPathNames(tempParentForPath.getPathNames() + "," + tempParentForPath.getPermissionGroupName());
                    } else {
                        tempForPath.setPathNames(tempParentForPath.getPermissionGroupName());
                    }
                } else {
                    tempForPath.setPermissionGroupParentId("-1");
                    tempForPath.setPathCodes(null);
                    tempForPath.setPathIds(null);
                    tempForPath.setPathNames(null);
                }

                mapper.updateByPrimaryKey(tempForPath);
            }

            // 维护子集的角色
            ResponseVO responseVO =  updateRecursionPathColumnPermissionGroup(inputPermissionGroup.getPermissionGroupId());
            return ResponseVO.successdata(tempForPath);
        } else {
            return ResponseVO.validfail("角色被循环依赖，请检查数据有效性!");
        }
    }

    @Override
    public ResponseVO<PermissionGroup> updateRecursionPathColumnPermissionGroup(String inputPermissionGroupId) {
        try {
            Map<Object, Object> parentIdMap = new HashMap<Object, Object>();
            parentIdMap.put("permissionGroupParentId", inputPermissionGroupId);
            List<PermissionGroup> list = mapper.selectListByMap(parentIdMap);
            if (null != list && list.size()>0) {
                for (int i = 0; i < list.size(); i++) {
                    PermissionGroup permissionGroup = list.get(i);
                    // 更新数据
                    PermissionGroup parentTemp = mapper.selectByPrimaryKey(permissionGroup.getPermissionGroupParentId());
                    if(parentTemp != null) {
                        if(StringUtilss.isNotEmpty(parentTemp.getPathIds())) {
                            permissionGroup.setPathIds(parentTemp.getPathIds() + "," + parentTemp.getPermissionGroupId());
                        } else {
                            permissionGroup.setPathIds(parentTemp.getPermissionGroupId());
                        }
                        if(StringUtilss.isNotEmpty(parentTemp.getPathCodes())) {
                            permissionGroup.setPathCodes(parentTemp.getPathCodes() + "," + parentTemp.getPermissionGroupCode());
                        } else {
                            permissionGroup.setPathCodes(parentTemp.getPermissionGroupCode());
                        }
                        if(StringUtilss.isNotEmpty(parentTemp.getPathNames())) {
                            permissionGroup.setPathNames(parentTemp.getPathNames() + "," + parentTemp.getPermissionGroupName());
                        } else {
                            permissionGroup.setPathNames(parentTemp.getPermissionGroupName());
                        }
                    } else {
                        permissionGroup.setPathCodes(null);
                        permissionGroup.setPathIds(null);
                        permissionGroup.setPathNames(null);
                    }
                    mapper.updateByPrimaryKeySelective(permissionGroup);
                    updateRecursionPathColumnPermissionGroup(permissionGroup.getPermissionGroupId());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseVO.validfail(e.getMessage());
        }
        return ResponseVO.successdata("更新成功");
    }
}