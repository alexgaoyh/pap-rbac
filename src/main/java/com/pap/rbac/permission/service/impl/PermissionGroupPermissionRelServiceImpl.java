package com.pap.rbac.permission.service.impl;

import com.pap.base.util.bean.BeanCopyUtilss;
import com.pap.base.util.date.DateUtils;
import com.pap.obj.vo.logineduser.LoginedUserVO;
import com.pap.rbac.dto.PermissionDTO;
import com.pap.rbac.dto.PermissionGroupPermissionRelDTO;
import com.pap.rbac.permission.entity.Permission;
import com.pap.rbac.permission.entity.PermissionGroupPermissionRel;
import com.pap.rbac.permission.mapper.PermissionGroupPermissionRelMapper;
import com.pap.rbac.permission.mapper.PermissionMapper;
import com.pap.rbac.permission.service.IPermissionGroupPermissionRelService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Transactional
@Service("permissionGroupPermissionRelService")
public class PermissionGroupPermissionRelServiceImpl implements IPermissionGroupPermissionRelService {

    @Resource(name = "permissionGroupPermissionRelMapper")
    private PermissionGroupPermissionRelMapper mapper;

    @Resource(name = "permissionMapper")
    private PermissionMapper permissionMapper;

    @Override
    public int deleteByPrimaryKey(String id) {
        return mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int selectCountByMap(Map<Object, Object> map) {
        return mapper.selectCountByMap(map);
    }

    @Override
    public List<PermissionGroupPermissionRel> selectListByMap(Map<Object, Object> map) {
        return mapper.selectListByMap(map);
    }

    @Override
    public int insert(PermissionGroupPermissionRel record) {
        return mapper.insert(record);
    }

    @Override
    public int insertSelective(PermissionGroupPermissionRel record) {
        return mapper.insertSelective(record);
    }

    @Override
    public PermissionGroupPermissionRel selectByPrimaryKey(String id) {
        return mapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(PermissionGroupPermissionRel record) {
        return mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(PermissionGroupPermissionRel record) {
        return mapper.updateByPrimaryKey(record);
    }

    @Override
    public void updatePermissionGroupPermissionRel(LoginedUserVO loginedUserVO, String permissionGroupId, List<String> permissionIdsList) {
        if(permissionIdsList != null) {
            // 删除已被删除的明细数据: dto中无明细数据，数据库中有明细数据，删除
            Map<Object, Object> operSelectOrganizationMap = new HashMap<Object, Object>();
            operSelectOrganizationMap.put("permissionGroupId", permissionGroupId);
            List<PermissionGroupPermissionRel> dbRelList = mapper.selectListByMap(operSelectOrganizationMap);
            if(dbRelList != null && dbRelList.size() > 0) {
                for(PermissionGroupPermissionRel dbRel : dbRelList) {
                    boolean existBool = false;
                    for(String permissionIdTemp: permissionIdsList) {
                        if(dbRel.getPermissionId().equals(permissionIdTemp)) {
                            existBool = true;
                        }
                    }
                    // existBool = true 说明  数据未被删除
                    if(existBool == false) {
                        mapper.deleteByPrimaryKey(dbRel.getPermissionGroupPermissionRelId());
                    }

                }
            }

            for (String permissionIdTemp : permissionIdsList) {
                Map<Object, Object> operSelectResourceOperationMap = new HashMap<Object, Object>();
                operSelectResourceOperationMap.put("permissionGroupId", permissionGroupId);
                operSelectResourceOperationMap.put("permissionId", permissionIdTemp);
                operSelectResourceOperationMap.put("clientLicenseId", loginedUserVO != null ? loginedUserVO.getClientLicenseId() : "");
                List<PermissionGroupPermissionRel> permissionGroupPermissionRelList = mapper.selectListByMap(operSelectResourceOperationMap);
                if(permissionGroupPermissionRelList == null || permissionGroupPermissionRelList.size() == 0) {

                    PermissionGroupPermissionRel permissionGroupPermissionRel = new PermissionGroupPermissionRel();
                    permissionGroupPermissionRel.setPermissionGroupPermissionRelId(new StringBuffer(permissionGroupId).append(":")
                            .append(permissionIdTemp).toString());
                    permissionGroupPermissionRel.setPermissionGroupId(permissionGroupId);
                    permissionGroupPermissionRel.setPermissionId(permissionIdTemp);
                    permissionGroupPermissionRel.setCreateIp("0.0.0.0");
                    permissionGroupPermissionRel.setCreateTime(DateUtils.getCurrDateTimeStr());
                    permissionGroupPermissionRel.setCreateUser(loginedUserVO != null ? loginedUserVO.getId() : "");
                    permissionGroupPermissionRel.setClientLicenseId(loginedUserVO != null ? loginedUserVO.getClientLicenseId() : "");
                    mapper.insertSelective(permissionGroupPermissionRel);
                }
            }
        }
    }

    @Override
    public List<PermissionGroupPermissionRelDTO> selectPermissionIdsByPermissionGroupId(String permissionGroupId) {
        Map<Object, Object> operSelectResourceMap = new HashMap<Object, Object>();
        operSelectResourceMap.put("permissionGroupId", permissionGroupId);
        List<PermissionGroupPermissionRel> dbRelList = mapper.selectListByMap(operSelectResourceMap);

        List<PermissionGroupPermissionRelDTO> dtoRelList = toDTO(dbRelList);
        return dtoRelList;
    }

    @Override
    public PermissionDTO selectDTOByPermissionId(String permissionId) {
        Permission permission = permissionMapper.selectByPrimaryKey(permissionId);
        PermissionDTO dtoTemp = new PermissionDTO();
        BeanCopyUtilss.myCopyProperties(permission, dtoTemp);
        return dtoTemp;
    }

    private List<PermissionGroupPermissionRelDTO> toDTO(List<PermissionGroupPermissionRel> databaseList) {
        List<PermissionGroupPermissionRelDTO> returnList = new ArrayList<PermissionGroupPermissionRelDTO>();
        if(databaseList != null) {
            for(PermissionGroupPermissionRel dbEntity : databaseList) {
                PermissionGroupPermissionRelDTO dtoTemp = new PermissionGroupPermissionRelDTO();
                BeanCopyUtilss.myCopyProperties(dbEntity, dtoTemp);
                returnList.add(dtoTemp);
            }
        }
        return returnList;
    }
}