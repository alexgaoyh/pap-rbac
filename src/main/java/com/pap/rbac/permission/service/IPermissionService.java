package com.pap.rbac.permission.service;

import com.pap.rbac.permission.entity.Permission;

import java.util.List;
import java.util.Map;

@Deprecated
public interface IPermissionService {

    int deleteByPrimaryKey(String id);

    int selectCountByMap(Map<Object, Object> map);

    List<Permission> selectListByMap(Map<Object, Object> map);

    int insert(Permission record);

    int insertSelective(Permission record);

    Permission selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(Permission record);

    int updateByPrimaryKey(Permission record);
}
