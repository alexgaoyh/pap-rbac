package com.pap.rbac.permission.service;

import com.pap.obj.vo.logineduser.LoginedUserVO;
import com.pap.rbac.dto.PermissionDTO;
import com.pap.rbac.dto.PermissionGroupPermissionRelDTO;
import com.pap.rbac.permission.entity.PermissionGroupPermissionRel;

import java.util.List;
import java.util.Map;

public interface IPermissionGroupPermissionRelService {

    int deleteByPrimaryKey(String id);

    int selectCountByMap(Map<Object, Object> map);

    List<PermissionGroupPermissionRel> selectListByMap(Map<Object, Object> map);

    int insert(PermissionGroupPermissionRel record);

    int insertSelective(PermissionGroupPermissionRel record);

    PermissionGroupPermissionRel selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(PermissionGroupPermissionRel record);

    int updateByPrimaryKey(PermissionGroupPermissionRel record);


    // alexgaoyh added

    /**
     * 根据权限组编号,更新最新的权限集合
     * @param loginedUserVO
     * @param permissionGroupId
     * @param permissionIdsList
     */
    void updatePermissionGroupPermissionRel(LoginedUserVO loginedUserVO, String permissionGroupId, List<String> permissionIdsList);

    /**
     * 根据权限组 permissionGroupId ,将包含的权限集合进行返回
     * @param permissionGroupId
     * @return
     */
    List<PermissionGroupPermissionRelDTO> selectPermissionIdsByPermissionGroupId(String permissionGroupId);


    PermissionDTO selectDTOByPermissionId(String permissionId);

}
