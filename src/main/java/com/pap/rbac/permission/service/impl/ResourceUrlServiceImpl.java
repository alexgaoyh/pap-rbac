package com.pap.rbac.permission.service.impl;

import com.pap.rbac.permission.mapper.ResourceUrlMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pap.rbac.permission.entity.ResourceUrl;
import com.pap.rbac.permission.service.IResourceUrlService;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Transactional
@Service("resourceUrlService")
@Deprecated
public class ResourceUrlServiceImpl implements IResourceUrlService {

    @Resource(name = "resourceUrlMapper")
    private ResourceUrlMapper mapper;

    @Override
    public int deleteByPrimaryKey(String id) {
       return mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int selectCountByMap(Map<Object, Object> map) {
      return mapper.selectCountByMap(map);
    }

    @Override
    public List<ResourceUrl> selectListByMap(Map<Object, Object> map) {
        return mapper.selectListByMap(map);
    }

    @Override
    public int insert(ResourceUrl record) {
       return mapper.insert(record);
    }

    @Override
    public int insertSelective(ResourceUrl record) {
       return mapper.insertSelective(record);
    }

    @Override
    public ResourceUrl selectByPrimaryKey(String id) {
       return mapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(ResourceUrl record) {
       return mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(ResourceUrl record) {
      return mapper.updateByPrimaryKey(record);
    }
}