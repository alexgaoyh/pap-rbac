package com.pap.rbac.permission.controller;

import com.pap.base.util.bean.BeanCopyUtilss;
import com.pap.beans.idworker.IdWorker;
import com.pap.obj.vo.logineduser.LoginedUser;
import com.pap.obj.vo.logineduser.LoginedUserVO;
import com.pap.obj.vo.page.Page;
import com.pap.obj.vo.response.ResponseVO;
import com.pap.rbac.dto.PermissionDTO;
import com.pap.rbac.permission.entity.Permission;
import com.pap.rbac.permission.service.IPermissionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * 权限对象管理进行额外处理，再维护过程中需要拆分为多张表分别进行存储
 * 原因： 权限对象仅包含权限编码与对应的权限名称和备注， 所有额外的解释说明存放到资源对象中
 *
 * 形如一个 HTTP接口， 额外的说明存放到 t_rbac_resource_url 中，
 * 用户仅需要绑定到权限编码即可，对权限编码的解释说明需要额外请求资源。
 */
@RestController
@RequestMapping("/permission")
@Api(value = "权限管理", tags = "权限管理", description="权限管理")
@Deprecated
public class PermissionController {

	private static Logger logger  =  LoggerFactory.getLogger(PermissionController.class);
	
	@Resource(name = "permissionService")
	private IPermissionService permissionService;

	@Resource(name = "idWorker")
	private IdWorker idWorker;

	@ApiOperation("创建")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "permissionDTO", value = "", required = true, dataType = "PermissionDTO", paramType="body")
	})
	@PostMapping(value = "")
	public ResponseVO<PermissionDTO> create(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
											@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
											@RequestBody PermissionDTO permissionDTO,
											@LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){
		String idStr = idWorker.nextIdStr();
		// TODO 设置主键
		permissionDTO.setPermissionId(idStr);

		Permission databaseObj = new Permission();
		BeanCopyUtilss.myCopyProperties(permissionDTO, databaseObj, loginedUserVO);

		int i = permissionService.insertSelective(databaseObj);

		return ResponseVO.successdata(permissionDTO);
	}

	@ApiOperation("更新")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "permissionDTO", value = "", required = true, dataType = "PermissionDTO", paramType="body")
	})
	@PutMapping(value = "")
	public ResponseVO<PermissionDTO> update(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@RequestBody PermissionDTO permissionDTO,
													@LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){

		Permission databaseObj = new Permission();
		BeanCopyUtilss.myCopyProperties(permissionDTO, databaseObj, loginedUserVO, false);
		int i = permissionService.updateByPrimaryKeySelective(databaseObj);

		// TODO 主键部分
		databaseObj = permissionService.selectByPrimaryKey(databaseObj.getPermissionId());

		return ResponseVO.successdata(databaseObj);
	}


	@ApiOperation("删除")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "permissionDTO", value = "", required = true, dataType = "PermissionDTO", paramType="body")
	})
	@DeleteMapping(value = "")
	public ResponseVO<String> delete(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@RequestBody PermissionDTO permissionDTO,
													@LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){

		Permission databaseObj = new Permission();
		BeanCopyUtilss.myCopyProperties(permissionDTO, databaseObj);

		// TODO 主键部分
		int i = permissionService.deleteByPrimaryKey(databaseObj.getPermissionId());

		return ResponseVO.successdata("操作成功");
	}

	@ApiOperation("详情")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "id", value = "编号", required = true, dataType = "String", paramType="path")
	})
	@GetMapping(value = "/{id}")
	public ResponseVO<PermissionDTO> selectOne(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@PathVariable("id") String id){

		Permission databaseObj = permissionService.selectByPrimaryKey(id);

		return ResponseVO.successdata(databaseObj);
	}

	@ApiOperation("查询")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "permissionDTO", value = "应用查询参数", required = false, dataType = "PermissionDTO", paramType="body"),
		@ApiImplicitParam(name = "pageNo", value = "页码", required = true, dataType = "int", paramType="query"),
		@ApiImplicitParam(name = "pageSize", value = "每页记录数", required = true, dataType = "int", paramType="query"),
		@ApiImplicitParam(name = "sortBy", value = "按照哪个属性排序", required = false, dataType = "String", paramType="query"),
		@ApiImplicitParam(name = "order", value = "排序顺序", required = false, dataType = "String", paramType="query")
	})
	@PostMapping(value = "/query")
	public ResponseVO<PermissionDTO>query(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
												@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
												@RequestBody PermissionDTO permissionDTO,
												@RequestParam Integer pageNo, @RequestParam Integer pageSize,
												@RequestParam(defaultValue = "create_time") String sortBy, @RequestParam(defaultValue = "desc") String order,
												@LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){
		Map<Object, Object> map = BeanCopyUtilss.beanToMap(permissionDTO, loginedUserVO);
		if(pageSize != 0) {
			Page newPage = new Page(pageNo + "", pageSize + "");
			map.put("page", newPage);
		}
		List<Permission> list = permissionService.selectListByMap(map);

		//
		List<PermissionDTO> dtoList = toDTO(list);

		if(pageSize != 0) {
			Page newPage = new Page(pageNo + "", pageSize + "");
			int countNum = permissionService.selectCountByMap(map);
			newPage.setCount(countNum);
			return ResponseVO.successdatas(dtoList, newPage);
		} else {
			return ResponseVO.successdatas(dtoList, null);
		}

	}

	private List<PermissionDTO> toDTO(List<Permission> databaseList) {
		List<PermissionDTO> returnList = new ArrayList<PermissionDTO>();
		if(databaseList != null) {
			for(Permission dbEntity : databaseList) {
				PermissionDTO dtoTemp = new PermissionDTO();
				BeanCopyUtilss.myCopyProperties(dbEntity, dtoTemp);
				returnList.add(dtoTemp);
			}
		}
		return returnList;
	}

	private List<Permission> toDB(List<PermissionDTO> dtoList) {
		List<Permission> returnList = new ArrayList<Permission>();
		if(dtoList != null) {
			for(PermissionDTO dtoEntity : dtoList) {
				Permission dbTemp = new Permission();
				BeanCopyUtilss.myCopyProperties(dtoEntity, dbTemp);
				returnList.add(dbTemp);
			}
		}
		return returnList;
	}
}
