package com.pap.rbac.permission.controller;

import com.pap.base.util.bean.BeanCopyUtilss;
import com.pap.beans.idworker.IdWorker;
import com.pap.obj.vo.logineduser.LoginedUser;
import com.pap.obj.vo.logineduser.LoginedUserVO;
import com.pap.obj.vo.page.Page;
import com.pap.obj.vo.response.ResponseVO;
import com.pap.rbac.dto.PermissionDTO;
import com.pap.rbac.dto.PermissionGroupPermissionRelDTO;
import com.pap.rbac.dto.PermissionGroupPermissionRelDetailsDTO;
import com.pap.rbac.permission.entity.PermissionGroupPermissionRel;
import com.pap.rbac.permission.service.IPermissionGroupPermissionRelService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/permissiongrouppermissionrel")
@Api(value = "权限组-权限关联关系管理", tags = "权限组-权限关联关系管理", description="权限组-权限关联关系管理")
public class PermissionGroupPermissionRelController {

	private static Logger logger  =  LoggerFactory.getLogger(PermissionGroupPermissionRelController.class);

	@Resource(name = "permissionGroupPermissionRelService")
	private IPermissionGroupPermissionRelService permissionGroupPermissionRelService;

	@Resource(name = "idWorker")
	private IdWorker idWorker;


	@ApiOperation("保存权限组与权限管理关系")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "permissionGroupPermissionRelDetailsDTO", value = "", required = true, dataType = "PermissionGroupPermissionRelDetailsDTO", paramType="body")
	})
	@PostMapping(value = "/saverels")
	public ResponseVO<String> saveRels(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
									   @RequestHeader(value = "papVersion", defaultValue = "-1") String papVersion,
									   @RequestBody PermissionGroupPermissionRelDetailsDTO permissionGroupPermissionRelDetailsDTO,
									   @LoginedUser @ApiIgnore LoginedUserVO loginedUserVO) throws Exception {

		String clientLicenseId = loginedUserVO != null ? loginedUserVO.getClientLicenseId() : "";
		permissionGroupPermissionRelService.updatePermissionGroupPermissionRel(loginedUserVO, permissionGroupPermissionRelDetailsDTO.getPermissionGroupId(), permissionGroupPermissionRelDetailsDTO.getDetails());
		return ResponseVO.successdata("操作成功");
	}

	@ApiOperation("根据权限组编号查询权限信息")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "permissionGroupId", value = "权限组编号", required = true, dataType = "String", paramType="query")
	})
	@GetMapping(value = "/selectpermissionidsbypermissiongroupid")
	public ResponseVO<PermissionDTO> selectOermissionIdsByPermissionGroupId(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
																			@RequestHeader(value = "papVersion", defaultValue = "-1") String papVersion,
																			@RequestParam String permissionGroupId) throws Exception {
		List<PermissionDTO> permisstionDTOList = new ArrayList<PermissionDTO>();

		List<PermissionGroupPermissionRelDTO> permissionGroupPermissionRelDTOList = permissionGroupPermissionRelService.selectPermissionIdsByPermissionGroupId(permissionGroupId);
		if(permissionGroupPermissionRelDTOList != null && permissionGroupPermissionRelDTOList.size() > 0) {
			for (PermissionGroupPermissionRelDTO permissionGroupPermissionRelDTO : permissionGroupPermissionRelDTOList) {
				PermissionDTO permissionDTO = permissionGroupPermissionRelService.selectDTOByPermissionId(permissionGroupPermissionRelDTO.getPermissionId());
				permisstionDTOList.add(permissionDTO);
			}
		}

		return ResponseVO.successdatas(permisstionDTOList, null);
	}


	@ApiOperation("创建")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "permissionGroupPermissionRelDTO", value = "", required = true, dataType = "PermissionGroupPermissionRelDTO", paramType="body")
	})
	@PostMapping(value = "")
	@Deprecated
	public ResponseVO<PermissionGroupPermissionRelDTO> create(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
															  @RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
															  @RequestBody PermissionGroupPermissionRelDTO permissionGroupPermissionRelDTO,
															  @LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){
		String idStr = idWorker.nextIdStr();
		// TODO 设置主键
		permissionGroupPermissionRelDTO.setPermissionGroupPermissionRelId(idStr);

		PermissionGroupPermissionRel databaseObj = new PermissionGroupPermissionRel();
		BeanCopyUtilss.myCopyProperties(permissionGroupPermissionRelDTO, databaseObj, loginedUserVO);

		int i = permissionGroupPermissionRelService.insertSelective(databaseObj);

		return ResponseVO.successdata(permissionGroupPermissionRelDTO);
	}

	@ApiOperation("更新")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "permissionGroupPermissionRelDTO", value = "", required = true, dataType = "PermissionGroupPermissionRelDTO", paramType="body")
	})
	@PutMapping(value = "")
	@Deprecated
	public ResponseVO<PermissionGroupPermissionRelDTO> update(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
															  @RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
															  @RequestBody PermissionGroupPermissionRelDTO permissionGroupPermissionRelDTO,
															  @LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){

		PermissionGroupPermissionRel databaseObj = new PermissionGroupPermissionRel();
		BeanCopyUtilss.myCopyProperties(permissionGroupPermissionRelDTO, databaseObj, loginedUserVO, false);
		int i = permissionGroupPermissionRelService.updateByPrimaryKeySelective(databaseObj);

		// TODO 主键部分
		databaseObj = permissionGroupPermissionRelService.selectByPrimaryKey(databaseObj.getPermissionGroupPermissionRelId());

		return ResponseVO.successdata(databaseObj);
	}


	@ApiOperation("删除")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "permissionGroupPermissionRelDTO", value = "", required = true, dataType = "PermissionGroupPermissionRelDTO", paramType="body")
	})
	@DeleteMapping(value = "")
	@Deprecated
	public ResponseVO<String> delete(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
									 @RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
									 @RequestBody PermissionGroupPermissionRelDTO permissionGroupPermissionRelDTO,
									 @LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){

		PermissionGroupPermissionRel databaseObj = new PermissionGroupPermissionRel();
		BeanCopyUtilss.myCopyProperties(permissionGroupPermissionRelDTO, databaseObj);

		// TODO 主键部分
		int i = permissionGroupPermissionRelService.deleteByPrimaryKey(databaseObj.getPermissionGroupPermissionRelId());

		return ResponseVO.successdata("操作成功");
	}

	@ApiOperation("详情")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "id", value = "编号", required = true, dataType = "String", paramType="path")
	})
	@GetMapping(value = "/{id}")
	@Deprecated
	public ResponseVO<PermissionGroupPermissionRelDTO> selectOne(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
																 @RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
																 @PathVariable("id") String id){

		PermissionGroupPermissionRel databaseObj = permissionGroupPermissionRelService.selectByPrimaryKey(id);

		return ResponseVO.successdata(databaseObj);
	}

	@ApiOperation("查询")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "permissionGroupPermissionRelDTO", value = "应用查询参数", required = false, dataType = "PermissionGroupPermissionRelDTO", paramType="body"),
			@ApiImplicitParam(name = "pageNo", value = "页码", required = true, dataType = "int", paramType="query"),
			@ApiImplicitParam(name = "pageSize", value = "每页记录数", required = true, dataType = "int", paramType="query"),
			@ApiImplicitParam(name = "sortBy", value = "按照哪个属性排序", required = false, dataType = "String", paramType="query"),
			@ApiImplicitParam(name = "order", value = "排序顺序", required = false, dataType = "String", paramType="query")
	})
	@PostMapping(value = "/query")
	@Deprecated
	public ResponseVO<PermissionGroupPermissionRelDTO>query(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
															@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
															@RequestBody PermissionGroupPermissionRelDTO permissionGroupPermissionRelDTO,
															@RequestParam Integer pageNo, @RequestParam Integer pageSize,
															@RequestParam(defaultValue = "create_time") String sortBy, @RequestParam(defaultValue = "desc") String order,
															@LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){
		Map<Object, Object> map = BeanCopyUtilss.beanToMap(permissionGroupPermissionRelDTO, loginedUserVO);
		if(pageSize != 0) {
			Page newPage = new Page(pageNo + "", pageSize + "");
			map.put("page", newPage);
		}
		List<PermissionGroupPermissionRel> list = permissionGroupPermissionRelService.selectListByMap(map);

		//
		List<PermissionGroupPermissionRelDTO> dtoList = toDTO(list);

		if(pageSize != 0) {
			Page newPage = new Page(pageNo + "", pageSize + "");
			int countNum = permissionGroupPermissionRelService.selectCountByMap(map);
			newPage.setCount(countNum);
			return ResponseVO.successdatas(dtoList, newPage);
		} else {
			return ResponseVO.successdatas(dtoList, null);
		}

	}

	private List<PermissionGroupPermissionRelDTO> toDTO(List<PermissionGroupPermissionRel> databaseList) {
		List<PermissionGroupPermissionRelDTO> returnList = new ArrayList<PermissionGroupPermissionRelDTO>();
		if(databaseList != null) {
			for(PermissionGroupPermissionRel dbEntity : databaseList) {
				PermissionGroupPermissionRelDTO dtoTemp = new PermissionGroupPermissionRelDTO();
				BeanCopyUtilss.myCopyProperties(dbEntity, dtoTemp);
				returnList.add(dtoTemp);
			}
		}
		return returnList;
	}

	private List<PermissionGroupPermissionRel> toDB(List<PermissionGroupPermissionRelDTO> dtoList) {
		List<PermissionGroupPermissionRel> returnList = new ArrayList<PermissionGroupPermissionRel>();
		if(dtoList != null) {
			for(PermissionGroupPermissionRelDTO dtoEntity : dtoList) {
				PermissionGroupPermissionRel dbTemp = new PermissionGroupPermissionRel();
				BeanCopyUtilss.myCopyProperties(dtoEntity, dbTemp);
				returnList.add(dbTemp);
			}
		}
		return returnList;
	}
}
