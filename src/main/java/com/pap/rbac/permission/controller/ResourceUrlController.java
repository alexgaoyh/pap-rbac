package com.pap.rbac.permission.controller;

import com.pap.base.util.bean.BeanCopyUtilss;
import com.pap.beans.idworker.IdWorker;
import com.pap.obj.vo.logineduser.LoginedUser;
import com.pap.obj.vo.logineduser.LoginedUserVO;
import com.pap.obj.vo.page.Page;
import com.pap.obj.vo.response.ResponseVO;
import com.pap.rbac.dto.ResourceUrlDTO;
import com.pap.rbac.permission.entity.ResourceUrl;
import com.pap.rbac.permission.service.IResourceUrlService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 权限对象管理进行额外处理，再维护过程中需要拆分为多张表分别进行存储
 * 原因： 权限对象仅包含权限编码与对应的权限名称和备注， 所有额外的解释说明存放到资源对象中
 *
 * 形如一个 HTTP接口， 额外的说明存放到 t_rbac_resource_url 中，
 * 用户仅需要绑定到权限编码即可，对权限编码的解释说明需要额外请求资源。
 */
@RestController
@RequestMapping("/resourceurl")
@Api(value = "HTTP请求资源管理", tags = "HTTP请求资源管理", description="HTTP请求资源管理")
@Deprecated
public class ResourceUrlController {

	private static Logger logger  =  LoggerFactory.getLogger(ResourceUrlController.class);
	
	@Resource(name = "resourceUrlService")
	private IResourceUrlService resourceUrlService;

	@Resource(name = "idWorker")
	private IdWorker idWorker;

	@ApiOperation("创建")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "resourceUrlDTO", value = "", required = true, dataType = "ResourceUrlDTO", paramType="body")
	})
	@PostMapping(value = "")
	public ResponseVO<ResourceUrlDTO> create(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
											 @RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
											 @RequestBody ResourceUrlDTO resourceUrlDTO,
											 @LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){
		String idStr = idWorker.nextIdStr();
		// TODO 设置主键
		resourceUrlDTO.setResourceUrlId(idStr);

		ResourceUrl databaseObj = new ResourceUrl();
		BeanCopyUtilss.myCopyProperties(resourceUrlDTO, databaseObj, loginedUserVO);

		int i = resourceUrlService.insertSelective(databaseObj);

		return ResponseVO.successdata(resourceUrlDTO);
	}

	@ApiOperation("更新")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "resourceUrlDTO", value = "", required = true, dataType = "ResourceUrlDTO", paramType="body")
	})
	@PutMapping(value = "")
	public ResponseVO<ResourceUrlDTO> update(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@RequestBody ResourceUrlDTO resourceUrlDTO,
													@LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){

		ResourceUrl databaseObj = new ResourceUrl();
		BeanCopyUtilss.myCopyProperties(resourceUrlDTO, databaseObj, loginedUserVO, false);
		int i = resourceUrlService.updateByPrimaryKeySelective(databaseObj);

		// TODO 主键部分
		databaseObj = resourceUrlService.selectByPrimaryKey(databaseObj.getResourceUrlId());

		return ResponseVO.successdata(databaseObj);
	}


	@ApiOperation("删除")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "resourceUrlDTO", value = "", required = true, dataType = "ResourceUrlDTO", paramType="body")
	})
	@DeleteMapping(value = "")
	public ResponseVO<String> delete(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@RequestBody ResourceUrlDTO resourceUrlDTO,
													@LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){

		ResourceUrl databaseObj = new ResourceUrl();
		BeanCopyUtilss.myCopyProperties(resourceUrlDTO, databaseObj);

		// TODO 主键部分
		int i = resourceUrlService.deleteByPrimaryKey(databaseObj.getResourceUrlId());

		return ResponseVO.successdata("操作成功");
	}

	@ApiOperation("详情")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "id", value = "编号", required = true, dataType = "String", paramType="path")
	})
	@GetMapping(value = "/{id}")
	public ResponseVO<ResourceUrlDTO> selectOne(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@PathVariable("id") String id){

		ResourceUrl databaseObj = resourceUrlService.selectByPrimaryKey(id);

		return ResponseVO.successdata(databaseObj);
	}

	@ApiOperation("查询")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "resourceUrlDTO", value = "应用查询参数", required = false, dataType = "ResourceUrlDTO", paramType="body"),
		@ApiImplicitParam(name = "pageNo", value = "页码", required = true, dataType = "int", paramType="query"),
		@ApiImplicitParam(name = "pageSize", value = "每页记录数", required = true, dataType = "int", paramType="query"),
		@ApiImplicitParam(name = "sortBy", value = "按照哪个属性排序", required = false, dataType = "String", paramType="query"),
		@ApiImplicitParam(name = "order", value = "排序顺序", required = false, dataType = "String", paramType="query")
	})
	@PostMapping(value = "/query")
	public ResponseVO<ResourceUrlDTO>query(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
												@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
												@RequestBody ResourceUrlDTO resourceUrlDTO,
												@RequestParam Integer pageNo, @RequestParam Integer pageSize,
												@RequestParam(defaultValue = "create_time") String sortBy, @RequestParam(defaultValue = "desc") String order,
												@LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){
		Map<Object, Object> map = BeanCopyUtilss.beanToMap(resourceUrlDTO, loginedUserVO);
		if(pageSize != 0) {
			Page newPage = new Page(pageNo + "", pageSize + "");
			map.put("page", newPage);
		}
		List<ResourceUrl> list = resourceUrlService.selectListByMap(map);

		//
		List<ResourceUrlDTO> dtoList = toDTO(list);

		if(pageSize != 0) {
			Page newPage = new Page(pageNo + "", pageSize + "");
			int countNum = resourceUrlService.selectCountByMap(map);
			newPage.setCount(countNum);
			return ResponseVO.successdatas(dtoList, newPage);
		} else {
			return ResponseVO.successdatas(dtoList, null);
		}

	}

	private List<ResourceUrlDTO> toDTO(List<ResourceUrl> databaseList) {
		List<ResourceUrlDTO> returnList = new ArrayList<ResourceUrlDTO>();
		if(databaseList != null) {
			for(ResourceUrl dbEntity : databaseList) {
				ResourceUrlDTO dtoTemp = new ResourceUrlDTO();
				BeanCopyUtilss.myCopyProperties(dbEntity, dtoTemp);
				returnList.add(dtoTemp);
			}
		}
		return returnList;
	}

	private List<ResourceUrl> toDB(List<ResourceUrlDTO> dtoList) {
		List<ResourceUrl> returnList = new ArrayList<ResourceUrl>();
		if(dtoList != null) {
			for(ResourceUrlDTO dtoEntity : dtoList) {
				ResourceUrl dbTemp = new ResourceUrl();
				BeanCopyUtilss.myCopyProperties(dtoEntity, dbTemp);
				returnList.add(dbTemp);
			}
		}
		return returnList;
	}
}
