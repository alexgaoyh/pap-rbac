package com.pap.rbac.permission.controller;

import com.github.stuxuhai.jpinyin.PinyinFormat;
import com.github.stuxuhai.jpinyin.PinyinHelper;
import com.pap.base.util.bean.BeanCopyUtilss;
import com.pap.base.util.string.StringUtilss;
import com.pap.beans.idworker.IdWorker;
import com.pap.obj.vo.logineduser.LoginedUser;
import com.pap.obj.vo.logineduser.LoginedUserVO;
import com.pap.obj.vo.page.Page;
import com.pap.obj.vo.response.ResponseVO;
import com.pap.rbac.dto.PermissionGroupDTO;
import com.pap.rbac.dto.PermissionGroupTreeNodeDTO;
import com.pap.rbac.permission.entity.PermissionGroup;
import com.pap.rbac.permission.service.IPermissionGroupService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/permissiongroup")
@Api(value = "权限组管理", tags = "权限组管理", description="权限组管理")
public class PermissionGroupController {

	private static Logger logger  =  LoggerFactory.getLogger(PermissionGroupController.class);
	
	@Resource(name = "permissionGroupService")
	private IPermissionGroupService permissionGroupService;

	@Resource(name = "idWorker")
	private IdWorker idWorker;


	@RequestMapping(value = "/treejson", method = {RequestMethod.GET})
	public ResponseVO<List<PermissionGroupTreeNodeDTO>> treeJSON(@LoginedUser @ApiIgnore LoginedUserVO loginedUserVO) {
		//  全局默认角色的顶层编码为 -1
		String globalParentId = "-1";
		return ResponseVO.successdatas(permissionGroupService.permissionGroupTreeJson(loginedUserVO != null ? loginedUserVO.getClientLicenseId() : "", globalParentId), null);
	}
	
	@ApiOperation("创建")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "permissionGroupDTO", value = "", required = true, dataType = "PermissionGroupDTO", paramType="body")
	})
	@PostMapping(value = "")
	public ResponseVO<PermissionGroupDTO> create(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
												 @RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
												 @RequestBody PermissionGroupDTO permissionGroupDTO,
												 @LoginedUser @ApiIgnore LoginedUserVO loginedUserVO) throws Exception {
		String idStr = idWorker.nextIdStr();

		permissionGroupDTO.setPermissionGroupId(idStr);

		if(permissionGroupDTO != null &&
				StringUtilss.isNotEmpty(permissionGroupDTO.getPermissionGroupName())) {
			// 如果前端没有传递过来code值，则默认使用拼音代替
			if(StringUtilss.isEmpty(permissionGroupDTO.getPermissionGroupCode())) {
				permissionGroupDTO.setPermissionGroupCode(PinyinHelper.convertToPinyinString(permissionGroupDTO.getPermissionGroupName(), "", PinyinFormat.WITHOUT_TONE));
			}
			// 处理父级数据
			PermissionGroup parentPermissionGroup = permissionGroupService.selectByPrimaryKey(permissionGroupDTO.getPermissionGroupParentId());
			if(parentPermissionGroup != null) {
				// pathId
				if(StringUtilss.isNotEmpty(parentPermissionGroup.getPathIds())) {
					permissionGroupDTO.setPathIds(parentPermissionGroup.getPathIds() + "," + parentPermissionGroup.getPermissionGroupId());
				} else {
					permissionGroupDTO.setPathIds(parentPermissionGroup.getPermissionGroupId());
				}
				// pathCode
				if(StringUtilss.isNotEmpty(parentPermissionGroup.getPathCodes())) {
					permissionGroupDTO.setPathCodes(parentPermissionGroup.getPathCodes() + "," + parentPermissionGroup.getPermissionGroupCode());
				} else {
					permissionGroupDTO.setPathCodes(parentPermissionGroup.getPermissionGroupCode());
				}
				// pathName
				if(StringUtilss.isNotEmpty(parentPermissionGroup.getPathNames())) {
					permissionGroupDTO.setPathNames(parentPermissionGroup.getPathNames() + "," + parentPermissionGroup.getPermissionGroupName());
				} else {
					permissionGroupDTO.setPathNames(parentPermissionGroup.getPermissionGroupName());
				}
			} else {
				permissionGroupDTO.setPermissionGroupParentId(null);
				permissionGroupDTO.setPathIds(null);
				permissionGroupDTO.setPathCodes(null);
				permissionGroupDTO.setPathNames(null);
			}
			permissionGroupDTO.setPermissionGroupParentId(StringUtilss.isNotEmpty(permissionGroupDTO.getPermissionGroupParentId()) == true ? permissionGroupDTO.getPermissionGroupParentId() : "-1");


			PermissionGroup dbPermissionGroup = new PermissionGroup();
			BeanCopyUtilss.myCopyProperties(permissionGroupDTO, dbPermissionGroup, loginedUserVO, true);
			int operationInt = permissionGroupService.insertSelective(dbPermissionGroup);

			return ResponseVO.successdata(permissionGroupDTO);

		} else {
			return ResponseVO.validfail("入参异常，请判断是否含有角色名称");
		}

	}

	@ApiOperation("更新")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "permissionGroupDTO", value = "", required = true, dataType = "PermissionGroupDTO", paramType="body")
	})
	@PutMapping(value = "")
	public ResponseVO<PermissionGroupDTO> update(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@RequestBody PermissionGroupDTO permissionGroupDTO,
													@LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){

		PermissionGroup dbPermissionGroup = new PermissionGroup();
		BeanCopyUtilss.myCopyProperties(permissionGroupDTO, dbPermissionGroup, loginedUserVO, false);

		ResponseVO responseVO = permissionGroupService.updateAndCheckPermissionGroup(dbPermissionGroup);
		if(responseVO.getCode().equals("200")) {
			PermissionGroupDTO permissionGroupDTOTemp = new PermissionGroupDTO();
			BeanCopyUtilss.myCopyProperties(responseVO.getData(), permissionGroupDTOTemp);
			return ResponseVO.successdata(permissionGroupDTOTemp);
		} else {
			return responseVO;
		}
	}


	@ApiOperation("删除")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "permissionGroupDTO", value = "", required = true, dataType = "PermissionGroupDTO", paramType="body")
	})
	@DeleteMapping(value = "")
	public ResponseVO<String> delete(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@RequestBody PermissionGroupDTO permissionGroupDTO,
													@LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){

		PermissionGroup databaseObj = new PermissionGroup();
		BeanCopyUtilss.myCopyProperties(permissionGroupDTO, databaseObj);
		if(databaseObj != null && StringUtilss.isNotEmpty(databaseObj.getPermissionGroupId())) {
			Map<Object, Object> pathIdsMap = new HashMap<Object, Object>();
			pathIdsMap.put("myLike_pathIds", databaseObj.getPermissionGroupId());
			List<PermissionGroup> permissionGroupist = permissionGroupService.selectListByMap(pathIdsMap);
			if(permissionGroupist != null && permissionGroupist.size() > 0) {
				return ResponseVO.validfail("请检查入参，此资源被其他资源所依赖!");
			} else {
				int operationInt = permissionGroupService.deleteByPrimaryKey(databaseObj.getPermissionGroupId());
				return ResponseVO.successdata("操作成功");
			}
		} else {
			return ResponseVO.validfail("入参异常，请判断是否含有角色资源编码");
		}
	}

	@ApiOperation("详情")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "id", value = "编号", required = true, dataType = "String", paramType="path")
	})
	@GetMapping(value = "/{id}")
	public ResponseVO<PermissionGroupDTO> selectOne(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@PathVariable("id") String id){

		PermissionGroup databaseObj = permissionGroupService.selectByPrimaryKey(id);

		return ResponseVO.successdata(databaseObj);
	}

	@ApiOperation("查询")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "permissionGroupDTO", value = "应用查询参数", required = false, dataType = "PermissionGroupDTO", paramType="body"),
		@ApiImplicitParam(name = "pageNo", value = "页码", required = true, dataType = "int", paramType="query"),
		@ApiImplicitParam(name = "pageSize", value = "每页记录数", required = true, dataType = "int", paramType="query"),
		@ApiImplicitParam(name = "sortBy", value = "按照哪个属性排序", required = false, dataType = "String", paramType="query"),
		@ApiImplicitParam(name = "order", value = "排序顺序", required = false, dataType = "String", paramType="query")
	})
	@PostMapping(value = "/query")
	public ResponseVO<PermissionGroupDTO>query(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
												@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
												@RequestBody PermissionGroupDTO permissionGroupDTO,
												@RequestParam Integer pageNo, @RequestParam Integer pageSize,
												@RequestParam(defaultValue = "create_time") String sortBy, @RequestParam(defaultValue = "desc") String order,
												@LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){
		Map<Object, Object> map = BeanCopyUtilss.beanToMap(permissionGroupDTO, loginedUserVO);
		if(pageSize != 0) {
			Page newPage = new Page(pageNo + "", pageSize + "");
			map.put("page", newPage);
		}
		List<PermissionGroup> list = permissionGroupService.selectListByMap(map);

		//
		List<PermissionGroupDTO> dtoList = toDTO(list);

		if(pageSize != 0) {
			Page newPage = new Page(pageNo + "", pageSize + "");
			int countNum = permissionGroupService.selectCountByMap(map);
			newPage.setCount(countNum);
			return ResponseVO.successdatas(dtoList, newPage);
		} else {
			return ResponseVO.successdatas(dtoList, null);
		}

	}

	private List<PermissionGroupDTO> toDTO(List<PermissionGroup> databaseList) {
		List<PermissionGroupDTO> returnList = new ArrayList<PermissionGroupDTO>();
		if(databaseList != null) {
			for(PermissionGroup dbEntity : databaseList) {
				PermissionGroupDTO dtoTemp = new PermissionGroupDTO();
				BeanCopyUtilss.myCopyProperties(dbEntity, dtoTemp);
				returnList.add(dtoTemp);
			}
		}
		return returnList;
	}

	private List<PermissionGroup> toDB(List<PermissionGroupDTO> dtoList) {
		List<PermissionGroup> returnList = new ArrayList<PermissionGroup>();
		if(dtoList != null) {
			for(PermissionGroupDTO dtoEntity : dtoList) {
				PermissionGroup dbTemp = new PermissionGroup();
				BeanCopyUtilss.myCopyProperties(dtoEntity, dbTemp);
				returnList.add(dbTemp);
			}
		}
		return returnList;
	}
}
