package com.pap.rbac.permission.controller;

import com.pap.base.util.bean.BeanCopyUtilss;
import com.pap.beans.idworker.IdWorker;
import com.pap.obj.vo.logineduser.LoginedUser;
import com.pap.obj.vo.logineduser.LoginedUserVO;
import com.pap.obj.vo.page.Page;
import com.pap.obj.vo.response.ResponseVO;
import com.pap.rbac.dto.PermissionResourceUrlDTO;
import com.pap.rbac.permission.service.IPermissionResourceUrlService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @Auther: alexgaoyh
 * @Date: 2019/2/14 14:48
 * @Description:
 */
@RestController
@RequestMapping("/permission/resource/url")
@Api(value = "HTTP协议权限管理", tags = "HTTP协议权限管理", description="HTTP协议权限管理")
public class PermissionResourceUrlController {

    private static Logger logger  =  LoggerFactory.getLogger(PermissionController.class);

    @Resource(name = "permissionResourceUrlService")
    private IPermissionResourceUrlService permissionResourceUrlService;

    @Resource(name = "idWorker")
    private IdWorker idWorker;


    @ApiOperation("创建")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
            @ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
            @ApiImplicitParam(name = "permissionResourceUrlDTO", value = "", required = true, dataType = "PermissionResourceUrlDTO", paramType="body")
    })
    @PostMapping(value = "")
    public ResponseVO<PermissionResourceUrlDTO> create(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
                                            @RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
                                            @RequestBody PermissionResourceUrlDTO permissionResourceUrlDTO,
                                            @LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){
        String idStr = idWorker.nextIdStr();
        permissionResourceUrlDTO.setPermissionId(idStr);

        int i = permissionResourceUrlService.insertSelective(permissionResourceUrlDTO);

        return ResponseVO.successdata(permissionResourceUrlDTO);
    }

    @ApiOperation("更新")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
            @ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
            @ApiImplicitParam(name = "permissionResourceUrlDTO", value = "", required = true, dataType = "PermissionResourceUrlDTO", paramType="body")
    })
    @PutMapping(value = "")
    public ResponseVO<PermissionResourceUrlDTO> update(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
                                            @RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
                                            @RequestBody PermissionResourceUrlDTO permissionResourceUrlDTO,
                                            @LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){


        int i = permissionResourceUrlService.updateByPrimaryKeySelective(permissionResourceUrlDTO);

        return ResponseVO.successdata(permissionResourceUrlDTO);
    }


    @ApiOperation("删除")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
            @ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
            @ApiImplicitParam(name = "permissionResourceUrlDTO", value = "", required = true, dataType = "PermissionResourceUrlDTO", paramType="body")
    })
    @DeleteMapping(value = "")
    public ResponseVO<String> delete(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
                                     @RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
                                     @RequestBody PermissionResourceUrlDTO permissionResourceUrlDTO,
                                     @LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){

        int i = permissionResourceUrlService.deleteByPrimaryKey(permissionResourceUrlDTO.getPermissionId());

        return ResponseVO.successdata("操作成功");
    }

    @ApiOperation("详情")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
            @ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
            @ApiImplicitParam(name = "id", value = "编号", required = true, dataType = "String", paramType="path")
    })
    @GetMapping(value = "/{id}")
    public ResponseVO<PermissionResourceUrlDTO> selectOne(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
                                               @RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
                                               @PathVariable("id") String id){

        PermissionResourceUrlDTO permissionResourceUrlDTO = permissionResourceUrlService.selectByPrimaryKey(id);

        return ResponseVO.successdata(permissionResourceUrlDTO);
    }

    @ApiOperation("查询")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
            @ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
            @ApiImplicitParam(name = "permissionResourceUrlDTO", value = "应用查询参数", required = false, dataType = "PermissionResourceUrlDTO", paramType="body"),
            @ApiImplicitParam(name = "pageNo", value = "页码", required = true, dataType = "int", paramType="query"),
            @ApiImplicitParam(name = "pageSize", value = "每页记录数", required = true, dataType = "int", paramType="query"),
            @ApiImplicitParam(name = "sortBy", value = "按照哪个属性排序", required = false, dataType = "String", paramType="query"),
            @ApiImplicitParam(name = "order", value = "排序顺序", required = false, dataType = "String", paramType="query")
    })
    @PostMapping(value = "/query")
    public ResponseVO<PermissionResourceUrlDTO>query(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
                                          @RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
                                          @RequestBody PermissionResourceUrlDTO permissionResourceUrlDTO,
                                          @RequestParam Integer pageNo, @RequestParam Integer pageSize,
                                          @RequestParam(defaultValue = "create_time") String sortBy, @RequestParam(defaultValue = "desc") String order,
                                          @LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){
        Map<Object, Object> map = BeanCopyUtilss.beanToMap(permissionResourceUrlDTO);
        if(pageSize != 0) {
            Page newPage = new Page(pageNo + "", pageSize + "");
            map.put("page", newPage);
        }
        List<PermissionResourceUrlDTO> list = permissionResourceUrlService.selectListByMap(map);


        if(pageSize != 0) {
            Page newPage = new Page(pageNo + "", pageSize + "");
            int countNum = permissionResourceUrlService.selectCountByMap(map);
            newPage.setCount(countNum);
            return ResponseVO.successdatas(list, newPage);
        } else {
            return ResponseVO.successdatas(list, null);
        }

    }

}
