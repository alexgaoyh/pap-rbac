package com.pap.rbac.organization.controller;

import com.github.stuxuhai.jpinyin.PinyinFormat;
import com.github.stuxuhai.jpinyin.PinyinHelper;
import com.pap.base.annotation.vo.ExtraParamsVO;
import com.pap.base.util.bean.BeanCopyUtilss;
import com.pap.base.util.string.StringUtilss;
import com.pap.beans.idworker.IdWorker;
import com.pap.obj.vo.business.DisableFlagEnum;
import com.pap.obj.vo.logineduser.LoginedUser;
import com.pap.obj.vo.logineduser.LoginedUserVO;
import com.pap.obj.vo.page.Page;
import com.pap.obj.vo.response.ResponseVO;
import com.pap.rbac.dto.OrganizationDTO;
import com.pap.rbac.dto.OrganizationTreeNodeVO;
import com.pap.rbac.organization.entity.Organization;
import com.pap.rbac.organization.service.IOrganizationExtraService;
import com.pap.rbac.organization.service.IOrganizationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/organization")
@Api(value = "组织机构", tags = "组织机构", description="组织机构")
public class OrganizationController {

	private static Logger logger  =  LoggerFactory.getLogger(OrganizationController.class);

	@Resource(name = "organizationService")
	private IOrganizationService organizationService;

	@Resource(name = "organizationExtraService")
	private IOrganizationExtraService organizationExtraService;

	@Resource(name = "idWorker")
	private IdWorker idWorker;

	/**
	 *
	 *
	 * dynamic params Solution --- 动态参数解决:
	 * 		在DTO中维护一个 Map<String, String> extraParams 参数，对象里面的key value 都由前端维护，并将此数据进行维护处理，放置到 *_extra 的表结构中
	 *
	 * Tips： 记录另一种额外参数的维护处理方式
	 * 		用以解决： 请求参数的过程中，如果存在动态参数的情况(参数key value由前端维护)
	 * 			此时可以将对应的JSON格式以request.getParameter() 的形式进行发送，通过如下方式进行接收，维护处理
	 * 	ApiImplicitParam 增加： @ApiImplicitParam(name = "extraParams", value = "额外JSON数据", required = false, dataType = "String", paramType="query"),
	 * 	方法入参增加： @ExtraParamsAnnotation @ApiIgnore ExtraParamsVO extraParamsVO
	 * @param papToken
	 * @param papVersion
	 * @param organizationDTO
	 * @return
	 * @throws Exception
	 */
	@ApiOperation("创建")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "organizationDTO", value = "", required = true, dataType = "OrganizationDTO", paramType="body")
	})
	@PostMapping(value = "")
	public ResponseVO<OrganizationDTO> create(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
											  @RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
											  @RequestBody OrganizationDTO organizationDTO,
											  @LoginedUser @ApiIgnore LoginedUserVO loginedUserVO) throws Exception {
		String idStr = idWorker.nextIdStr();
		organizationDTO.setOrganizationId(idStr);

		BeanCopyUtilss.myCopyPropertiesForLoginedUser(organizationDTO, loginedUserVO, true);

		// 此处维护数据集合 - 如果请求参数request.getParameter("extraParams") 没有匹配的数据，则从DTO中进行维护
		ExtraParamsVO extraParamsVO = ExtraParamsVO.ofExtraParamsVO(organizationDTO.getExtraParams());

		if(organizationDTO != null &&
				StringUtilss.isNotEmpty(organizationDTO.getOrganizationName())) {
			// 如果前端没有传递过来code值，则默认使用拼音代替
			if(StringUtilss.isEmpty(organizationDTO.getOrganizationCode())) {
				organizationDTO.setOrganizationCode(PinyinHelper.convertToPinyinString(organizationDTO.getOrganizationName(), "", PinyinFormat.WITHOUT_TONE));
			}
			// 处理父级数据
			Organization parentOrganization = organizationService.selectByPrimaryKey(organizationDTO.getOrganizationParentId());
			if(parentOrganization != null) {
				// pathId
				if(StringUtilss.isNotEmpty(parentOrganization.getPathIds())) {
					organizationDTO.setPathIds(parentOrganization.getPathIds() + "," + parentOrganization.getOrganizationId());
				} else {
					organizationDTO.setPathIds(parentOrganization.getOrganizationId());
				}
				// pathCode
				if(StringUtilss.isNotEmpty(parentOrganization.getPathCodes())) {
					organizationDTO.setPathCodes(parentOrganization.getPathCodes() + "," + parentOrganization.getOrganizationCode());
				} else {
					organizationDTO.setPathCodes(parentOrganization.getOrganizationCode());
				}
				// pathName
				if(StringUtilss.isNotEmpty(parentOrganization.getPathNames())) {
					organizationDTO.setPathNames(parentOrganization.getPathNames() + "," + parentOrganization.getOrganizationName());
				} else {
					organizationDTO.setPathNames(parentOrganization.getOrganizationName());
				}
			} else {
				organizationDTO.setOrganizationParentId(null);
				organizationDTO.setPathIds(null);
				organizationDTO.setPathCodes(null);
				organizationDTO.setPathNames(null);
			}
			organizationDTO.setDisableFlag(DisableFlagEnum.YES.getKey());
			organizationDTO.setOrganizationParentId(StringUtilss.isNotEmpty(organizationDTO.getOrganizationParentId()) == true ? organizationDTO.getOrganizationParentId() : "-1");

			Organization dbOrganization = new Organization();
			BeanCopyUtilss.myCopyProperties(organizationDTO, dbOrganization, loginedUserVO, true);
			String operationInt = organizationService.insertSelectiveWithExtraParams(dbOrganization, extraParamsVO);

			return ResponseVO.successdata(organizationDTO);

		} else {
			return ResponseVO.validfail("入参异常，请判断是否含有组织机构名称");
		}
	}

	@ApiOperation("更新")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "organizationDTO", value = "", required = true, dataType = "OrganizationDTO", paramType="body")
	})
	@PutMapping(value = "")
	public ResponseVO<OrganizationDTO> update(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
											  @RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
											  @RequestBody OrganizationDTO organizationDTO,
											  @LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){

		Organization databaseObj = new Organization();
		BeanCopyUtilss.myCopyProperties(organizationDTO, databaseObj, loginedUserVO, false);

		// 此处维护数据集合 - 如果请求参数request.getParameter("extraParams") 没有匹配的数据，则从DTO中进行维护
		ExtraParamsVO extraParamsVO = ExtraParamsVO.ofExtraParamsVO(organizationDTO.getExtraParams());


		Organization dbOrganization = new Organization();
		BeanCopyUtilss.myCopyProperties(organizationDTO, dbOrganization);
		ResponseVO responseVO = organizationService.updateAndCheckOrganization(dbOrganization);

		if(responseVO.getCode().equals("200")) {
			// 重新更新实体对象和扩展字段
			int i = organizationService.updateByPrimaryKeySelectiveForExtraParams(databaseObj, extraParamsVO);
			// 查询最新的数据进行返回
			OrganizationDTO organizationDTOReturn = organizationService.selectDTOByPrimaryKey(organizationDTO.getOrganizationId());
			return ResponseVO.successdata(organizationDTOReturn);
		} else {
			return responseVO;
		}


	}


	@ApiOperation("删除")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "organizationDTO", value = "", required = true, dataType = "OrganizationDTO", paramType="body")
	})
	@DeleteMapping(value = "")
	public ResponseVO<String> delete(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
											  @RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
											  @RequestBody OrganizationDTO organizationDTO){

		if(organizationDTO != null && StringUtilss.isNotEmpty(organizationDTO.getOrganizationId())) {
			Map<Object, Object> pathIdsMap = new HashMap<Object, Object>();
			pathIdsMap.put("myLike_pathIds", organizationDTO.getOrganizationId());
			List<Organization> organizationList = organizationService.selectListByMap(pathIdsMap);
			if(organizationList != null && organizationList.size() > 0) {
				return ResponseVO.validfail("请检查入参，此机构被其他机构所依赖!");
			} else {
				int operationInt = organizationService.deleteWithExtraParamsByPrimaryKey(organizationDTO.getOrganizationId());
				return ResponseVO.successdata("操作成功");
			}
		} else {
			return ResponseVO.validfail("入参异常，请判断是否含有机构编码");
		}

	}

	@ApiOperation("详情")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "id", value = "编号", required = true, dataType = "String", paramType="path")
	})
	@GetMapping(value = "/{id}")
	public ResponseVO<OrganizationDTO> selectOne(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
												 @RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
												 @PathVariable("id") String id){

		OrganizationDTO organizationDTO = organizationService.selectDTOByPrimaryKey(id);

		return ResponseVO.successdata(organizationDTO);
	}


	@ApiOperation("查询")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "organizationDTO", value = "应用查询参数", required = false, dataType = "OrganizationDTO", paramType="body"),
			@ApiImplicitParam(name = "pageNo", value = "页码", required = true, dataType = "int", paramType="query"),
			@ApiImplicitParam(name = "pageSize", value = "每页记录数", required = true, dataType = "int", paramType="query"),
			@ApiImplicitParam(name = "sortBy", value = "按照哪个属性排序", required = false, dataType = "String", paramType="query"),
			@ApiImplicitParam(name = "order", value = "排序顺序", required = false, dataType = "String", paramType="query")
	})
	@PostMapping(value = "/query")
	public ResponseVO<OrganizationDTO>query(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
											@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
											@RequestBody OrganizationDTO organizationDTO,
											@RequestParam Integer pageNo, @RequestParam Integer pageSize,
											@RequestParam(defaultValue = "create_time") String sortBy, @RequestParam(defaultValue = "desc") String order,
											@LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){
		Map<Object, Object> map = BeanCopyUtilss.beanToMap(organizationDTO, loginedUserVO);
		if(pageSize != 0) {
			Page newPage = new Page(pageNo + "", pageSize + "");
			map.put("page", newPage);
		}
		List<Organization> list = organizationService.selectListByMap(map);

		//
		List<OrganizationDTO> dtoList = toDTO(list);

		if(pageSize != 0) {
			Page newPage = new Page(pageNo + "", pageSize + "");
			int countNum = organizationService.selectCountByMap(map);
			newPage.setCount(countNum);
			return ResponseVO.successdatas(dtoList, newPage);
		} else {
			return ResponseVO.successdatas(dtoList, null);
		}

	}

	@RequestMapping(value = "/treejson", method = {RequestMethod.GET})
	public ResponseVO<List<OrganizationTreeNodeVO>> treeJSON(@LoginedUser @ApiIgnore LoginedUserVO loginedUserVO) {
		//  全局默认组织机构的顶层编码为 -1 
		String globalParentId = "-1";
		return ResponseVO.successdatas(organizationService.organizationTreeJson(loginedUserVO.getClientLicenseId(), globalParentId), null);
	}

	private List<OrganizationDTO> toDTO(List<Organization> databaseList) {
		List<OrganizationDTO> returnList = new ArrayList<OrganizationDTO>();
		if(databaseList != null) {
			for(Organization dbEntity : databaseList) {
				OrganizationDTO dtoTemp = new OrganizationDTO();
				BeanCopyUtilss.myCopyProperties(dbEntity, dtoTemp);
				returnList.add(dtoTemp);
			}
		}
		return returnList;
	}

	private List<Organization> toDB(List<OrganizationDTO> dtoList) {
		List<Organization> returnList = new ArrayList<Organization>();
		if(dtoList != null) {
			for(OrganizationDTO dtoEntity : dtoList) {
				Organization dbTemp = new Organization();
				BeanCopyUtilss.myCopyProperties(dtoEntity, dbTemp);
				returnList.add(dbTemp);
			}
		}
		return returnList;
	}
}
