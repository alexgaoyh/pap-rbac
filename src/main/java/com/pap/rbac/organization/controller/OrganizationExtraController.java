package com.pap.rbac.organization.controller;

import com.pap.base.util.bean.BeanCopyUtilss;
import com.pap.beans.idworker.IdWorker;
import com.pap.obj.vo.logineduser.LoginedUser;
import com.pap.obj.vo.logineduser.LoginedUserVO;
import com.pap.obj.vo.page.Page;
import com.pap.obj.vo.response.ResponseVO;
import com.pap.rbac.dto.OrganizationExtraDTO;
import com.pap.rbac.organization.entity.OrganizationExtra;
import com.pap.rbac.organization.service.IOrganizationExtraService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


//@RestController
//@RequestMapping("/organizationextra")
//@Api(value = "组织机构扩展属性", tags = "组织机构扩展属性", description="组织机构扩展属性")
@Deprecated
public class OrganizationExtraController {

	private static Logger logger  =  LoggerFactory.getLogger(OrganizationExtraController.class);
	
	@Resource(name = "organizationExtraService")
	private IOrganizationExtraService organizationExtraService;

	@Resource(name = "idWorker")
	private IdWorker idWorker;

	@ApiOperation("创建")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "organizationExtraDTO", value = "", required = true, dataType = "OrganizationExtraDTO", paramType="body")
	})
	@PostMapping(value = "")
	public ResponseVO<OrganizationExtraDTO> create(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
												   @RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
												   @RequestBody OrganizationExtraDTO organizationExtraDTO){
		String idStr = idWorker.nextIdStr();
		organizationExtraDTO.setOrganizationExtraId(idStr);

		OrganizationExtra databaseObj = new OrganizationExtra();
		BeanCopyUtilss.myCopyProperties(organizationExtraDTO, databaseObj);

		int i = organizationExtraService.insertSelective(databaseObj);

		return ResponseVO.successdata(organizationExtraDTO);
	}

	@ApiOperation("更新")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "organizationExtraDTO", value = "", required = true, dataType = "OrganizationExtraDTO", paramType="body")
	})
	@PutMapping(value = "")
	public ResponseVO<OrganizationExtraDTO> update(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@RequestBody OrganizationExtraDTO organizationExtraDTO){

		OrganizationExtra databaseObj = new OrganizationExtra();
		BeanCopyUtilss.myCopyProperties(organizationExtraDTO, databaseObj);
		int i = organizationExtraService.updateByPrimaryKeySelective(databaseObj);

		databaseObj = organizationExtraService.selectByPrimaryKey(databaseObj.getOrganizationExtraId());

		return ResponseVO.successdata(databaseObj);
	}


	@ApiOperation("删除")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "organizationExtraDTO", value = "", required = true, dataType = "OrganizationExtraDTO", paramType="body")
	})
	@DeleteMapping(value = "")
	public ResponseVO<String> delete(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@RequestBody OrganizationExtraDTO organizationExtraDTO){

		OrganizationExtra databaseObj = new OrganizationExtra();
		BeanCopyUtilss.myCopyProperties(organizationExtraDTO, databaseObj);
		int i = organizationExtraService.deleteByPrimaryKey(databaseObj.getOrganizationExtraId());

		return ResponseVO.successdata("操作成功");
	}

	@ApiOperation("详情")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "id", value = "编号", required = true, dataType = "String", paramType="path")
	})
	@GetMapping(value = "/{id}")
	public ResponseVO<OrganizationExtraDTO> selectOne(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@PathVariable("id") String id){

		OrganizationExtra databaseObj = organizationExtraService.selectByPrimaryKey(id);

		return ResponseVO.successdata(databaseObj);
	}

	@ApiOperation("查询")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "organizationExtraDTO", value = "应用查询参数", required = false, dataType = "OrganizationExtraDTO", paramType="body"),
		@ApiImplicitParam(name = "pageNo", value = "页码", required = true, dataType = "int", paramType="query"),
		@ApiImplicitParam(name = "pageSize", value = "每页记录数", required = true, dataType = "int", paramType="query"),
		@ApiImplicitParam(name = "sortBy", value = "按照哪个属性排序", required = false, dataType = "String", paramType="query"),
		@ApiImplicitParam(name = "order", value = "排序顺序", required = false, dataType = "String", paramType="query")
	})
	@PostMapping(value = "/query")
	public ResponseVO<OrganizationExtraDTO>query(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
												@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
												@RequestBody OrganizationExtraDTO organizationExtraDTO,
												@RequestParam Integer pageNo, @RequestParam Integer pageSize,
												@RequestParam(defaultValue = "create_time") String sortBy, @RequestParam(defaultValue = "desc") String order,
												 @LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){
		Map<Object, Object> map = BeanCopyUtilss.beanToMap(organizationExtraDTO, loginedUserVO);
		if(pageSize != 0) {
			Page newPage = new Page(pageNo + "", pageSize + "");
			map.put("page", newPage);
		}
		List<OrganizationExtra> list = organizationExtraService.selectListByMap(map);

		//
		List<OrganizationExtraDTO> dtoList = toDTO(list);

		if(pageSize != 0) {
			Page newPage = new Page(pageNo + "", pageSize + "");
			int countNum = organizationExtraService.selectCountByMap(map);
			newPage.setCount(countNum);
			return ResponseVO.successdatas(dtoList, newPage);
		} else {
			return ResponseVO.successdatas(dtoList, null);
		}

	}

	private List<OrganizationExtraDTO> toDTO(List<OrganizationExtra> databaseList) {
		List<OrganizationExtraDTO> returnList = new ArrayList<OrganizationExtraDTO>();
		if(databaseList != null) {
			for(OrganizationExtra dbEntity : databaseList) {
				OrganizationExtraDTO dtoTemp = new OrganizationExtraDTO();
				BeanCopyUtilss.myCopyProperties(dbEntity, dtoTemp);
				returnList.add(dtoTemp);
			}
		}
		return returnList;
	}

	private List<OrganizationExtra> toDB(List<OrganizationExtraDTO> dtoList) {
		List<OrganizationExtra> returnList = new ArrayList<OrganizationExtra>();
		if(dtoList != null) {
			for(OrganizationExtraDTO dtoEntity : dtoList) {
				OrganizationExtra dbTemp = new OrganizationExtra();
				BeanCopyUtilss.myCopyProperties(dtoEntity, dbTemp);
				returnList.add(dbTemp);
			}
		}
		return returnList;
	}
}
