package com.pap.rbac.organization.service;

import com.pap.rbac.organization.entity.OrganizationExtra;

import java.util.List;
import java.util.Map;

public interface IOrganizationExtraService {

    int deleteByPrimaryKey(String id);

    int selectCountByMap(Map<Object, Object> map);

    List<OrganizationExtra> selectListByMap(Map<Object, Object> map);

    int insert(OrganizationExtra record);

    int insertSelective(OrganizationExtra record);

    OrganizationExtra selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(OrganizationExtra record);

    int updateByPrimaryKey(OrganizationExtra record);

}
