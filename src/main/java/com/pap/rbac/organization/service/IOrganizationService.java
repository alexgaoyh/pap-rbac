package com.pap.rbac.organization.service;

import com.pap.base.annotation.vo.ExtraParamsVO;
import com.pap.obj.vo.response.ResponseVO;
import com.pap.rbac.dto.OrganizationDTO;
import com.pap.rbac.dto.OrganizationTreeNodeVO;
import com.pap.rbac.organization.entity.Organization;

import java.util.List;
import java.util.Map;

public interface IOrganizationService {

    int deleteByPrimaryKey(String id);

    int selectCountByMap(Map<Object, Object> map);

    List<Organization> selectListByMap(Map<Object, Object> map);

    int insert(Organization record);

    int insertSelective(Organization record);

    Organization selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(Organization record);

    int updateByPrimaryKey(Organization record);

    // alexgaoyh add

    /**
     *
     * @param record    组织机构信息
     * @param extraParamsVO 组织机构扩充信息
     * @return
     */
    String insertSelectiveWithExtraParams(Organization record, ExtraParamsVO extraParamsVO);

    /**
     * 批量插入操作
     * @param argList	插入实体
     * @return
     */
    int insertSelectiveList(List<Organization> argList);

    /**
     * 组织机构树形结构，根据 license 获取
     * @param clientLicenseId
     * @param globalParentId 全局统一的默认顶层组织架构的编码为 -1
     */
    List<OrganizationTreeNodeVO> organizationTreeJson(String clientLicenseId, String globalParentId);

    /**
     * 检查当前操作的组织机构编码，对照新维护的上级组织机构，是否被循环依赖
     * @param operaOrganizationId
     * @param newParentOrganizationId
     * @return
     */
    Boolean checkTreeCircularDependency(String operaOrganizationId, String newParentOrganizationId);

    /**
     * 组织机构更新操作,参数校验并且进行数据维护	pathIds pathCodes pathNames 三个字段的数据维护
     * @return
     */
    ResponseVO<Organization> updateAndCheckOrganization(Organization inputOrganization);

    /**
     *
     * @param record
     * @param extraParamsVO
     * @return
     */
    int updateByPrimaryKeySelectiveForExtraParams(Organization record, ExtraParamsVO extraParamsVO);

    /**
     * 维护当前组织机构节点下，包含的子集的组织机构的数据值,冗余字段的维护，
     * pathIds pathCodes pathNames
     * 请确保当前操作的 inputOrganizationId，对应的实体数据已经是最新的。
     *
     * 递归调用，维护完毕所有子类的path冗余字段
     * @param inputOrganizationId
     * @return
     */
    ResponseVO<Organization> updateRecursionPathColumnOrganization(String inputOrganizationId);


    /**
     *  根据organizationId 编号查询出来实体信息，含extar扩展信息
     * @param organizationId
     * @return
     */
    OrganizationDTO selectDTOByPrimaryKey(String organizationId);

    /**
     * 删除(包含扩展信息删除)
     * @param id
     * @return
     */
    int deleteWithExtraParamsByPrimaryKey(String id);
}
