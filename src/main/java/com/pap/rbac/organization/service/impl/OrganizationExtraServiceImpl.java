package com.pap.rbac.organization.service.impl;

import com.pap.beans.idworker.IdWorker;
import com.pap.rbac.organization.entity.OrganizationExtra;
import com.pap.rbac.organization.mapper.OrganizationExtraMapper;
import com.pap.rbac.organization.service.IOrganizationExtraService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Transactional
@Service("organizationExtraService")
public class OrganizationExtraServiceImpl implements IOrganizationExtraService {

    @Resource(name = "idWorker")
    private IdWorker idWorker;

    @Resource(name = "organizationExtraMapper")
    private OrganizationExtraMapper mapper;

    @Override
    public int deleteByPrimaryKey(String id) {
       return mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int selectCountByMap(Map<Object, Object> map) {
      return mapper.selectCountByMap(map);
    }

    @Override
    public List<OrganizationExtra> selectListByMap(Map<Object, Object> map) {
        return mapper.selectListByMap(map);
    }

    @Override
    public int insert(OrganizationExtra record) {
       return mapper.insert(record);
    }

    @Override
    public int insertSelective(OrganizationExtra record) {
       return mapper.insertSelective(record);
    }

    @Override
    public OrganizationExtra selectByPrimaryKey(String id) {
       return mapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(OrganizationExtra record) {
       return mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(OrganizationExtra record) {
      return mapper.updateByPrimaryKey(record);
    }

}