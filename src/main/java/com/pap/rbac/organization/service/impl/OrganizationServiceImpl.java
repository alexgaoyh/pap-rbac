package com.pap.rbac.organization.service.impl;

import com.pap.base.annotation.vo.ExtraParam;
import com.pap.base.annotation.vo.ExtraParamsVO;
import com.pap.base.util.bean.BeanCopyUtilss;
import com.pap.base.util.date.DateUtils;
import com.pap.base.util.string.StringUtilss;
import com.pap.beans.idworker.IdWorker;
import com.pap.obj.vo.response.ResponseVO;
import com.pap.rbac.dto.OrganizationDTO;
import com.pap.rbac.dto.OrganizationTreeNodeVO;
import com.pap.rbac.organization.entity.Organization;
import com.pap.rbac.organization.entity.OrganizationExtra;
import com.pap.rbac.organization.mapper.OrganizationExtraMapper;
import com.pap.rbac.organization.mapper.OrganizationMapper;
import com.pap.rbac.organization.service.IOrganizationService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Transactional
@Service("organizationService")
public class OrganizationServiceImpl implements IOrganizationService {

    @Resource(name = "idWorker")
    private IdWorker idWorker;

    @Resource(name = "organizationMapper")
    private OrganizationMapper mapper;

    @Resource(name = "organizationExtraMapper")
    private OrganizationExtraMapper organizationExtraMapper;

    @Override
    public int deleteByPrimaryKey(String id) {
       return mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int selectCountByMap(Map<Object, Object> map) {
      return mapper.selectCountByMap(map);
    }

    @Override
    public List<Organization> selectListByMap(Map<Object, Object> map) {
        return mapper.selectListByMap(map);
    }

    @Override
    public int insert(Organization record) {
       return mapper.insert(record);
    }

    @Override
    public int insertSelective(Organization record) {
       return mapper.insertSelective(record);
    }

    @Override
    public Organization selectByPrimaryKey(String id) {
       return mapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(Organization record) {
       return mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Organization record) {
      return mapper.updateByPrimaryKey(record);
    }

    @Override
    public String insertSelectiveWithExtraParams(Organization record, ExtraParamsVO extraParamsVO) {

        mapper.insertSelective(record);

        if(extraParamsVO != null) {
            List<ExtraParam> list = extraParamsVO.getDetails();
            for(ExtraParam extraParam : list) {
                OrganizationExtra organizationExtra = new OrganizationExtra();
                organizationExtra.setOrganizationExtraId(idWorker.nextIdStr());
                organizationExtra.setOrganizationId(record.getOrganizationId());
                organizationExtra.setAttrKey(extraParam.getAttrKey());
                organizationExtra.setAttrValue(extraParam.getAttrValue());
                organizationExtra.setCreateTime(DateUtils.getCurrDateTimeStr());
                organizationExtra.setCreateUser(record.getCreateUser());
                organizationExtra.setCreateIp(record.getCreateIp());
                organizationExtra.setClientLicenseId(record.getClientLicenseId());

                organizationExtraMapper.insertSelective(organizationExtra);
            }
        }

        return record.getOrganizationId();
    }

    @Override
    public int insertSelectiveList(List<Organization> argList) {
        int returnInt = 0;
        if(argList != null && argList.size() > 0) {
            returnInt = argList.size();
            for(Organization organization : argList) {
                mapper.insertSelective(organization);
            }
        }
        return returnInt;
    }

    @Override
    public List<OrganizationTreeNodeVO> organizationTreeJson(String clientLicenseId, String globalParentId) {
        return mapper.organizationTreeJson(clientLicenseId, globalParentId);
    }

    @Override
    public ResponseVO<Organization> updateAndCheckOrganization(Organization inputOrganization) {
        // 检验是否循环依赖
        if(checkTreeCircularDependency(inputOrganization.getOrganizationId(), inputOrganization.getOrganizationParentId())) {

            // 忽略前台传递过来的数据
            inputOrganization.setPathIds(null);
            inputOrganization.setPathCodes(null);
            inputOrganization.setPathNames(null);
            mapper.updateByPrimaryKeySelective(inputOrganization);

            Organization tempForPath = mapper.selectByPrimaryKey(inputOrganization.getOrganizationId());
            if("-1".equals(inputOrganization.getOrganizationParentId())) {
                tempForPath.setPathCodes(null);
                tempForPath.setPathIds(null);
                tempForPath.setPathNames(null);

                tempForPath.setOrganizationParentId("-1");
                mapper.updateByPrimaryKey(tempForPath);
            } else {
                // 查询父节点信息，处理当前节点的冗余字段
                Organization tempParentForPath = mapper.selectByPrimaryKey(inputOrganization.getOrganizationParentId());
                if(tempParentForPath != null) {
                    if(StringUtilss.isNotEmpty(tempParentForPath.getPathIds())) {
                        tempForPath.setPathIds(tempParentForPath.getPathIds() + "," + tempParentForPath.getOrganizationId());
                    } else {
                        tempForPath.setPathIds(tempParentForPath.getOrganizationId());
                    }
                    if(StringUtilss.isNotEmpty(tempParentForPath.getPathCodes())) {
                        tempForPath.setPathCodes(tempParentForPath.getPathCodes() + "," + tempParentForPath.getOrganizationCode());
                    } else {
                        tempForPath.setPathCodes(tempParentForPath.getOrganizationCode());
                    }
                    if(StringUtilss.isNotEmpty(tempParentForPath.getPathNames())) {
                        tempForPath.setPathNames(tempParentForPath.getPathNames() + "," + tempParentForPath.getOrganizationName());
                    } else {
                        tempForPath.setPathNames(tempParentForPath.getOrganizationName());
                    }
                } else {
                    tempForPath.setOrganizationParentId("-1");
                    tempForPath.setPathCodes(null);
                    tempForPath.setPathIds(null);
                    tempForPath.setPathNames(null);
                }

                mapper.updateByPrimaryKey(tempForPath);
            }

            // 维护子集的组织机构
            ResponseVO<Organization> responseVO =  updateRecursionPathColumnOrganization(inputOrganization.getOrganizationId());
            return responseVO;
        } else {
            return ResponseVO.validfail("组织机构被循环依赖，请检查数据有效性!");
        }
    }

    @Override
    public int updateByPrimaryKeySelectiveForExtraParams(Organization record, ExtraParamsVO extraParamsVO) {

        //删除所有，再插入最新的
        organizationExtraMapper.deleteByOrganizationId(record.getOrganizationId());
        if(extraParamsVO != null) {
            List<ExtraParam> list = extraParamsVO.getDetails();
            for(ExtraParam extraParam : list) {
                OrganizationExtra organizationExtra = new OrganizationExtra();
                organizationExtra.setOrganizationExtraId(idWorker.nextIdStr());
                organizationExtra.setOrganizationId(record.getOrganizationId());
                organizationExtra.setAttrKey(extraParam.getAttrKey());
                organizationExtra.setAttrValue(extraParam.getAttrValue());
                organizationExtra.setCreateTime(DateUtils.getCurrDateTimeStr());
                organizationExtra.setCreateUser(record.getCreateUser());
                organizationExtra.setCreateIp(record.getCreateIp());
                organizationExtra.setClientLicenseId(record.getClientLicenseId());

                organizationExtraMapper.insertSelective(organizationExtra);
            }
        }

        return 0;
    }

    /**
     * 新维护的组织机构，是否被循环依赖的校验
     * @param operaOrganizationId
     * @param newParentOrganizationId
     * @return
     */
    @Override
    public Boolean checkTreeCircularDependency(String operaOrganizationId, String newParentOrganizationId) {
        String parentOrganizationTemp = "";
        Organization newParentOrganizationInfo = mapper.selectByPrimaryKey(newParentOrganizationId);
        if(newParentOrganizationInfo != null) {
            if(StringUtilss.isNotEmpty(newParentOrganizationInfo.getPathIds())) {
                parentOrganizationTemp = newParentOrganizationInfo.getPathIds() + ",";
            }
            parentOrganizationTemp += newParentOrganizationInfo.getOrganizationId();
        }
        if (StringUtilss.checkArrayValue(parentOrganizationTemp.split(","), operaOrganizationId)) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public ResponseVO<Organization> updateRecursionPathColumnOrganization(String inputOrganizationId) {
        try {
            Map<Object, Object> parentIdMap = new HashMap<Object, Object>();
            parentIdMap.put("organizationParentId", inputOrganizationId);
            List<Organization> list = mapper.selectListByMap(parentIdMap);
            if (null != list && list.size()>0) {
                for (int i = 0; i < list.size(); i++) {
                    Organization tempForPath = list.get(i);
                    // 更新数据
                    Organization tempParentForPath = mapper.selectByPrimaryKey(tempForPath.getOrganizationParentId());
                    if(tempParentForPath != null) {
                        if(StringUtilss.isNotEmpty(tempParentForPath.getPathIds())) {
                            tempForPath.setPathIds(tempParentForPath.getPathIds() + "," + tempParentForPath.getOrganizationId());
                        } else {
                            tempForPath.setPathIds(tempParentForPath.getOrganizationId());
                        }
                        if(StringUtilss.isNotEmpty(tempParentForPath.getPathCodes())) {
                            tempForPath.setPathCodes(tempParentForPath.getPathCodes() + "," + tempParentForPath.getOrganizationCode());
                        } else {
                            tempForPath.setPathCodes(tempParentForPath.getOrganizationCode());
                        }
                        if(StringUtilss.isNotEmpty(tempParentForPath.getPathNames())) {
                            tempForPath.setPathNames(tempParentForPath.getPathNames() + "," + tempParentForPath.getOrganizationName());
                        } else {
                            tempForPath.setPathNames(tempParentForPath.getOrganizationName());
                        }

                    } else {
                        tempForPath.setPathCodes(null);
                        tempForPath.setPathIds(null);
                        tempForPath.setPathNames(null);
                    }
                    mapper.updateByPrimaryKeySelective(tempForPath);
                    updateRecursionPathColumnOrganization(tempForPath.getOrganizationId());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseVO.validfail(e.getMessage());
        }
        return ResponseVO.successdata("更新成功");
    }

    @Override
    public OrganizationDTO selectDTOByPrimaryKey(String organizationId) {
        OrganizationDTO organizationDTO = new OrganizationDTO();

        Organization organization = mapper.selectByPrimaryKey(organizationId);
        BeanCopyUtilss.myCopyProperties(organization, organizationDTO);

        Map<Object, Object> organizationExtraMap = new HashMap<Object, Object>();
        organizationExtraMap.put("organizationId", organizationId);
        List<OrganizationExtra> list = organizationExtraMapper.selectListByMap(organizationExtraMap);

        Map<String, String> extraMap = extraToMap(list);
        organizationDTO.setExtraParams(extraMap);

        return organizationDTO;
    }

    @Override
    public int deleteWithExtraParamsByPrimaryKey(String id) {
        mapper.deleteByPrimaryKey(id);
        organizationExtraMapper.deleteByOrganizationId(id);
        return 1;
    }

    private Map<String, String> extraToMap(List<OrganizationExtra> organizationExtraList) {
        Map<String, String> returnExtraMap = new HashMap<String, String>();
        if(organizationExtraList != null && organizationExtraList.size() > 0) {
            for (OrganizationExtra organizationExtra : organizationExtraList) {
                returnExtraMap.put(organizationExtra.getAttrKey(), organizationExtra.getAttrValue());
            }
        }
        return returnExtraMap;
    }
}