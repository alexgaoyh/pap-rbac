package com.pap.rbac.organization.mapper;

import com.pap.base.mapper.PapBaseMapper;
import com.pap.rbac.organization.entity.OrganizationExtra;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;;

@Mapper
public interface OrganizationExtraMapper extends PapBaseMapper<OrganizationExtra> {
    int deleteByPrimaryKey(String organizationExtraId);

    int selectCountByMap(Map<Object, Object> map);

    List<OrganizationExtra> selectListByMap(Map<Object, Object> map);

    OrganizationExtra selectByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int deleteByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int insert(OrganizationExtra record);

    int insertSelective(OrganizationExtra record);

    OrganizationExtra selectByPrimaryKey(String organizationExtraId);

    int updateByPrimaryKeySelective(OrganizationExtra record);

    int updateByPrimaryKey(OrganizationExtra record);

    // alexgaoyh added

    int deleteByOrganizationId(@Param("organizationId") String organizationId);
}