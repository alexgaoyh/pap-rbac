package com.pap.rbac.organization.mapper;

import com.pap.base.mapper.PapBaseMapper;
import com.pap.rbac.dto.OrganizationTreeNodeVO;
import com.pap.rbac.organization.entity.Organization;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;;

@Mapper
public interface OrganizationMapper extends PapBaseMapper<Organization> {
    int deleteByPrimaryKey(String organizationId);

    int selectCountByMap(Map<Object, Object> map);

    List<Organization> selectListByMap(Map<Object, Object> map);

    Organization selectByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int deleteByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int insert(Organization record);

    int insertSelective(Organization record);

    Organization selectByPrimaryKey(String organizationId);

    int updateByPrimaryKeySelective(Organization record);

    int updateByPrimaryKey(Organization record);

    // add alexgaoyh
    List<OrganizationTreeNodeVO> organizationTreeJson(@Param("clientLicenseId") String clientLicenseId,
                                                      @Param("globalParentId") String globalParentId);
}