package com.pap.rbac.role.mapper;

import com.pap.base.mapper.PapBaseMapper;
import com.pap.rbac.dto.RoleTreeNodeVO;
import com.pap.rbac.role.entity.Role;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;;

@Mapper
public interface RoleMapper extends PapBaseMapper<Role> {
    int deleteByPrimaryKey(String roleId);

    int selectCountByMap(Map<Object, Object> map);

    List<Role> selectListByMap(Map<Object, Object> map);

    Role selectByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int deleteByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int insert(Role record);

    int insertSelective(Role record);

    Role selectByPrimaryKey(String roleId);

    int updateByPrimaryKeySelective(Role record);

    int updateByPrimaryKey(Role record);

    // add alexgaoyh
    List<RoleTreeNodeVO> roleTreeJson(@Param("clientLicenseId") String clientLicenseId,
                                      @Param("globalParentId") String globalParentId);
}