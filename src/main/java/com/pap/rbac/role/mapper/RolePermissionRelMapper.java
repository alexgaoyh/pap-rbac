package com.pap.rbac.role.mapper;

import com.pap.base.mapper.PapBaseMapper;
import com.pap.rbac.role.entity.RolePermissionRel;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface RolePermissionRelMapper extends PapBaseMapper<RolePermissionRel> {
    int deleteByPrimaryKey(String rolePermissionRelId);

    int selectCountByMap(Map<Object, Object> map);

    List<RolePermissionRel> selectListByMap(Map<Object, Object> map);

    RolePermissionRel selectByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int deleteByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int insert(RolePermissionRel record);

    int insertSelective(RolePermissionRel record);

    RolePermissionRel selectByPrimaryKey(String rolePermissionRelId);

    int updateByPrimaryKeySelective(RolePermissionRel record);

    int updateByPrimaryKey(RolePermissionRel record);


    // alexgaoyh

    int deleteByRoleId(@Param("roleId") String roleId);

    /**
     * 根据角色 roleId ,将包含的权限组编号集合进行返回
     * @param roleId
     * @return
     */
    List<String> selectPermissionGroupIdsByRoleId(@Param("roleId") String roleId);
}