package com.pap.rbac.role.controller;

import com.pap.base.util.bean.BeanCopyUtilss;
import com.pap.beans.idworker.IdWorker;
import com.pap.obj.vo.logineduser.LoginedUser;
import com.pap.obj.vo.logineduser.LoginedUserVO;
import com.pap.obj.vo.page.Page;
import com.pap.obj.vo.response.ResponseVO;
import com.pap.rbac.dto.RolePermissionRelDTO;
import com.pap.rbac.dto.RolePermissionRelDetailsDTO;
import com.pap.rbac.role.entity.RolePermissionRel;
import com.pap.rbac.role.service.IRolePermissionRelService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/rolepermissionrel")
@Api(value = "角色权限关联关系管理", tags = "角色权限关联关系管理", description="角色权限关联关系管理")
public class RolePermissionRelController {

	private static Logger logger  =  LoggerFactory.getLogger(RolePermissionRelController.class);
	
	@Resource(name = "rolePermissionRelService")
	private IRolePermissionRelService rolePermissionRelService;

	@Resource(name = "idWorker")
	private IdWorker idWorker;


	@ApiOperation("保存角色与权限管理关系")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "rolePermissionRelDetailsDTO", value = "", required = true, dataType = "RolePermissionRelDetailsDTO", paramType="body")
	})
	@PostMapping(value = "/saverels")
	public ResponseVO<String> saveRels(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
									   @RequestHeader(value = "papVersion", defaultValue = "-1") String papVersion,
									   @RequestBody RolePermissionRelDetailsDTO rolePermissionRelDetailsDTO,
									   @LoginedUser @ApiIgnore LoginedUserVO loginedUserVO) throws Exception {

		rolePermissionRelService.updateUserPermissionRel(loginedUserVO, rolePermissionRelDetailsDTO.getRoleId(), rolePermissionRelDetailsDTO.getDetails());
		return ResponseVO.successdata("操作成功");
	}

	@ApiOperation("根据角色编号查询权限信息")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "roleId", value = "角色编号", required = true, dataType = "String", paramType="query")
	})
	@GetMapping(value = "/selectpermissiongroupidsbyroleid")
	public ResponseVO<String> selectPermissiongroupIdsbyRoleId(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
																			@RequestHeader(value = "papVersion", defaultValue = "-1") String papVersion,
																			@RequestParam String roleId) throws Exception {
		List<String> organizationGroupIdList = rolePermissionRelService.selectPermissionGroupIdsByRoleId(roleId);

		return ResponseVO.successdatas(organizationGroupIdList, null);
	}
	
	@ApiOperation("创建")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "rolePermissionRelDTO", value = "", required = true, dataType = "RolePermissionRelDTO", paramType="body")
	})
	@PostMapping(value = "")
	@Deprecated
	public ResponseVO<RolePermissionRelDTO> create(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
												   @RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
												   @RequestBody RolePermissionRelDTO rolePermissionRelDTO,
												   @LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){
		String idStr = idWorker.nextIdStr();
		// TODO 设置主键
		rolePermissionRelDTO.setRolePermissionRelId(idStr);

		RolePermissionRel databaseObj = new RolePermissionRel();
		BeanCopyUtilss.myCopyProperties(rolePermissionRelDTO, databaseObj, loginedUserVO);

		int i = rolePermissionRelService.insertSelective(databaseObj);

		return ResponseVO.successdata(rolePermissionRelDTO);
	}

	@ApiOperation("更新")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "rolePermissionRelDTO", value = "", required = true, dataType = "RolePermissionRelDTO", paramType="body")
	})
	@PutMapping(value = "")
	@Deprecated
	public ResponseVO<RolePermissionRelDTO> update(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@RequestBody RolePermissionRelDTO rolePermissionRelDTO,
													@LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){

		RolePermissionRel databaseObj = new RolePermissionRel();
		BeanCopyUtilss.myCopyProperties(rolePermissionRelDTO, databaseObj, loginedUserVO, false);
		int i = rolePermissionRelService.updateByPrimaryKeySelective(databaseObj);

		// TODO 主键部分
		databaseObj = rolePermissionRelService.selectByPrimaryKey(databaseObj.getRolePermissionRelId());

		return ResponseVO.successdata(databaseObj);
	}


	@ApiOperation("删除")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "rolePermissionRelDTO", value = "", required = true, dataType = "RolePermissionRelDTO", paramType="body")
	})
	@DeleteMapping(value = "")
	@Deprecated
	public ResponseVO<String> delete(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@RequestBody RolePermissionRelDTO rolePermissionRelDTO,
													@LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){

		RolePermissionRel databaseObj = new RolePermissionRel();
		BeanCopyUtilss.myCopyProperties(rolePermissionRelDTO, databaseObj);

		// TODO 主键部分
		int i = rolePermissionRelService.deleteByPrimaryKey(databaseObj.getRolePermissionRelId());

		return ResponseVO.successdata("操作成功");
	}

	@ApiOperation("详情")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "id", value = "编号", required = true, dataType = "String", paramType="path")
	})
	@GetMapping(value = "/{id}")
	@Deprecated
	public ResponseVO<RolePermissionRelDTO> selectOne(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@PathVariable("id") String id){

		RolePermissionRel databaseObj = rolePermissionRelService.selectByPrimaryKey(id);

		return ResponseVO.successdata(databaseObj);
	}

	@ApiOperation("查询")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "rolePermissionRelDTO", value = "应用查询参数", required = false, dataType = "RolePermissionRelDTO", paramType="body"),
		@ApiImplicitParam(name = "pageNo", value = "页码", required = true, dataType = "int", paramType="query"),
		@ApiImplicitParam(name = "pageSize", value = "每页记录数", required = true, dataType = "int", paramType="query"),
		@ApiImplicitParam(name = "sortBy", value = "按照哪个属性排序", required = false, dataType = "String", paramType="query"),
		@ApiImplicitParam(name = "order", value = "排序顺序", required = false, dataType = "String", paramType="query")
	})
	@PostMapping(value = "/query")
	@Deprecated
	public ResponseVO<RolePermissionRelDTO>query(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
												@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
												@RequestBody RolePermissionRelDTO rolePermissionRelDTO,
												@RequestParam Integer pageNo, @RequestParam Integer pageSize,
												@RequestParam(defaultValue = "create_time") String sortBy, @RequestParam(defaultValue = "desc") String order,
												@LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){
		Map<Object, Object> map = BeanCopyUtilss.beanToMap(rolePermissionRelDTO, loginedUserVO);
		if(pageSize != 0) {
			Page newPage = new Page(pageNo + "", pageSize + "");
			map.put("page", newPage);
		}
		List<RolePermissionRel> list = rolePermissionRelService.selectListByMap(map);

		//
		List<RolePermissionRelDTO> dtoList = toDTO(list);

		if(pageSize != 0) {
			Page newPage = new Page(pageNo + "", pageSize + "");
			int countNum = rolePermissionRelService.selectCountByMap(map);
			newPage.setCount(countNum);
			return ResponseVO.successdatas(dtoList, newPage);
		} else {
			return ResponseVO.successdatas(dtoList, null);
		}

	}

	private List<RolePermissionRelDTO> toDTO(List<RolePermissionRel> databaseList) {
		List<RolePermissionRelDTO> returnList = new ArrayList<RolePermissionRelDTO>();
		if(databaseList != null) {
			for(RolePermissionRel dbEntity : databaseList) {
				RolePermissionRelDTO dtoTemp = new RolePermissionRelDTO();
				BeanCopyUtilss.myCopyProperties(dbEntity, dtoTemp);
				returnList.add(dtoTemp);
			}
		}
		return returnList;
	}

	private List<RolePermissionRel> toDB(List<RolePermissionRelDTO> dtoList) {
		List<RolePermissionRel> returnList = new ArrayList<RolePermissionRel>();
		if(dtoList != null) {
			for(RolePermissionRelDTO dtoEntity : dtoList) {
				RolePermissionRel dbTemp = new RolePermissionRel();
				BeanCopyUtilss.myCopyProperties(dtoEntity, dbTemp);
				returnList.add(dbTemp);
			}
		}
		return returnList;
	}
}
