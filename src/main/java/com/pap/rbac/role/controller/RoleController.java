package com.pap.rbac.role.controller;

import com.github.stuxuhai.jpinyin.PinyinFormat;
import com.github.stuxuhai.jpinyin.PinyinHelper;
import com.pap.base.util.bean.BeanCopyUtilss;
import com.pap.base.util.string.StringUtilss;
import com.pap.beans.idworker.IdWorker;
import com.pap.obj.vo.business.StatusFlagEnum;
import com.pap.obj.vo.logineduser.LoginedUser;
import com.pap.obj.vo.logineduser.LoginedUserVO;
import com.pap.obj.vo.page.Page;
import com.pap.obj.vo.response.ResponseVO;
import com.pap.rbac.dto.RoleDTO;
import com.pap.rbac.dto.RoleTreeNodeVO;
import com.pap.rbac.role.entity.Role;
import com.pap.rbac.role.service.IRoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/role")
@Api(value = "用户角色", tags = "用户角色", description="用户角色")
public class RoleController {

	private static Logger logger  =  LoggerFactory.getLogger(RoleController.class);
	
	@Resource(name = "roleService")
	private IRoleService roleService;

	@Resource(name = "idWorker")
	private IdWorker idWorker;

	@RequestMapping(value = "/treejson", method = {RequestMethod.GET})
	public ResponseVO<List<RoleTreeNodeVO>> treeJSON(@LoginedUser @ApiIgnore LoginedUserVO loginedUserVO) {
		//  全局默认角色的顶层编码为 -1
		String globalParentId = "-1";
		return ResponseVO.successdatas(roleService.roleTreeJson(loginedUserVO != null ? loginedUserVO.getClientLicenseId() : "", globalParentId), null);
	}

	@ApiOperation("创建")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "roleDTO", value = "", required = true, dataType = "RoleDTO", paramType="body")
	})
	@PostMapping(value = "")
	public ResponseVO<RoleDTO> create(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
									  @RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
									  @RequestBody RoleDTO roleDTO,
									  @LoginedUser @ApiIgnore LoginedUserVO loginedUserVO) throws Exception {
		String idStr = idWorker.nextIdStr();

		roleDTO.setRoleId(idStr);

		if(roleDTO != null &&
				StringUtilss.isNotEmpty(roleDTO.getRoleName())) {
			// 如果前端没有传递过来code值，则默认使用拼音代替
			if(StringUtilss.isEmpty(roleDTO.getRoleCode())) {
				roleDTO.setRoleCode(PinyinHelper.convertToPinyinString(roleDTO.getRoleName(), "", PinyinFormat.WITHOUT_TONE));
			}
			roleDTO.setLevel("0");
			// 处理父级数据
			Role parentRole = roleService.selectByPrimaryKey(roleDTO.getRoleParentId());
			if(parentRole != null) {
				// pathId
				if(StringUtilss.isNotEmpty(parentRole.getPathIds())) {
					roleDTO.setPathIds(parentRole.getPathIds() + "," + parentRole.getRoleId());
				} else {
					roleDTO.setPathIds(parentRole.getRoleId());
				}
				// pathCode
				if(StringUtilss.isNotEmpty(parentRole.getPathCodes())) {
					roleDTO.setPathCodes(parentRole.getPathCodes() + "," + parentRole.getRoleCode());
				} else {
					roleDTO.setPathCodes(parentRole.getRoleCode());
				}
				// pathName
				if(StringUtilss.isNotEmpty(parentRole.getPathNames())) {
					roleDTO.setPathNames(parentRole.getPathNames() + "," + parentRole.getRoleName());
				} else {
					roleDTO.setPathNames(parentRole.getRoleName());
				}
				// level
				if(StringUtilss.isNotEmpty(parentRole.getLevel())) {
					roleDTO.setLevel((Integer.parseInt(parentRole.getLevel()) + 1) + "");
				} else {
					roleDTO.setLevel("0");
				}
			} else {
				roleDTO.setRoleParentId(null);
				roleDTO.setPathIds(null);
				roleDTO.setPathCodes(null);
				roleDTO.setPathNames(null);
				roleDTO.setLevel("0");
			}
			roleDTO.setStatus(StatusFlagEnum.YES.getKey());
			roleDTO.setRoleParentId(StringUtilss.isNotEmpty(roleDTO.getRoleParentId()) == true ? roleDTO.getRoleParentId() : "-1");


			Role dbRole = new Role();
			BeanCopyUtilss.myCopyProperties(roleDTO, dbRole, loginedUserVO, true);
			int operationInt = roleService.insertSelective(dbRole);

			return ResponseVO.successdata(roleDTO);

		} else {
			return ResponseVO.validfail("入参异常，请判断是否含有角色名称");
		}
	}

	@ApiOperation("更新")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "roleDTO", value = "", required = true, dataType = "RoleDTO", paramType="body")
	})
	@PutMapping(value = "")
	public ResponseVO<RoleDTO> update(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@RequestBody RoleDTO roleDTO,
									  @LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){

		Role dbRole = new Role();
		BeanCopyUtilss.myCopyProperties(roleDTO, dbRole, loginedUserVO, false);

		ResponseVO responseVO = roleService.updateAndCheckRole(dbRole);
		if(responseVO.getCode().equals("200")) {
			RoleDTO roleDTOTemp = new RoleDTO();
			BeanCopyUtilss.myCopyProperties(responseVO.getData(), roleDTOTemp);
			return ResponseVO.successdata(roleDTOTemp);
		} else {
			return responseVO;
		}
	}


	@ApiOperation("删除")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "roleDTO", value = "", required = true, dataType = "RoleDTO", paramType="body")
	})
	@DeleteMapping(value = "")
	public ResponseVO<String> delete(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@RequestBody RoleDTO roleDTO){

		Role databaseObj = new Role();
		BeanCopyUtilss.myCopyProperties(roleDTO, databaseObj);
		if(databaseObj != null && StringUtilss.isNotEmpty(databaseObj.getRoleId())) {
			Map<Object, Object> pathIdsMap = new HashMap<Object, Object>();
			pathIdsMap.put("myLike_pathIds", databaseObj.getRoleId());
			List<Role> roleList = roleService.selectListByMap(pathIdsMap);
			if(roleList != null && roleList.size() > 0) {
				return ResponseVO.validfail("请检查入参，此角色被其他角色所依赖!");
			} else {
				int operationInt = roleService.deleteByPrimaryKey(databaseObj.getRoleId());
				return ResponseVO.successdata("操作成功");
			}
		} else {
			return ResponseVO.validfail("入参异常，请判断是否含有角色编码");
		}
	}

	@ApiOperation("详情")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "id", value = "编号", required = true, dataType = "String", paramType="path")
	})
	@GetMapping(value = "/{id}")
	public ResponseVO<RoleDTO> selectOne(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@PathVariable("id") String id){

		Role databaseObj = roleService.selectByPrimaryKey(id);

		return ResponseVO.successdata(databaseObj);
	}

	@ApiOperation("查询")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "roleDTO", value = "应用查询参数", required = false, dataType = "RoleDTO", paramType="body"),
		@ApiImplicitParam(name = "pageNo", value = "页码", required = true, dataType = "int", paramType="query"),
		@ApiImplicitParam(name = "pageSize", value = "每页记录数", required = true, dataType = "int", paramType="query"),
		@ApiImplicitParam(name = "sortBy", value = "按照哪个属性排序", required = false, dataType = "String", paramType="query"),
		@ApiImplicitParam(name = "order", value = "排序顺序", required = false, dataType = "String", paramType="query")
	})
	@PostMapping(value = "/query")
	public ResponseVO<RoleDTO>query(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
												@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
												@RequestBody RoleDTO roleDTO,
												@RequestParam Integer pageNo, @RequestParam Integer pageSize,
												@RequestParam(defaultValue = "create_time") String sortBy, @RequestParam(defaultValue = "desc") String order,
									@LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){
		Map<Object, Object> map = BeanCopyUtilss.beanToMap(roleDTO, loginedUserVO);
		if(pageSize != 0) {
			Page newPage = new Page(pageNo + "", pageSize + "");
			map.put("page", newPage);
		}
		List<Role> list = roleService.selectListByMap(map);

		//
		List<RoleDTO> dtoList = toDTO(list);

		if(pageSize != 0) {
			Page newPage = new Page(pageNo + "", pageSize + "");
			int countNum = roleService.selectCountByMap(map);
			newPage.setCount(countNum);
			return ResponseVO.successdatas(dtoList, newPage);
		} else {
			return ResponseVO.successdatas(dtoList, null);
		}

	}

	private List<RoleDTO> toDTO(List<Role> databaseList) {
		List<RoleDTO> returnList = new ArrayList<RoleDTO>();
		if(databaseList != null) {
			for(Role dbEntity : databaseList) {
				RoleDTO dtoTemp = new RoleDTO();
				BeanCopyUtilss.myCopyProperties(dbEntity, dtoTemp);
				returnList.add(dtoTemp);
			}
		}
		return returnList;
	}

	private List<Role> toDB(List<RoleDTO> dtoList) {
		List<Role> returnList = new ArrayList<Role>();
		if(dtoList != null) {
			for(RoleDTO dtoEntity : dtoList) {
				Role dbTemp = new Role();
				BeanCopyUtilss.myCopyProperties(dtoEntity, dbTemp);
				returnList.add(dbTemp);
			}
		}
		return returnList;
	}
}
