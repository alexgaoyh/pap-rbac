package com.pap.rbac.role.service.impl;

import com.pap.base.util.date.DateUtils;
import com.pap.base.util.string.StringUtilss;
import com.pap.obj.vo.response.ResponseVO;
import com.pap.rbac.dto.RoleTreeNodeVO;
import com.pap.rbac.role.mapper.RoleMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pap.rbac.role.entity.Role;
import com.pap.rbac.role.service.IRoleService;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Transactional
@Service("roleService")
public class RoleServiceImpl implements IRoleService {

    @Resource(name = "roleMapper")
    private RoleMapper mapper;

    @Override
    public int deleteByPrimaryKey(String id) {
       return mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int selectCountByMap(Map<Object, Object> map) {
      return mapper.selectCountByMap(map);
    }

    @Override
    public List<Role> selectListByMap(Map<Object, Object> map) {
        return mapper.selectListByMap(map);
    }

    @Override
    public int insert(Role record) {
       return mapper.insert(record);
    }

    @Override
    public int insertSelective(Role record) {
       return mapper.insertSelective(record);
    }

    @Override
    public Role selectByPrimaryKey(String id) {
       return mapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(Role record) {
       return mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Role record) {
      return mapper.updateByPrimaryKey(record);
    }

    @Override
    public List<RoleTreeNodeVO> roleTreeJson(String clientLicenseId, String globalParentId) {
        return mapper.roleTreeJson(clientLicenseId, globalParentId);
    }

    @Override
    public ResponseVO<Role> updateAndCheckRole(Role inputRole) {
        // 检验是否循环依赖
        if(checkTreeCircularDependency(inputRole.getRoleId(), inputRole.getRoleParentId())) {

            // 忽略前台传递过来的数据
            inputRole.setPathIds(null);
            inputRole.setPathCodes(null);
            inputRole.setPathNames(null);
            mapper.updateByPrimaryKeySelective(inputRole);

            Role tempForPath = mapper.selectByPrimaryKey(inputRole.getRoleId());
            if("-1".equals(inputRole.getRoleParentId())) {
                tempForPath.setPathCodes(null);
                tempForPath.setPathIds(null);
                tempForPath.setPathNames(null);
                tempForPath.setLevel("0");
                mapper.updateByPrimaryKey(tempForPath);
            } else {
                Role tempParentForPath = mapper.selectByPrimaryKey(inputRole.getRoleParentId());
                if(tempParentForPath != null) {
                    if(StringUtilss.isNotEmpty(tempParentForPath.getPathIds())) {
                        tempForPath.setPathIds(tempParentForPath.getPathIds() + "," + tempParentForPath.getRoleId());
                    } else {
                        tempForPath.setPathIds(tempParentForPath.getRoleId());
                    }
                    if(StringUtilss.isNotEmpty(tempParentForPath.getPathCodes())) {
                        tempForPath.setPathCodes(tempParentForPath.getPathCodes() + "," + tempParentForPath.getRoleCode());
                    } else {
                        tempForPath.setPathCodes(tempParentForPath.getRoleCode());
                    }
                    if(StringUtilss.isNotEmpty(tempParentForPath.getPathNames())) {
                        tempForPath.setPathNames(tempParentForPath.getPathNames() + "," + tempParentForPath.getRoleName());
                    } else {
                        tempForPath.setPathNames(tempParentForPath.getRoleName());
                    }
                    // level
                    if(StringUtilss.isNotEmpty(tempParentForPath.getLevel())) {
                        tempForPath.setLevel((Integer.parseInt(tempParentForPath.getLevel()) + 1) + "");
                    } else {
                        tempForPath.setLevel("0");
                    }
                } else {
                    tempForPath.setRoleParentId("-1");
                    tempForPath.setPathCodes(null);
                    tempForPath.setPathIds(null);
                    tempForPath.setPathNames(null);
                    tempForPath.setLevel("0");
                }

                mapper.updateByPrimaryKey(tempForPath);
            }

            // 维护子集的角色
            ResponseVO responseVO =  updateRecursionPathColumnRole(inputRole.getRoleId());
            return ResponseVO.successdata(tempForPath);
        } else {
            return ResponseVO.validfail("角色被循环依赖，请检查数据有效性!");
        }
    }

    /**
     * 新维护的角色，是否被循环依赖的校验
     * @param operaRoleId
     * @param newParentRoleId
     * @return
     */
    @Override
    public Boolean checkTreeCircularDependency(String operaRoleId, String newParentRoleId) {
        String parentRoleTemp = "";
        Role newParentRoleInfo = mapper.selectByPrimaryKey(newParentRoleId);
        if(newParentRoleInfo != null) {
            if(StringUtilss.isNotEmpty(newParentRoleInfo.getPathIds())) {
                parentRoleTemp = newParentRoleInfo.getPathIds() + ",";
            }
            parentRoleTemp += newParentRoleInfo.getRoleId();
        }
        if (StringUtilss.checkArrayValue(parentRoleTemp.split(","), operaRoleId)) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public ResponseVO<Role> updateRecursionPathColumnRole(String inputRoleId) {
        try {
            Map<Object, Object> parentIdMap = new HashMap<Object, Object>();
            parentIdMap.put("roleParentId", inputRoleId);
            List<Role> list = mapper.selectListByMap(parentIdMap);
            if (null != list && list.size()>0) {
                for (int i = 0; i < list.size(); i++) {
                    Role role = list.get(i);
                    // 更新数据
                    Role parentTemp = mapper.selectByPrimaryKey(role.getRoleParentId());
                    if(parentTemp != null) {
                        if(StringUtilss.isNotEmpty(parentTemp.getPathIds())) {
                            role.setPathIds(parentTemp.getPathIds() + "," + parentTemp.getRoleId());
                        } else {
                            role.setPathIds(parentTemp.getRoleId());
                        }
                        if(StringUtilss.isNotEmpty(parentTemp.getPathCodes())) {
                            role.setPathCodes(parentTemp.getPathCodes() + "," + parentTemp.getRoleCode());
                        } else {
                            role.setPathCodes(parentTemp.getRoleCode());
                        }
                        if(StringUtilss.isNotEmpty(parentTemp.getPathNames())) {
                            role.setPathNames(parentTemp.getPathNames() + "," + parentTemp.getRoleName());
                        } else {
                            role.setPathNames(parentTemp.getRoleName());
                        }
                        // level
                        if(StringUtilss.isNotEmpty(parentTemp.getLevel())) {
                            role.setLevel((Integer.parseInt(parentTemp.getLevel()) + 1) + "");
                        } else {
                            role.setLevel("0");
                        }
                    } else {
                        role.setPathCodes(null);
                        role.setPathIds(null);
                        role.setPathNames(null);
                    }
                    mapper.updateByPrimaryKeySelective(role);
                    updateRecursionPathColumnRole(role.getRoleId());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseVO.validfail(e.getMessage());
        }
        return ResponseVO.successdata("更新成功");
    }
}