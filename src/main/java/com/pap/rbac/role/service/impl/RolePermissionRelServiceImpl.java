package com.pap.rbac.role.service.impl;

import com.pap.base.util.date.DateUtils;
import com.pap.beans.idworker.IdWorker;
import com.pap.obj.vo.logineduser.LoginedUserVO;
import com.pap.rbac.role.entity.RolePermissionRel;
import com.pap.rbac.role.mapper.RolePermissionRelMapper;
import com.pap.rbac.role.service.IRolePermissionRelService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Transactional
@Service("rolePermissionRelService")
public class RolePermissionRelServiceImpl implements IRolePermissionRelService {

    @Resource(name = "rolePermissionRelMapper")
    private RolePermissionRelMapper mapper;

    @Resource(name = "idWorker")
    private IdWorker idWorker;

    @Override
    public int deleteByPrimaryKey(String id) {
       return mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int selectCountByMap(Map<Object, Object> map) {
      return mapper.selectCountByMap(map);
    }

    @Override
    public List<RolePermissionRel> selectListByMap(Map<Object, Object> map) {
        return mapper.selectListByMap(map);
    }

    @Override
    public int insert(RolePermissionRel record) {
       return mapper.insert(record);
    }

    @Override
    public int insertSelective(RolePermissionRel record) {
       return mapper.insertSelective(record);
    }

    @Override
    public RolePermissionRel selectByPrimaryKey(String id) {
       return mapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(RolePermissionRel record) {
       return mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(RolePermissionRel record) {
      return mapper.updateByPrimaryKey(record);
    }

    @Override
    public void updateUserPermissionRel(LoginedUserVO loginedUserVO, String roleId, List<String> permissionGroupIdsList) {
        mapper.deleteByRoleId(roleId);
        if(permissionGroupIdsList != null && permissionGroupIdsList.size() > 0) {
            for (String permissionGroupId : permissionGroupIdsList) {
                RolePermissionRel rolePermissionRel = new RolePermissionRel();
                rolePermissionRel.setRolePermissionRelId(idWorker.nextIdStr());
                rolePermissionRel.setPermissionGroupId(permissionGroupId);
                rolePermissionRel.setRoleId(roleId);
                rolePermissionRel.setCreateIp("0.0.0.0");
                rolePermissionRel.setCreateTime(DateUtils.getCurrDateTimeStr());
                rolePermissionRel.setCreateUser(loginedUserVO != null ? loginedUserVO.getId() : "");
                rolePermissionRel.setClientLicenseId(loginedUserVO != null ? loginedUserVO.getClientLicenseId() : "");
                mapper.insertSelective(rolePermissionRel);
            }
        }
    }

    @Override
    public List<String> selectPermissionGroupIdsByRoleId(String roleId) {
        return mapper.selectPermissionGroupIdsByRoleId(roleId);
    }
}