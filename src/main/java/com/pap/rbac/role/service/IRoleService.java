package com.pap.rbac.role.service;

import com.pap.obj.vo.response.ResponseVO;
import com.pap.rbac.dto.RoleTreeNodeVO;
import com.pap.rbac.role.entity.Role;

import java.util.List;
import java.util.Map;

public interface IRoleService {

    int deleteByPrimaryKey(String id);

    int selectCountByMap(Map<Object, Object> map);

    List<Role> selectListByMap(Map<Object, Object> map);

    int insert(Role record);

    int insertSelective(Role record);

    Role selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(Role record);

    int updateByPrimaryKey(Role record);

    /**
     * 角色树形结构，根据 license 获取
     * @param clientLicenseId
     * @param globalParentId 全局统一的默认顶层组织架构的编码为 -1
     */
    List<RoleTreeNodeVO> roleTreeJson(String clientLicenseId, String globalParentId);

    /**
     * 检查当前操作的角色编码，对照新维护的上级角色依赖，是否被循环依赖
     * @param operaRoleId
     * @param newParentRoleId
     * @return
     */
    Boolean checkTreeCircularDependency(String operaRoleId, String newParentRoleId);

    /**
     * 角色更新操作,参数校验并且进行数据维护	pathIds pathCodes pathNames 三个字段的数据维护
     * @return
     */
    ResponseVO<Role> updateAndCheckRole(Role inputRole);

    /**
     * 维护当前角色节点下，包含的子集的角色的数据值,冗余字段的维护，
     * pathIds pathCodes pathNames
     * 请确保当前操作的 inputRoleId，对应的实体数据已经是最新的。
     *
     * 递归调用，维护完毕所有子类的path冗余字段
     * @param inputRoleId
     * @return
     */
    ResponseVO<Role> updateRecursionPathColumnRole(String inputRoleId);
}
