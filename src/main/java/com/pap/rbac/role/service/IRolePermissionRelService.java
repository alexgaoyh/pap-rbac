package com.pap.rbac.role.service;

import com.pap.obj.vo.logineduser.LoginedUserVO;
import com.pap.rbac.role.entity.RolePermissionRel;

import java.util.List;
import java.util.Map;

public interface IRolePermissionRelService {

    int deleteByPrimaryKey(String id);

    int selectCountByMap(Map<Object, Object> map);

    List<RolePermissionRel> selectListByMap(Map<Object, Object> map);

    int insert(RolePermissionRel record);

    int insertSelective(RolePermissionRel record);

    RolePermissionRel selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(RolePermissionRel record);

    int updateByPrimaryKey(RolePermissionRel record);

    // alexgaoyh added

    /**
     * 根据角色编号,更新最新的权限集合
     * @param loginedUserVO
     * @param roleId    角色
     * @param permissionGroupIdsList    权限组编号集合
     */
    void updateUserPermissionRel(LoginedUserVO loginedUserVO, String roleId, List<String> permissionGroupIdsList);

    /**
     * 根据角色 roleId ,将包含的权限组编号集合进行返回
     * @param roleId
     * @return
     */
    List<String> selectPermissionGroupIdsByRoleId(String roleId);
}
