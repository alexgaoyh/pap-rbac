package com.pap.rbac.operation.service.impl;

import com.pap.rbac.operation.mapper.OperationMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pap.rbac.operation.entity.Operation;
import com.pap.rbac.operation.service.IOperationService;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Transactional
@Service("operationService")
public class OperationServiceImpl implements IOperationService {

    @Resource(name = "operationMapper")
    private OperationMapper mapper;

    @Override
    public int deleteByPrimaryKey(String id) {
       return mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int selectCountByMap(Map<Object, Object> map) {
      return mapper.selectCountByMap(map);
    }

    @Override
    public List<Operation> selectListByMap(Map<Object, Object> map) {
        return mapper.selectListByMap(map);
    }

    @Override
    public int insert(Operation record) {
       return mapper.insert(record);
    }

    @Override
    public int insertSelective(Operation record) {
       return mapper.insertSelective(record);
    }

    @Override
    public Operation selectByPrimaryKey(String id) {
       return mapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(Operation record) {
       return mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Operation record) {
      return mapper.updateByPrimaryKey(record);
    }
}