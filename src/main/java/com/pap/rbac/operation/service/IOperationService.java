package com.pap.rbac.operation.service;

import com.pap.rbac.operation.entity.Operation;

import java.util.List;
import java.util.Map;

public interface IOperationService {

    int deleteByPrimaryKey(String id);

    int selectCountByMap(Map<Object, Object> map);

    List<Operation> selectListByMap(Map<Object, Object> map);

    int insert(Operation record);

    int insertSelective(Operation record);

    Operation selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(Operation record);

    int updateByPrimaryKey(Operation record);
}
