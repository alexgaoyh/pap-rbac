package com.pap.rbac.operation.entity;

import com.pap.base.entity.PapBaseEntity;
import com.pap.base.mybatis.plugin.annotation.*;
import java.io.Serializable;

@MyBatisTableAnnotation(name = "t_rbac_operation", namespace = "com.pap.rbac.operation.mapper.OperationMapper", remarks = " 修改点 ", aliasName = "t_rbac_operation t_rbac_operation" )
public class Operation extends PapBaseEntity implements Serializable {
    /**
     *  系统操作编号,所属表字段为t_rbac_operation.OPERATION_ID
     */
    @MyBatisColumnAnnotation(name = "OPERATION_ID", value = "t_rbac_operation_OPERATION_ID", chineseNote = "系统操作编号", tableAlias = "t_rbac_operation")
    @MyApiModelPropertyAnnotation(value = "系统操作编号")
    private String operationId;

    /**
     *  系统操作编码,所属表字段为t_rbac_operation.OPERATION_CODE
     */
    @MyBatisColumnAnnotation(name = "OPERATION_CODE", value = "t_rbac_operation_OPERATION_CODE", chineseNote = "系统操作编码", tableAlias = "t_rbac_operation")
    @MyApiModelPropertyAnnotation(value = "系统操作编码")
    private String operationCode;

    /**
     *  系统操作名称,所属表字段为t_rbac_operation.OPERATION_NAME
     */
    @MyBatisColumnAnnotation(name = "OPERATION_NAME", value = "t_rbac_operation_OPERATION_NAME", chineseNote = "系统操作名称", tableAlias = "t_rbac_operation")
    @MyApiModelPropertyAnnotation(value = "系统操作名称")
    private String operationName;

    /**
     *  操作链接,所属表字段为t_rbac_operation.OPERATION_URI
     */
    @MyBatisColumnAnnotation(name = "OPERATION_URI", value = "t_rbac_operation_OPERATION_URI", chineseNote = "操作链接", tableAlias = "t_rbac_operation")
    @MyApiModelPropertyAnnotation(value = "操作链接")
    private String operationUri;

    /**
     *  HTTP请求协议(POST/PUT/DELETE),所属表字段为t_rbac_operation.HTTP_METHOD
     */
    @MyBatisColumnAnnotation(name = "HTTP_METHOD", value = "t_rbac_operation_HTTP_METHOD", chineseNote = "HTTP请求协议(POST/PUT/DELETE)", tableAlias = "t_rbac_operation")
    @MyApiModelPropertyAnnotation(value = "HTTP请求协议(POST/PUT/DELETE)")
    private String httpMethod;

    @Override
    public String getDynamicTableName() {
        return "t_rbac_operation";
    }

    private static final long serialVersionUID = 1L;

    public String getOperationId() {
        return operationId;
    }

    public void setOperationId(String operationId) {
        this.operationId = operationId;
    }

    public String getOperationCode() {
        return operationCode;
    }

    public void setOperationCode(String operationCode) {
        this.operationCode = operationCode;
    }

    public String getOperationName() {
        return operationName;
    }

    public void setOperationName(String operationName) {
        this.operationName = operationName;
    }

    public String getOperationUri() {
        return operationUri;
    }

    public void setOperationUri(String operationUri) {
        this.operationUri = operationUri;
    }

    public String getHttpMethod() {
        return httpMethod;
    }

    public void setHttpMethod(String httpMethod) {
        this.httpMethod = httpMethod;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", operationId=").append(operationId);
        sb.append(", operationCode=").append(operationCode);
        sb.append(", operationName=").append(operationName);
        sb.append(", operationUri=").append(operationUri);
        sb.append(", httpMethod=").append(httpMethod);
        sb.append("]");
        return sb.toString();
    }
}