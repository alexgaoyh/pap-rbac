package com.pap.rbac.operation.mapper;

import com.pap.base.mapper.PapBaseMapper;
import com.pap.rbac.operation.entity.Operation;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;;

@Mapper
public interface OperationMapper extends PapBaseMapper<Operation> {
    int deleteByPrimaryKey(String operationId);

    int selectCountByMap(Map<Object, Object> map);

    List<Operation> selectListByMap(Map<Object, Object> map);

    Operation selectByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int deleteByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int insert(Operation record);

    int insertSelective(Operation record);

    Operation selectByPrimaryKey(String operationId);

    int updateByPrimaryKeySelective(Operation record);

    int updateByPrimaryKey(Operation record);
}