package com.pap.rbac.organizationuserrel.mapper;

import com.pap.base.mapper.PapBaseMapper;
import com.pap.rbac.organizationuserrel.entity.OrganizationUserRel;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;;

@Mapper
public interface OrganizationUserRelMapper extends PapBaseMapper<OrganizationUserRel> {
    int deleteByPrimaryKey(String organizationUserRelId);

    int selectCountByMap(Map<Object, Object> map);

    List<OrganizationUserRel> selectListByMap(Map<Object, Object> map);

    OrganizationUserRel selectByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int deleteByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int insert(OrganizationUserRel record);

    int insertSelective(OrganizationUserRel record);

    OrganizationUserRel selectByPrimaryKey(String organizationUserRelId);

    int updateByPrimaryKeySelective(OrganizationUserRel record);

    int updateByPrimaryKey(OrganizationUserRel record);

    // alexgaoyh

    int deleteByOrganizationId(@Param("organizationId") String organizationId);

}