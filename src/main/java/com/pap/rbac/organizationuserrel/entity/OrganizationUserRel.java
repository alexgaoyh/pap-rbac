package com.pap.rbac.organizationuserrel.entity;

import com.pap.base.entity.PapBaseEntity;
import com.pap.base.mybatis.plugin.annotation.*;
import java.io.Serializable;

@MyBatisTableAnnotation(name = "t_rbac_organization_user_rel", namespace = "com.pap.rbac.organizationuserrel.mapper.OrganizationUserRelMapper", remarks = " 修改点 ", aliasName = "t_rbac_organization_user_rel t_rbac_organization_user_rel" )
public class OrganizationUserRel extends PapBaseEntity implements Serializable {
    /**
     *  编号,所属表字段为t_rbac_organization_user_rel.ORGANIZATION_USER_REL_ID
     */
    @MyBatisColumnAnnotation(name = "ORGANIZATION_USER_REL_ID", value = "t_rbac_organization_user_rel_ORGANIZATION_USER_REL_ID", chineseNote = "编号", tableAlias = "t_rbac_organization_user_rel")
    @MyApiModelPropertyAnnotation(value = "编号")
    private String organizationUserRelId;

    /**
     *  所属组织机构编号,所属表字段为t_rbac_organization_user_rel.ORGANIZATION_ID
     */
    @MyBatisColumnAnnotation(name = "ORGANIZATION_ID", value = "t_rbac_organization_user_rel_ORGANIZATION_ID", chineseNote = "所属组织机构编号", tableAlias = "t_rbac_organization_user_rel")
    @MyApiModelPropertyAnnotation(value = "所属组织机构编号")
    private String organizationId;

    /**
     *  所属用户编号,所属表字段为t_rbac_organization_user_rel.USER_ID
     */
    @MyBatisColumnAnnotation(name = "USER_ID", value = "t_rbac_organization_user_rel_USER_ID", chineseNote = "所属用户编号", tableAlias = "t_rbac_organization_user_rel")
    @MyApiModelPropertyAnnotation(value = "所属用户编号")
    private String userId;

    @Override
    public String getDynamicTableName() {
        return "t_rbac_organization_user_rel";
    }

    private static final long serialVersionUID = 1L;

    public String getOrganizationUserRelId() {
        return organizationUserRelId;
    }

    public void setOrganizationUserRelId(String organizationUserRelId) {
        this.organizationUserRelId = organizationUserRelId;
    }

    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", organizationUserRelId=").append(organizationUserRelId);
        sb.append(", organizationId=").append(organizationId);
        sb.append(", userId=").append(userId);
        sb.append("]");
        return sb.toString();
    }
}