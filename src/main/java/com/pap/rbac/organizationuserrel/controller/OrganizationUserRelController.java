package com.pap.rbac.organizationuserrel.controller;

import com.pap.base.util.bean.BeanCopyUtilss;
import com.pap.base.util.string.StringUtilss;
import com.pap.beans.idworker.IdWorker;
import com.pap.obj.vo.logineduser.LoginedUser;
import com.pap.obj.vo.logineduser.LoginedUserVO;
import com.pap.obj.vo.page.Page;
import com.pap.obj.vo.response.ResponseVO;
import com.pap.rbac.dto.OrganizationUserRelDTO;
import com.pap.rbac.dto.OrganizationUserRelDetailsDTO;
import com.pap.rbac.dto.UserDTO;
import com.pap.rbac.organizationuserrel.entity.OrganizationUserRel;
import com.pap.rbac.organizationuserrel.service.IOrganizationUserRelService;
import com.pap.rbac.user.service.IUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import java.util.*;


@RestController
@RequestMapping("/organizationuserrel")
@Api(value = "组织机构下用户关联关系管理", tags = "组织机构下用户关联关系管理", description="组织机构下用户关联关系管理")
public class OrganizationUserRelController {

	private static Logger logger  =  LoggerFactory.getLogger(OrganizationUserRelController.class);
	
	@Resource(name = "organizationUserRelService")
	private IOrganizationUserRelService organizationUserRelService;

	@Resource(name = "userService")
	private IUserService userService;

	@Resource(name = "idWorker")
	private IdWorker idWorker;


	@ApiOperation("保存组织机构与用户管理关系")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "organizationUserRelDetailsDTO", value = "", required = true, dataType = "OrganizationUserRelDetailsDTO", paramType="body")
	})
	@PostMapping(value = "/saverels")
	public ResponseVO<String> saveRels(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
									   @RequestHeader(value = "papVersion", defaultValue = "-1") String papVersion,
									   @RequestBody OrganizationUserRelDetailsDTO organizationUserRelDetailsDTO,
									   @LoginedUser @ApiIgnore LoginedUserVO loginedUserVO) throws Exception {

		String clientLicenseId = loginedUserVO != null ? loginedUserVO.getClientLicenseId() : "";
		organizationUserRelService.updateOrganizationUserRel(loginedUserVO, organizationUserRelDetailsDTO.getOrganizationId(), organizationUserRelDetailsDTO.getDetails());
		return ResponseVO.successdata("操作成功");
	}

	@ApiOperation("根据组织机构编号查询用户信息")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "organizationId", value = "组织机构编号", required = true, dataType = "String", paramType="query")
	})
	@GetMapping(value = "/selectuserIdsbyorganizationId")
	public ResponseVO<OrganizationUserRelDTO> selectUserIdsByOrganizationId(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
																			  @RequestHeader(value = "papVersion", defaultValue = "-1") String papVersion,
																			  @RequestParam String organizationId) throws Exception {
		List<UserDTO> userDTOList = new ArrayList<UserDTO>();

		List<OrganizationUserRelDTO> organizationUserRelDTOList = organizationUserRelService.selectUserIdsByOrganizationId(organizationId);
		if(organizationUserRelDTOList != null && organizationUserRelDTOList.size() > 0) {
			for (OrganizationUserRelDTO organizationUserRelDTO : organizationUserRelDTOList) {
				UserDTO userDTOTemp = userService.selectDTOByPrimaryKey(organizationUserRelDTO.getUserId());
				userDTOList.add(userDTOTemp);
			}
		}

		return ResponseVO.successdatas(userDTOList, null);
	}

	@ApiOperation("根据组织机构编号集合(逗号分隔)查询用户信息")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "organizationId", value = "组织机构编号", required = true, dataType = "String", paramType="query")
	})
	@GetMapping(value = "/selectuserIdsbyorganizationIds")
	public ResponseVO<String> selectUserIdsByOrganizationIds(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
																			@RequestHeader(value = "papVersion", defaultValue = "-1") String papVersion,
																			@RequestParam String organizationIds) throws Exception {
		Set<String> userIdSet = new HashSet<String>();
		if (StringUtilss.isNotEmpty(organizationIds)) {
			String[] idArray = organizationIds.split(",");
			for (int idIdx = 0; idIdx < idArray.length; idIdx++) {
				List<OrganizationUserRelDTO> organizationUserRelDTOList = organizationUserRelService.selectUserIdsByOrganizationId(idArray[idIdx]);
				if(organizationUserRelDTOList != null && organizationUserRelDTOList.size() > 0) {
					for (OrganizationUserRelDTO organizationUserRelDTO : organizationUserRelDTOList) {
                        userIdSet.add(organizationUserRelDTO.getUserId());
					}
				}
			}
		}

        List<String> userIdList = new ArrayList<>(userIdSet);
		return ResponseVO.successdatas(userIdList, null);
	}

	@ApiOperation("创建")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "organizationUserRelDTO", value = "", required = true, dataType = "OrganizationUserRelDTO", paramType="body")
	})
	@PostMapping(value = "")
	@Deprecated
	public ResponseVO<OrganizationUserRelDTO> create(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													 @RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													 @RequestBody OrganizationUserRelDTO organizationUserRelDTO,
													 @LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){
		String idStr = idWorker.nextIdStr();
		// TODO 设置主键
		organizationUserRelDTO.setOrganizationUserRelId(idStr);

		OrganizationUserRel databaseObj = new OrganizationUserRel();
		BeanCopyUtilss.myCopyProperties(organizationUserRelDTO, databaseObj, loginedUserVO);

		int i = organizationUserRelService.insertSelective(databaseObj);

		return ResponseVO.successdata(organizationUserRelDTO);
	}

	@ApiOperation("更新")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "organizationUserRelDTO", value = "", required = true, dataType = "OrganizationUserRelDTO", paramType="body")
	})
	@PutMapping(value = "")
	@Deprecated
	public ResponseVO<OrganizationUserRelDTO> update(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@RequestBody OrganizationUserRelDTO organizationUserRelDTO,
													@LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){

		OrganizationUserRel databaseObj = new OrganizationUserRel();
		BeanCopyUtilss.myCopyProperties(organizationUserRelDTO, databaseObj, loginedUserVO, false);
		int i = organizationUserRelService.updateByPrimaryKeySelective(databaseObj);

		// TODO 主键部分
		databaseObj = organizationUserRelService.selectByPrimaryKey(databaseObj.getOrganizationUserRelId());

		return ResponseVO.successdata(databaseObj);
	}


	@ApiOperation("删除")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "organizationUserRelDTO", value = "", required = true, dataType = "OrganizationUserRelDTO", paramType="body")
	})
	@DeleteMapping(value = "")
	@Deprecated
	public ResponseVO<String> delete(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@RequestBody OrganizationUserRelDTO organizationUserRelDTO,
													@LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){

		OrganizationUserRel databaseObj = new OrganizationUserRel();
		BeanCopyUtilss.myCopyProperties(organizationUserRelDTO, databaseObj);

		// TODO 主键部分
		int i = organizationUserRelService.deleteByPrimaryKey(databaseObj.getOrganizationUserRelId());

		return ResponseVO.successdata("操作成功");
	}

	@ApiOperation("详情")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "id", value = "编号", required = true, dataType = "String", paramType="path")
	})
	@GetMapping(value = "/{id}")
	@Deprecated
	public ResponseVO<OrganizationUserRelDTO> selectOne(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@PathVariable("id") String id){

		OrganizationUserRel databaseObj = organizationUserRelService.selectByPrimaryKey(id);

		return ResponseVO.successdata(databaseObj);
	}

	@ApiOperation("查询")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "organizationUserRelDTO", value = "应用查询参数", required = false, dataType = "OrganizationUserRelDTO", paramType="body"),
		@ApiImplicitParam(name = "pageNo", value = "页码", required = true, dataType = "int", paramType="query"),
		@ApiImplicitParam(name = "pageSize", value = "每页记录数", required = true, dataType = "int", paramType="query"),
		@ApiImplicitParam(name = "sortBy", value = "按照哪个属性排序", required = false, dataType = "String", paramType="query"),
		@ApiImplicitParam(name = "order", value = "排序顺序", required = false, dataType = "String", paramType="query")
	})
	@PostMapping(value = "/query")
	@Deprecated
	public ResponseVO<OrganizationUserRelDTO>query(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
												@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
												@RequestBody OrganizationUserRelDTO organizationUserRelDTO,
												@RequestParam Integer pageNo, @RequestParam Integer pageSize,
												@RequestParam(defaultValue = "create_time") String sortBy, @RequestParam(defaultValue = "desc") String order,
												@LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){
		Map<Object, Object> map = BeanCopyUtilss.beanToMap(organizationUserRelDTO, loginedUserVO);
		if(pageSize != 0) {
			Page newPage = new Page(pageNo + "", pageSize + "");
			map.put("page", newPage);
		}
		List<OrganizationUserRel> list = organizationUserRelService.selectListByMap(map);

		//
		List<OrganizationUserRelDTO> dtoList = toDTO(list);

		if(pageSize != 0) {
			Page newPage = new Page(pageNo + "", pageSize + "");
			int countNum = organizationUserRelService.selectCountByMap(map);
			newPage.setCount(countNum);
			return ResponseVO.successdatas(dtoList, newPage);
		} else {
			return ResponseVO.successdatas(dtoList, null);
		}

	}

	private List<OrganizationUserRelDTO> toDTO(List<OrganizationUserRel> databaseList) {
		List<OrganizationUserRelDTO> returnList = new ArrayList<OrganizationUserRelDTO>();
		if(databaseList != null) {
			for(OrganizationUserRel dbEntity : databaseList) {
				OrganizationUserRelDTO dtoTemp = new OrganizationUserRelDTO();
				BeanCopyUtilss.myCopyProperties(dbEntity, dtoTemp);
				returnList.add(dtoTemp);
			}
		}
		return returnList;
	}

	private List<OrganizationUserRel> toDB(List<OrganizationUserRelDTO> dtoList) {
		List<OrganizationUserRel> returnList = new ArrayList<OrganizationUserRel>();
		if(dtoList != null) {
			for(OrganizationUserRelDTO dtoEntity : dtoList) {
				OrganizationUserRel dbTemp = new OrganizationUserRel();
				BeanCopyUtilss.myCopyProperties(dtoEntity, dbTemp);
				returnList.add(dbTemp);
			}
		}
		return returnList;
	}
}
