package com.pap.rbac.organizationuserrel.service;

import com.pap.obj.vo.logineduser.LoginedUserVO;
import com.pap.rbac.dto.OrganizationUserRelDTO;
import com.pap.rbac.organizationuserrel.entity.OrganizationUserRel;

import java.util.List;
import java.util.Map;

public interface IOrganizationUserRelService {

    int deleteByPrimaryKey(String id);

    int selectCountByMap(Map<Object, Object> map);

    List<OrganizationUserRel> selectListByMap(Map<Object, Object> map);

    int insert(OrganizationUserRel record);

    int insertSelective(OrganizationUserRel record);

    OrganizationUserRel selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(OrganizationUserRel record);

    int updateByPrimaryKey(OrganizationUserRel record);

    // alexgaoyh added

    /**
     * 根据组织机构编号,更新最新的用户集合
     * @param loginedUserVO
     * @param organizationId
     * @param userIdsList
     */
    void updateOrganizationUserRel(LoginedUserVO loginedUserVO, String organizationId, List<String> userIdsList);

    /**
     * 根据组织机构 organizationId ,将包含的用户集合进行返回
     * @param organizationId
     * @return
     */
    List<OrganizationUserRelDTO> selectUserIdsByOrganizationId(String organizationId);

}
