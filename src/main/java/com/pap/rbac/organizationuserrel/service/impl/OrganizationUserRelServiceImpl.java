package com.pap.rbac.organizationuserrel.service.impl;

import com.pap.base.util.bean.BeanCopyUtilss;
import com.pap.base.util.date.DateUtils;
import com.pap.obj.vo.logineduser.LoginedUserVO;
import com.pap.rbac.dto.OrganizationUserRelDTO;
import com.pap.rbac.organizationuserrel.mapper.OrganizationUserRelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pap.rbac.organizationuserrel.entity.OrganizationUserRel;
import com.pap.rbac.organizationuserrel.service.IOrganizationUserRelService;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Transactional
@Service("organizationUserRelService")
public class OrganizationUserRelServiceImpl implements IOrganizationUserRelService {

    @Resource(name = "organizationUserRelMapper")
    private OrganizationUserRelMapper mapper;

    @Override
    public int deleteByPrimaryKey(String id) {
       return mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int selectCountByMap(Map<Object, Object> map) {
      return mapper.selectCountByMap(map);
    }

    @Override
    public List<OrganizationUserRel> selectListByMap(Map<Object, Object> map) {
        return mapper.selectListByMap(map);
    }

    @Override
    public int insert(OrganizationUserRel record) {
       return mapper.insert(record);
    }

    @Override
    public int insertSelective(OrganizationUserRel record) {
       return mapper.insertSelective(record);
    }

    @Override
    public OrganizationUserRel selectByPrimaryKey(String id) {
       return mapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(OrganizationUserRel record) {
       return mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(OrganizationUserRel record) {
      return mapper.updateByPrimaryKey(record);
    }

    @Override
    public void updateOrganizationUserRel(LoginedUserVO loginedUserVO, String organizationId, List<String> userIdsList) {
        if(userIdsList != null) {
            // 删除已被删除的明细数据: dto中无明细数据，数据库中有明细数据，删除
            Map<Object, Object> operSelectOrganizationMap = new HashMap<Object, Object>();
            operSelectOrganizationMap.put("organizationId", organizationId);
            List<OrganizationUserRel> dbRelList = mapper.selectListByMap(operSelectOrganizationMap);
            if(dbRelList != null && dbRelList.size() > 0) {
                for(OrganizationUserRel dbRel : dbRelList) {
                    boolean existBool = false;
                    for(String userIdTemp: userIdsList) {
                        if(dbRel.getUserId().equals(userIdTemp)) {
                            existBool = true;
                        }
                    }
                    // existBool = true 说明  数据未被删除
                    if(existBool == false) {
                        mapper.deleteByPrimaryKey(dbRel.getOrganizationUserRelId());
                    }

                }
            }

            for (String userIdTemp : userIdsList) {
                Map<Object, Object> operSelectResourceOperationMap = new HashMap<Object, Object>();
                operSelectResourceOperationMap.put("organizationId", organizationId);
                operSelectResourceOperationMap.put("userId", userIdTemp);
                operSelectResourceOperationMap.put("clientLicenseId", loginedUserVO != null ? loginedUserVO.getClientLicenseId() : "");
                List<OrganizationUserRel> organizationUserList = mapper.selectListByMap(operSelectResourceOperationMap);
                if(organizationUserList == null || organizationUserList.size() == 0) {

                    OrganizationUserRel organizationUserRel = new OrganizationUserRel();
                    organizationUserRel.setOrganizationUserRelId(new StringBuffer(organizationId).append(":")
                            .append(userIdTemp).toString());
                    organizationUserRel.setOrganizationId(organizationId);
                    organizationUserRel.setUserId(userIdTemp);
                    organizationUserRel.setCreateIp("0.0.0.0");
                    organizationUserRel.setCreateTime(DateUtils.getCurrDateTimeStr());
                    organizationUserRel.setCreateUser(loginedUserVO != null ? loginedUserVO.getId() : "");
                    organizationUserRel.setClientLicenseId(loginedUserVO != null ? loginedUserVO.getClientLicenseId() : "");
                    mapper.insertSelective(organizationUserRel);
                }
            }
        }
    }

    @Override
    public List<OrganizationUserRelDTO> selectUserIdsByOrganizationId(String organizationId) {

        Map<Object, Object> operSelectResourceMap = new HashMap<Object, Object>();
        operSelectResourceMap.put("organizationId", organizationId);
        List<OrganizationUserRel> dbRelList = mapper.selectListByMap(operSelectResourceMap);

        List<OrganizationUserRelDTO> dtoRelList = toDTO(dbRelList);
        return dtoRelList;
    }

    private List<OrganizationUserRelDTO> toDTO(List<OrganizationUserRel> databaseList) {
        List<OrganizationUserRelDTO> returnList = new ArrayList<OrganizationUserRelDTO>();
        if(databaseList != null) {
            for(OrganizationUserRel dbEntity : databaseList) {
                OrganizationUserRelDTO dtoTemp = new OrganizationUserRelDTO();
                BeanCopyUtilss.myCopyProperties(dbEntity, dtoTemp);
                returnList.add(dtoTemp);
            }
        }
        return returnList;
    }
}