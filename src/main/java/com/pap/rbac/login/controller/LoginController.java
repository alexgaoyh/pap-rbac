package com.pap.rbac.login.controller;

import com.pap.base.constant.JWTConstants;
import com.pap.base.dto.jwt.JWTTokenDTO;
import com.pap.base.dto.jwt.JWTUserDTO;
import com.pap.base.util.bean.BeanCopyUtilss;
import com.pap.base.util.jwt.JWTTokenUtilss;
import com.pap.base.util.string.StringUtilss;
import com.pap.obj.vo.response.ResponseVO;
import com.pap.rbac.dto.UserDTO;
import com.pap.rbac.dto.login.UserInfoDTO;
import com.pap.rbac.user.entity.User;
import com.pap.rbac.user.service.IUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping("")
@Api(value = "登录管理", tags = "登录管理", description="登录管理")
public class LoginController {

	private static Logger logger  =  LoggerFactory.getLogger(LoginController.class);
	
	@Autowired
	@Qualifier(value = "userService")
	private IUserService userAgent;

	@ApiOperation("用户名密码登录")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "userDTO", value = "", required = true, dataType = "UserDTO", paramType="body")
	})
	@RequestMapping(value = "/loginbyloginname", method = {RequestMethod.POST} )
	public ResponseVO<UserDTO> loginByLoginName(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
											 @RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
											 @RequestBody UserDTO userDTO,
											 HttpServletResponse response) throws Exception {
		
		if(userDTO != null && StringUtilss.isNotEmpty(userDTO.getUserLoginName())
				&& StringUtilss.isNotEmpty(userDTO.getUserPasswords())) {
			Map<Object, Object> loginMap = new HashMap<Object, Object>();
			loginMap.put("userLoginName", userDTO.getUserLoginName());
			loginMap.put("userPasswords", userDTO.getUserPasswords());
			List<User> loginList = userAgent.selectListByMap(loginMap);
			if(loginList != null && loginList.size() > 0) {
				User user = loginList.get(0);
				user.setUserPasswords(null);

				UserDTO tempDTO = new UserDTO();
				BeanCopyUtilss.myCopyProperties(user, tempDTO);
				// 维护 uuid token
				// uuid 是用户身份唯一标识 用户注册的时候确定 并且不可改变 不可重复
				// token 代表用户当前登录状态 建议在网络请求中携带 token
				tempDTO.setUuid(tempDTO.getUserId());
				// TODO 后期这里的数据改为新的数据结构(含用户信息、权限信息等)
				tempDTO.setPapToken(UUID.randomUUID().toString());
				return ResponseVO.successdata(tempDTO);
			} else {
				return ResponseVO.validfail("用户名密码错误，请检验数据!");
			}
		} else {
			return ResponseVO.validfail("用户名密码为空，请检查数据!");
		}
	}

	/**
	 * 登录获取 JWT
	 * @param papVersion
	 * @param userDTO
	 * @return
	 */
	@RequestMapping(value = "/token", method = {RequestMethod.POST} )
	public ResponseVO<JWTTokenDTO> token(@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
										 @RequestBody UserDTO userDTO) {
		if(userDTO != null && StringUtilss.isNotEmpty(userDTO.getUserLoginName())
				&& StringUtilss.isNotEmpty(userDTO.getUserPasswords())) {
			Map<Object, Object> loginMap = new HashMap<Object, Object>();
			loginMap.put("userLoginName", userDTO.getUserLoginName());
			loginMap.put("userPasswords", userDTO.getUserPasswords());
			List<User> loginList = userAgent.selectListByMap(loginMap);
			if(loginList != null && loginList.size() > 0) {
				User user = loginList.get(0);

				UserInfoDTO userInfoDTO = formatLoginUserInfoByUser(user);


				// token 生成
				JWTUserDTO jwtUserDTO = new JWTUserDTO();
				BeanCopyUtilss.myCopyProperties(userInfoDTO, jwtUserDTO);

				String token = JWTTokenUtilss.create(jwtUserDTO, JWTConstants.SECRET, JWTConstants.VALIDITY_TIME_MS);

				JWTTokenDTO jwtTokenDTO = new JWTTokenDTO();
				jwtTokenDTO.setToken(token);
				jwtTokenDTO.setUserId(userInfoDTO.getUserId());
				jwtTokenDTO.setUserName(userInfoDTO.getUserName());
				jwtTokenDTO.setPermissionCodeList(userInfoDTO.getPermissionCodeList());
				jwtTokenDTO.setValidityTimeMs(JWTConstants.VALIDITY_TIME_MS);

				return ResponseVO.successdata(jwtTokenDTO);
			} else {
				return ResponseVO.validfail("用户名密码错误，请检验数据!");
			}
		} else {
			return ResponseVO.validfail("用户名密码为空，请检查数据!");
		}
	}

	/**
	 * 多版本请求处理，在 papVersion 中判断当前请求的版本，进入到不同的方法中。
	 * 注意 请求 URL 相同，但是请求头中的 papVersion 的参数不同，这样就可以进行版本请求的区分。
	 * @param papToken
	 * @param papVersion
	 * @return
	 */
	@Value("${spring.application.name}")
	private String springApplicationName;

	/**
	 * headers 数组参数的使用，存在如下的使用规则
	 * 			1、参数名(!)=参数值		存在某个参数且值等于或不等于指定值时匹配请求.
	 * 			2、(!)参数名 				存在或不存在某个参数时匹配.
	 * @param papToken
	 * @param papVersion
	 * @return
	 */
	@GetMapping(value = "/test", headers = {"papVersion"})
	public ResponseVO<String> testContainsHeader(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
									  @RequestHeader(value = "papVersion", defaultValue = "1.0.1") String papVersion) {
		return ResponseVO.successdata(springApplicationName + ":testContainsHeader:" + papVersion);
	}
	@GetMapping(value = "/test", headers = {"!papVersion"})
	public ResponseVO<String> testNoExistHeader(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
								   @RequestHeader(value = "papVersion", defaultValue = "1.0.2") String papVersion) {
		return ResponseVO.successdata(springApplicationName + ":testNoExistHeader:" + papVersion);
	}
	@GetMapping(value = "/test", headers = {"papVersion=1.0.3"})
	public ResponseVO<String> testV103(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													  @RequestHeader(value = "papVersion", defaultValue = "1.0.3") String papVersion) {
		return ResponseVO.successdata(springApplicationName + ":testV103:" + papVersion);
	}
	@GetMapping(value = "/test", headers = {"papVersion=1.1.0"})
	public ResponseVO<String> testV110(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
									 @RequestHeader(value = "papVersion", defaultValue = "1.1.0") String papVersion) {
		return ResponseVO.successdata(springApplicationName + ":testV110:" + papVersion);
	}

	/**
	 * 根据数据库user对象，重新封装含权限信息的用户对象
	 * @param user
	 * @return
	 */
	private UserInfoDTO formatLoginUserInfoByUser(User user) {
		UserInfoDTO userInfoDTO = new UserInfoDTO();
		userInfoDTO.setUserId(user.getUserId());
		userInfoDTO.setUserName(user.getUserName());
		userInfoDTO.setUserMobile(user.getMobile());
		userInfoDTO.setTenantId(user.getClientLicenseId());
		userInfoDTO.setPermissionCodeList(userAgent.selectPermissionCodesByUserId(user.getUserId()));
		return userInfoDTO;
	}

}
