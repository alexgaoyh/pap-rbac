package com.pap.rbac.dto;

import com.pap.base.entity.PapBaseEntity;
import com.pap.base.mybatis.plugin.annotation.MyBatisColumnAnnotation;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Map;

/**
 * @Auther: alexgaoyh
 * @Date: 2018/12/25 15:12
 * @Description:
 */
@ApiModel(value = "OrganizationDTO", description = "组织机构")
public class OrganizationDTO extends PapBaseEntity implements Serializable {
    /**
     * 编号，主键,所属表字段为t_rbac_organization.ORGANIZATION_ID
     */
    @MyBatisColumnAnnotation(name = "ORGANIZATION_ID", value = "t_rbac_organization_ORGANIZATION_ID", chineseNote = "编号，主键", tableAlias = "t_rbac_organization")
    @ApiModelProperty(value = "编号，主键")
    private String organizationId;

    /**
     * 业务编码,所属表字段为t_rbac_organization.ORGANIZATION_CODE
     */
    @MyBatisColumnAnnotation(name = "ORGANIZATION_CODE", value = "t_rbac_organization_ORGANIZATION_CODE", chineseNote = "业务编码", tableAlias = "t_rbac_organization")
    @ApiModelProperty(value = "业务编码")
    private String organizationCode;

    /**
     * 名称,所属表字段为t_rbac_organization.ORGANIZATION_NAME
     */
    @MyBatisColumnAnnotation(name = "ORGANIZATION_NAME", value = "t_rbac_organization_ORGANIZATION_NAME", chineseNote = "名称", tableAlias = "t_rbac_organization")
    @ApiModelProperty(value = "名称")
    private String organizationName;

    /**
     * 父级节点编号,所属表字段为t_rbac_organization.ORGANIZATION_PARENT_ID
     */
    @MyBatisColumnAnnotation(name = "ORGANIZATION_PARENT_ID", value = "t_rbac_organization_ORGANIZATION_PARENT_ID", chineseNote = "父级节点编号", tableAlias = "t_rbac_organization")
    @ApiModelProperty(value = "父级节点编号")
    private String organizationParentId;

    /**
     * 编号路径集合,所属表字段为t_rbac_organization.PATH_IDS
     */
    @MyBatisColumnAnnotation(name = "PATH_IDS", value = "t_rbac_organization_PATH_IDS", chineseNote = "编号路径集合", tableAlias = "t_rbac_organization")
    @ApiModelProperty(value = "编号路径集合")
    private String pathIds;

    /**
     * 业务编码路径集合,所属表字段为t_rbac_organization.PATH_CODES
     */
    @MyBatisColumnAnnotation(name = "PATH_CODES", value = "t_rbac_organization_PATH_CODES", chineseNote = "业务编码路径集合", tableAlias = "t_rbac_organization")
    @ApiModelProperty(value = "业务编码路径集合")
    private String pathCodes;

    /**
     * 名称路径集合,所属表字段为t_rbac_organization.PATH_NAMES
     */
    @MyBatisColumnAnnotation(name = "PATH_NAMES", value = "t_rbac_organization_PATH_NAMES", chineseNote = "名称路径集合", tableAlias = "t_rbac_organization")
    @ApiModelProperty(value = "名称路径集合")
    private String pathNames;

    /**
     * 省份编号,所属表字段为t_rbac_organization.PROVINCE_ID
     */
    @MyBatisColumnAnnotation(name = "PROVINCE_ID", value = "t_rbac_organization_PROVINCE_ID", chineseNote = "省份编号", tableAlias = "t_rbac_organization")
    @ApiModelProperty(value = "省份编号")
    private String provinceId;

    /**
     * 省份名称,所属表字段为t_rbac_organization.PROVINCE_NAME
     */
    @MyBatisColumnAnnotation(name = "PROVINCE_NAME", value = "t_rbac_organization_PROVINCE_NAME", chineseNote = "省份名称", tableAlias = "t_rbac_organization")
    @ApiModelProperty(value = "省份名称")
    private String provinceName;

    /**
     * 市级编号,所属表字段为t_rbac_organization.CITY_ID
     */
    @MyBatisColumnAnnotation(name = "CITY_ID", value = "t_rbac_organization_CITY_ID", chineseNote = "市级编号", tableAlias = "t_rbac_organization")
    @ApiModelProperty(value = "市级编号")
    private String cityId;

    /**
     * 市级名称,所属表字段为t_rbac_organization.CITY_NAME
     */
    @MyBatisColumnAnnotation(name = "CITY_NAME", value = "t_rbac_organization_CITY_NAME", chineseNote = "市级名称", tableAlias = "t_rbac_organization")
    @ApiModelProperty(value = "市级名称")
    private String cityName;

    /**
     * 区域编号,所属表字段为t_rbac_organization.AREA_ID
     */
    @MyBatisColumnAnnotation(name = "AREA_ID", value = "t_rbac_organization_AREA_ID", chineseNote = "区域编号", tableAlias = "t_rbac_organization")
    @ApiModelProperty(value = "区域编号")
    private String areaId;

    /**
     * 区域名称,所属表字段为t_rbac_organization.AREA_NAME
     */
    @MyBatisColumnAnnotation(name = "AREA_NAME", value = "t_rbac_organization_AREA_NAME", chineseNote = "区域名称", tableAlias = "t_rbac_organization")
    @ApiModelProperty(value = "区域名称")
    private String areaName;

    /**
     * 详细地址,所属表字段为t_rbac_organization.DETAIL_ADDRESS
     */
    @MyBatisColumnAnnotation(name = "DETAIL_ADDRESS", value = "t_rbac_organization_DETAIL_ADDRESS", chineseNote = "详细地址", tableAlias = "t_rbac_organization")
    @ApiModelProperty(value = "详细地址")
    private String detailAddress;

    /**
     *  分类:F法人公司;V虚拟公司,所属表字段为t_rbac_organization.ORGANIZATION_TYPE
     */
    @MyBatisColumnAnnotation(name = "ORGANIZATION_TYPE", value = "t_rbac_organization_ORGANIZATION_TYPE", chineseNote = "分类:F法人公司;V虚拟公司", tableAlias = "t_rbac_organization")
    @ApiModelProperty(value = "分类:F法人公司;V虚拟公司")
    private String organizationType;

    /**
     * 可用状态标示,所属表字段为t_rbac_organization.DISABLE_FLAG
     */
    @MyBatisColumnAnnotation(name = "DISABLE_FLAG", value = "t_rbac_organization_DISABLE_FLAG", chineseNote = "可用状态标示", tableAlias = "t_rbac_organization")
    @ApiModelProperty(value = "可用状态标示")
    private String disableFlag;

    /**
     * 备注字段,所属表字段为t_rbac_organization.REMARK
     */
    @MyBatisColumnAnnotation(name = "REMARK", value = "t_rbac_organization_REMARK", chineseNote = "备注字段", tableAlias = "t_rbac_organization")
    @ApiModelProperty(value = "备注字段")
    private String remark;

    /**
     * 额外参数动态添加
     */
    private Map<String, String> extraParams;

    public String getDynamicTableName() {
        return "t_rbac_organization";
    }

    private static final long serialVersionUID = 1L;

    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    public String getOrganizationCode() {
        return organizationCode;
    }

    public void setOrganizationCode(String organizationCode) {
        this.organizationCode = organizationCode;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public String getOrganizationParentId() {
        return organizationParentId;
    }

    public void setOrganizationParentId(String organizationParentId) {
        this.organizationParentId = organizationParentId;
    }

    public String getPathIds() {
        return pathIds;
    }

    public void setPathIds(String pathIds) {
        this.pathIds = pathIds;
    }

    public String getPathCodes() {
        return pathCodes;
    }

    public void setPathCodes(String pathCodes) {
        this.pathCodes = pathCodes;
    }

    public String getPathNames() {
        return pathNames;
    }

    public void setPathNames(String pathNames) {
        this.pathNames = pathNames;
    }

    public String getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(String provinceId) {
        this.provinceId = provinceId;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getDetailAddress() {
        return detailAddress;
    }

    public void setDetailAddress(String detailAddress) {
        this.detailAddress = detailAddress;
    }

    public String getOrganizationType() {
        return organizationType;
    }

    public void setOrganizationType(String organizationType) {
        this.organizationType = organizationType;
    }

    public String getDisableFlag() {
        return disableFlag;
    }

    public void setDisableFlag(String disableFlag) {
        this.disableFlag = disableFlag;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Map<String, String> getExtraParams() {
        return extraParams;
    }

    public void setExtraParams(Map<String, String> extraParams) {
        this.extraParams = extraParams;
    }

    @Override
    public String toString() {
        return "OrganizationDTO{" +
                "organizationId='" + organizationId + '\'' +
                ", organizationCode='" + organizationCode + '\'' +
                ", organizationName='" + organizationName + '\'' +
                ", organizationParentId='" + organizationParentId + '\'' +
                ", pathIds='" + pathIds + '\'' +
                ", pathCodes='" + pathCodes + '\'' +
                ", pathNames='" + pathNames + '\'' +
                ", provinceId='" + provinceId + '\'' +
                ", provinceName='" + provinceName + '\'' +
                ", cityId='" + cityId + '\'' +
                ", cityName='" + cityName + '\'' +
                ", areaId='" + areaId + '\'' +
                ", areaName='" + areaName + '\'' +
                ", detailAddress='" + detailAddress + '\'' +
                ", organizationType='" + organizationType + '\'' +
                ", disableFlag='" + disableFlag + '\'' +
                ", remark='" + remark + '\'' +
                ", extraParams=" + extraParams +
                '}';
    }
}