package com.pap.rbac.dto;

import io.swagger.annotations.ApiModel;

import java.io.Serializable;
import java.util.List;

@ApiModel(value = "OrganizationUserRelDetailsDTO", description = "组织机构与用户关联关系")
public class OrganizationUserRelDetailsDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 组织机构编号
	 */
	private String organizationId;

	/**
	 * 用户编号集合
	 */
	private List<String> details;

	public String getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(String organizationId) {
		this.organizationId = organizationId;
	}

	public List<String> getDetails() {
		return details;
	}

	public void setDetails(List<String> details) {
		this.details = details;
	}

	@Override
	public String toString() {
		return "OrganizationUserRelDetailsDTO{" +
				"organizationId='" + organizationId + '\'' +
				", details=" + details +
				'}';
	}
}
