package com.pap.rbac.dto;

import com.pap.base.entity.PapBaseEntity;
import com.pap.base.mybatis.plugin.annotation.MyBatisColumnAnnotation;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(value = "PermissionGroupPermissionRelDTO", description = "权限组权限关联关系")
public class PermissionGroupPermissionRelDTO extends PapBaseEntity implements Serializable {
    /**
     *  编号，主键,所属表字段为t_rbac_permission_group_permission_rel.PERMISSION_GROUP_PERMISSION_REL_ID
     */
    @MyBatisColumnAnnotation(name = "PERMISSION_GROUP_PERMISSION_REL_ID", value = "t_rbac_permission_group_permission_rel_PERMISSION_GROUP_PERMISSION_REL_ID", chineseNote = "编号，主键", tableAlias = "t_rbac_permission_group_permission_rel")
    @ApiModelProperty(value = "编号，主键")
    private String permissionGroupPermissionRelId;

    /**
     *  所属权限组编号,所属表字段为t_rbac_permission_group_permission_rel.PERMISSION_GROUP_ID
     */
    @MyBatisColumnAnnotation(name = "PERMISSION_GROUP_ID", value = "t_rbac_permission_group_permission_rel_PERMISSION_GROUP_ID", chineseNote = "所属权限组编号", tableAlias = "t_rbac_permission_group_permission_rel")
    @ApiModelProperty(value = "所属权限组编号")
    private String permissionGroupId;

    /**
     *  所属权限编号,所属表字段为t_rbac_permission_group_permission_rel.PERMISSION_ID
     */
    @MyBatisColumnAnnotation(name = "PERMISSION_ID", value = "t_rbac_permission_group_permission_rel_PERMISSION_ID", chineseNote = "所属权限编号", tableAlias = "t_rbac_permission_group_permission_rel")
    @ApiModelProperty(value = "所属权限编号")
    private String permissionId;

    @Override
    public String getDynamicTableName() {
        return "t_rbac_permission_group_permission_rel";
    }

    private static final long serialVersionUID = 1L;

    public String getPermissionGroupPermissionRelId() {
        return permissionGroupPermissionRelId;
    }

    public void setPermissionGroupPermissionRelId(String permissionGroupPermissionRelId) {
        this.permissionGroupPermissionRelId = permissionGroupPermissionRelId;
    }

    public String getPermissionGroupId() {
        return permissionGroupId;
    }

    public void setPermissionGroupId(String permissionGroupId) {
        this.permissionGroupId = permissionGroupId;
    }

    public String getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(String permissionId) {
        this.permissionId = permissionId;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", permissionGroupPermissionRelId=").append(permissionGroupPermissionRelId);
        sb.append(", permissionGroupId=").append(permissionGroupId);
        sb.append(", permissionId=").append(permissionId);
        sb.append("]");
        return sb.toString();
    }
}