package com.pap.rbac.dto;

import com.pap.base.entity.PapBaseEntity;
import com.pap.base.mybatis.plugin.annotation.MyBatisColumnAnnotation;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(value = "RolePermissionRelDTO", description = "角色权限关联对象")
public class RolePermissionRelDTO extends PapBaseEntity implements Serializable {
    /**
     *  角色权限编号,所属表字段为t_rbac_role_permission_rel.ROLE_PERMISSION_REL_ID
     */
    @MyBatisColumnAnnotation(name = "ROLE_PERMISSION_REL_ID", value = "t_rbac_role_permission_rel_ROLE_PERMISSION_REL_ID", chineseNote = "角色权限编号", tableAlias = "t_rbac_role_permission_rel")
    @ApiModelProperty(value = "角色权限编号")
    private String rolePermissionRelId;

    /**
     *  所属角色编号,所属表字段为t_rbac_role_permission_rel.ROLE_ID
     */
    @MyBatisColumnAnnotation(name = "ROLE_ID", value = "t_rbac_role_permission_rel_ROLE_ID", chineseNote = "所属角色编号", tableAlias = "t_rbac_role_permission_rel")
    @ApiModelProperty(value = "所属角色编号")
    private String roleId;

    /**
     *  所属权限组编号,所属表字段为t_rbac_role_permission_rel.PERMISSION_GROUP_ID
     */
    @MyBatisColumnAnnotation(name = "PERMISSION_GROUP_ID", value = "t_rbac_role_permission_rel_PERMISSION_GROUP_ID", chineseNote = "所属权限组编号", tableAlias = "t_rbac_role_permission_rel")
    @ApiModelProperty(value = "所属权限组编号")
    private String permissionGroupId;

    /**
     *  所属权限编号,所属表字段为t_rbac_role_permission_rel.PERMISSION_ID
     */
    @MyBatisColumnAnnotation(name = "PERMISSION_ID", value = "t_rbac_role_permission_rel_PERMISSION_ID", chineseNote = "所属权限编号", tableAlias = "t_rbac_role_permission_rel")
    @ApiModelProperty(value = "所属权限编号")
    private String permissionId;

    @Override
    public String getDynamicTableName() {
        return "t_rbac_role_permission_rel";
    }

    private static final long serialVersionUID = 1L;

    public String getRolePermissionRelId() {
        return rolePermissionRelId;
    }

    public void setRolePermissionRelId(String rolePermissionRelId) {
        this.rolePermissionRelId = rolePermissionRelId;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getPermissionGroupId() {
        return permissionGroupId;
    }

    public void setPermissionGroupId(String permissionGroupId) {
        this.permissionGroupId = permissionGroupId;
    }

    public String getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(String permissionId) {
        this.permissionId = permissionId;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", rolePermissionRelId=").append(rolePermissionRelId);
        sb.append(", roleId=").append(roleId);
        sb.append(", permissionGroupId=").append(permissionGroupId);
        sb.append(", permissionId=").append(permissionId);
        sb.append("]");
        return sb.toString();
    }
}