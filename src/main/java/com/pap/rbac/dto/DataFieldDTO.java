package com.pap.rbac.dto;

import com.pap.base.entity.PapBaseEntity;
import com.pap.base.mybatis.plugin.annotation.MyBatisColumnAnnotation;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * @Auther: alexgaoyh
 * @Date: 2018/12/25 14:50
 * @Description:
 */
@ApiModel(value = "DataFieldDTO", description = "数据模块类属性")
public class DataFieldDTO extends PapBaseEntity implements Serializable {
    /**
     *  编号,所属表字段为t_rbac_data_field.DATA_FIELD_ID
     */
    @MyBatisColumnAnnotation(name = "DATA_FIELD_ID", value = "t_rbac_data_field_DATA_FIELD_ID", chineseNote = "编号", tableAlias = "t_rbac_data_field")
    @ApiModelProperty(value = "编号")
    private String dataFieldId;

    /**
     *  编码,所属表字段为t_rbac_data_field.DATA_FIELD_CODE
     */
    @MyBatisColumnAnnotation(name = "DATA_FIELD_CODE", value = "t_rbac_data_field_DATA_FIELD_CODE", chineseNote = "编码", tableAlias = "t_rbac_data_field")
    @ApiModelProperty(value = "编码")
    private String dataFieldCode;

    /**
     *  所属项目编号,所属表字段为t_rbac_data_field.DATA_PROJECT_ID
     */
    @MyBatisColumnAnnotation(name = "DATA_PROJECT_ID", value = "t_rbac_data_field_DATA_PROJECT_ID", chineseNote = "所属项目编号", tableAlias = "t_rbac_data_field")
    @ApiModelProperty(value = "所属项目编号")
    private String dataProjectId;

    /**
     *  所属模块编号,所属表字段为t_rbac_data_field.DATA_MODULE_ID
     */
    @MyBatisColumnAnnotation(name = "DATA_MODULE_ID", value = "t_rbac_data_field_DATA_MODULE_ID", chineseNote = "所属模块编号", tableAlias = "t_rbac_data_field")
    @ApiModelProperty(value = "所属模块编号")
    private String dataModuleId;

    /**
     *  所属类路径编号,所属表字段为t_rbac_data_field.DATA_CLAZZ_ID
     */
    @MyBatisColumnAnnotation(name = "DATA_CLAZZ_ID", value = "t_rbac_data_field_DATA_CLAZZ_ID", chineseNote = "所属类路径编号", tableAlias = "t_rbac_data_field")
    @ApiModelProperty(value = "所属类路径编号")
    private String dataClazzId;

    /**
     *  属性名称,所属表字段为t_rbac_data_field.DATA_FIELD_NAME
     */
    @MyBatisColumnAnnotation(name = "DATA_FIELD_NAME", value = "t_rbac_data_field_DATA_FIELD_NAME", chineseNote = "属性名称", tableAlias = "t_rbac_data_field")
    @ApiModelProperty(value = "属性名称")
    private String dataFieldName;

    /**
     *  属性值,所属表字段为t_rbac_data_field.DATA_FIELD_VALUE
     */
    @MyBatisColumnAnnotation(name = "DATA_FIELD_VALUE", value = "t_rbac_data_field_DATA_FIELD_VALUE", chineseNote = "属性值", tableAlias = "t_rbac_data_field")
    @ApiModelProperty(value = "属性值")
    private String dataFieldValue;

    /**
     *  可用状态标识,所属表字段为t_rbac_data_field.DISABLE_FLAG
     */
    @MyBatisColumnAnnotation(name = "DISABLE_FLAG", value = "t_rbac_data_field_DISABLE_FLAG", chineseNote = "可用状态标识", tableAlias = "t_rbac_data_field")
    @ApiModelProperty(value = "可用状态标识")
    private String disableFlag;

    /**
     *  删除状态标识,所属表字段为t_rbac_data_field.DELETE_FLAG
     */
    @MyBatisColumnAnnotation(name = "DELETE_FLAG", value = "t_rbac_data_field_DELETE_FLAG", chineseNote = "删除状态标识", tableAlias = "t_rbac_data_field")
    @ApiModelProperty(value = "删除状态标识")
    private String deleteFlag;

    /**
     *  备注,所属表字段为t_rbac_data_field.REMARK
     */
    @MyBatisColumnAnnotation(name = "REMARK", value = "t_rbac_data_field_REMARK", chineseNote = "备注", tableAlias = "t_rbac_data_field")
    @ApiModelProperty(value = "备注")
    private String remark;

    public String getDynamicTableName() {
        return "t_rbac_data_field";
    }

    private static final long serialVersionUID = 1L;

    public String getDataFieldId() {
        return dataFieldId;
    }

    public void setDataFieldId(String dataFieldId) {
        this.dataFieldId = dataFieldId;
    }

    public String getDataFieldCode() {
        return dataFieldCode;
    }

    public void setDataFieldCode(String dataFieldCode) {
        this.dataFieldCode = dataFieldCode;
    }

    public String getDataProjectId() {
        return dataProjectId;
    }

    public void setDataProjectId(String dataProjectId) {
        this.dataProjectId = dataProjectId;
    }

    public String getDataModuleId() {
        return dataModuleId;
    }

    public void setDataModuleId(String dataModuleId) {
        this.dataModuleId = dataModuleId;
    }

    public String getDataClazzId() {
        return dataClazzId;
    }

    public void setDataClazzId(String dataClazzId) {
        this.dataClazzId = dataClazzId;
    }

    public String getDataFieldName() {
        return dataFieldName;
    }

    public void setDataFieldName(String dataFieldName) {
        this.dataFieldName = dataFieldName;
    }

    public String getDataFieldValue() {
        return dataFieldValue;
    }

    public void setDataFieldValue(String dataFieldValue) {
        this.dataFieldValue = dataFieldValue;
    }

    public String getDisableFlag() {
        return disableFlag;
    }

    public void setDisableFlag(String disableFlag) {
        this.disableFlag = disableFlag;
    }

    public String getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(String deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", dataFieldId=").append(dataFieldId);
        sb.append(", dataFieldCode=").append(dataFieldCode);
        sb.append(", dataProjectId=").append(dataProjectId);
        sb.append(", dataModuleId=").append(dataModuleId);
        sb.append(", dataClazzId=").append(dataClazzId);
        sb.append(", dataFieldName=").append(dataFieldName);
        sb.append(", dataFieldValue=").append(dataFieldValue);
        sb.append(", disableFlag=").append(disableFlag);
        sb.append(", deleteFlag=").append(deleteFlag);
        sb.append(", remark=").append(remark);
        sb.append("]");
        return sb.toString();
    }
}
