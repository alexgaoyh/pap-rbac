package com.pap.rbac.dto;

import com.pap.base.entity.PapBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * @Auther: alexgaoyh
 * @Date: 2018/12/25 14:41
 * @Description:
 */
@ApiModel(value = "DataModuleDTO", description = "数据模块项目")
public class DataModuleDTO extends PapBaseEntity implements Serializable {
    /**
     *  编号,所属表字段为t_rbac_data_module.DATA_MODULE_ID
     */
    @ApiModelProperty(value = "编号")
    private String dataModuleId;

    /**
     *  编码,所属表字段为t_rbac_data_module.DATA_MODULE_CODE
     */
    @ApiModelProperty(value = "编码")
    private String dataModuleCode;

    /**
     *  所属项目编号,所属表字段为t_rbac_data_module.DATA_PROJECT_ID
     */
    @ApiModelProperty(value = "所属项目编号")
    private String dataProjectId;

    /**
     *  姓名,所属表字段为t_rbac_data_module.DATA_MODULE_NAME
     */
    @ApiModelProperty(value = "姓名")
    private String dataModuleName;

    /**
     *  模块域名前缀,所属表字段为t_rbac_data_module.DATA_MODULE__DOMAIN
     */
    @ApiModelProperty(value = "模块域名前缀")
    private String dataModuleDomain;

    /**
     *  管理人员,所属表字段为t_rbac_data_module.DATA_MODULE_MANAGER
     */
    @ApiModelProperty(value = "管理人员")
    private String dataModuleManager;

    /**
     *  管理人员电话,所属表字段为t_rbac_data_module.DATA_MODULE_MOBILE
     */
    @ApiModelProperty(value = "管理人员电话")
    private String dataModuleMobile;

    /**
     *  可用状态标识,所属表字段为t_rbac_data_module.DISABLE_FLAG
     */
    @ApiModelProperty(value = "可用状态标识")
    private String disableFlag;

    /**
     *  删除状态标识,所属表字段为t_rbac_data_module.DELETE_FLAG
     */
    @ApiModelProperty(value = "删除状态标识")
    private String deleteFlag;

    /**
     *  备注,所属表字段为t_rbac_data_module.REMARK
     */
    @ApiModelProperty(value = "备注")
    private String remark;

    public String getDynamicTableName() {
        return "t_rbac_data_module";
    }

    public String getDataModuleId() {
        return dataModuleId;
    }

    public void setDataModuleId(String dataModuleId) {
        this.dataModuleId = dataModuleId;
    }

    public String getDataModuleCode() {
        return dataModuleCode;
    }

    public void setDataModuleCode(String dataModuleCode) {
        this.dataModuleCode = dataModuleCode;
    }

    public String getDataProjectId() {
        return dataProjectId;
    }

    public void setDataProjectId(String dataProjectId) {
        this.dataProjectId = dataProjectId;
    }

    public String getDataModuleName() {
        return dataModuleName;
    }

    public void setDataModuleName(String dataModuleName) {
        this.dataModuleName = dataModuleName;
    }

    public String getDataModuleDomain() {
        return dataModuleDomain;
    }

    public void setDataModuleDomain(String dataModuleDomain) {
        this.dataModuleDomain = dataModuleDomain;
    }

    public String getDataModuleManager() {
        return dataModuleManager;
    }

    public void setDataModuleManager(String dataModuleManager) {
        this.dataModuleManager = dataModuleManager;
    }

    public String getDataModuleMobile() {
        return dataModuleMobile;
    }

    public void setDataModuleMobile(String dataModuleMobile) {
        this.dataModuleMobile = dataModuleMobile;
    }

    public String getDisableFlag() {
        return disableFlag;
    }

    public void setDisableFlag(String disableFlag) {
        this.disableFlag = disableFlag;
    }

    public String getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(String deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "DataModuleDTO{" +
                "dataModuleId='" + dataModuleId + '\'' +
                ", dataModuleCode='" + dataModuleCode + '\'' +
                ", dataProjectId='" + dataProjectId + '\'' +
                ", dataModuleName='" + dataModuleName + '\'' +
                ", dataModuleDomain='" + dataModuleDomain + '\'' +
                ", dataModuleManager='" + dataModuleManager + '\'' +
                ", dataModuleMobile='" + dataModuleMobile + '\'' +
                ", disableFlag='" + disableFlag + '\'' +
                ", deleteFlag='" + deleteFlag + '\'' +
                ", remark='" + remark + '\'' +
                '}';
    }
}
