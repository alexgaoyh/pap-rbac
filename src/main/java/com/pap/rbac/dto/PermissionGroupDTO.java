package com.pap.rbac.dto;

import com.pap.base.entity.PapBaseEntity;
import com.pap.base.mybatis.plugin.annotation.MyBatisColumnAnnotation;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(value = "PermissionGroupDTO", description = "权限组")
public class PermissionGroupDTO extends PapBaseEntity implements Serializable {
    /**
     *  编号，主键,所属表字段为t_rbac_permission_group.PERMISSION_GROUP_ID
     */
    @MyBatisColumnAnnotation(name = "PERMISSION_GROUP_ID", value = "t_rbac_permission_group_PERMISSION_GROUP_ID", chineseNote = "编号，主键", tableAlias = "t_rbac_permission_group")
    @ApiModelProperty(value = "编号，主键")
    private String permissionGroupId;

    /**
     *  业务编码,所属表字段为t_rbac_permission_group.PERMISSION_GROUP_CODE
     */
    @MyBatisColumnAnnotation(name = "PERMISSION_GROUP_CODE", value = "t_rbac_permission_group_PERMISSION_GROUP_CODE", chineseNote = "业务编码", tableAlias = "t_rbac_permission_group")
    @ApiModelProperty(value = "业务编码")
    private String permissionGroupCode;

    /**
     *  名称,所属表字段为t_rbac_permission_group.PERMISSION_GROUP_NAME
     */
    @MyBatisColumnAnnotation(name = "PERMISSION_GROUP_NAME", value = "t_rbac_permission_group_PERMISSION_GROUP_NAME", chineseNote = "名称", tableAlias = "t_rbac_permission_group")
    @ApiModelProperty(value = "名称")
    private String permissionGroupName;

    /**
     *  父级节点编号,所属表字段为t_rbac_permission_group.PERMISSION_GROUP_PARENT_ID
     */
    @MyBatisColumnAnnotation(name = "PERMISSION_GROUP_PARENT_ID", value = "t_rbac_permission_group_PERMISSION_GROUP_PARENT_ID", chineseNote = "父级节点编号", tableAlias = "t_rbac_permission_group")
    @ApiModelProperty(value = "父级节点编号")
    private String permissionGroupParentId;

    /**
     *  编号路径集合,所属表字段为t_rbac_permission_group.PATH_IDS
     */
    @MyBatisColumnAnnotation(name = "PATH_IDS", value = "t_rbac_permission_group_PATH_IDS", chineseNote = "编号路径集合", tableAlias = "t_rbac_permission_group")
    @ApiModelProperty(value = "编号路径集合")
    private String pathIds;

    /**
     *  业务编码路径集合,所属表字段为t_rbac_permission_group.PATH_CODES
     */
    @MyBatisColumnAnnotation(name = "PATH_CODES", value = "t_rbac_permission_group_PATH_CODES", chineseNote = "业务编码路径集合", tableAlias = "t_rbac_permission_group")
    @ApiModelProperty(value = "业务编码路径集合")
    private String pathCodes;

    /**
     *  名称路径集合,所属表字段为t_rbac_permission_group.PATH_NAMES
     */
    @MyBatisColumnAnnotation(name = "PATH_NAMES", value = "t_rbac_permission_group_PATH_NAMES", chineseNote = "名称路径集合", tableAlias = "t_rbac_permission_group")
    @ApiModelProperty(value = "名称路径集合")
    private String pathNames;

    /**
     *  备注字段,所属表字段为t_rbac_permission_group.REMARK
     */
    @MyBatisColumnAnnotation(name = "REMARK", value = "t_rbac_permission_group_REMARK", chineseNote = "备注字段", tableAlias = "t_rbac_permission_group")
    @ApiModelProperty(value = "备注字段")
    private String remark;

    @Override
    public String getDynamicTableName() {
        return "t_rbac_permission_group";
    }

    private static final long serialVersionUID = 1L;

    public String getPermissionGroupId() {
        return permissionGroupId;
    }

    public void setPermissionGroupId(String permissionGroupId) {
        this.permissionGroupId = permissionGroupId;
    }

    public String getPermissionGroupCode() {
        return permissionGroupCode;
    }

    public void setPermissionGroupCode(String permissionGroupCode) {
        this.permissionGroupCode = permissionGroupCode;
    }

    public String getPermissionGroupName() {
        return permissionGroupName;
    }

    public void setPermissionGroupName(String permissionGroupName) {
        this.permissionGroupName = permissionGroupName;
    }

    public String getPermissionGroupParentId() {
        return permissionGroupParentId;
    }

    public void setPermissionGroupParentId(String permissionGroupParentId) {
        this.permissionGroupParentId = permissionGroupParentId;
    }

    public String getPathIds() {
        return pathIds;
    }

    public void setPathIds(String pathIds) {
        this.pathIds = pathIds;
    }

    public String getPathCodes() {
        return pathCodes;
    }

    public void setPathCodes(String pathCodes) {
        this.pathCodes = pathCodes;
    }

    public String getPathNames() {
        return pathNames;
    }

    public void setPathNames(String pathNames) {
        this.pathNames = pathNames;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", permissionGroupId=").append(permissionGroupId);
        sb.append(", permissionGroupCode=").append(permissionGroupCode);
        sb.append(", permissionGroupName=").append(permissionGroupName);
        sb.append(", permissionGroupParentId=").append(permissionGroupParentId);
        sb.append(", pathIds=").append(pathIds);
        sb.append(", pathCodes=").append(pathCodes);
        sb.append(", pathNames=").append(pathNames);
        sb.append(", remark=").append(remark);
        sb.append("]");
        return sb.toString();
    }
}