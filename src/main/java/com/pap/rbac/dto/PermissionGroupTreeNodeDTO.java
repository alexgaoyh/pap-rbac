package com.pap.rbac.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

@ApiModel(value = "PermissionGroupTreeNodeDTO", description = "权限组树形对象")
public class PermissionGroupTreeNodeDTO extends PermissionGroupDTO {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "孩子节点")
    private List<PermissionGroupTreeNodeDTO> children = null;

    public List<PermissionGroupTreeNodeDTO> getChildren() {
        if(children == null || children.size() == 0) {
            return null;
        } else {
            return children;
        }
    }

    public void setChildren(List<PermissionGroupTreeNodeDTO> children) {
        this.children = children;
    }

    @Override
    public String toString() {
        return "RoleTreeNodeVO [children=" + children + ", toString()=" + super.toString() + "]";
    }
}