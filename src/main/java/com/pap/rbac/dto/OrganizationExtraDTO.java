package com.pap.rbac.dto;

import com.pap.base.entity.PapBaseEntity;
import com.pap.base.mybatis.plugin.annotation.MyBatisColumnAnnotation;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * @Auther: alexgaoyh
 * @Date: 2018/12/28 14:13
 * @Description:
 */
@ApiModel(value = "OrganizationExtraDTO", description = "组织机构扩展字段")
public class OrganizationExtraDTO extends PapBaseEntity implements Serializable {
    /**
     *  编号，主键,所属表字段为t_rbac_organization_extra.ORGANIZATION_EXTRA_ID
     */
    @MyBatisColumnAnnotation(name = "ORGANIZATION_EXTRA_ID", value = "t_rbac_organization_extra_ORGANIZATION_EXTRA_ID", chineseNote = "编号，主键", tableAlias = "t_rbac_organization_extra")
    @ApiModelProperty(value = "编号，主键")
    private String organizationExtraId;

    /**
     *  所属组织,所属表字段为t_rbac_organization_extra.ORGANIZATION_ID
     */
    @MyBatisColumnAnnotation(name = "ORGANIZATION_ID", value = "t_rbac_organization_extra_ORGANIZATION_ID", chineseNote = "所属组织", tableAlias = "t_rbac_organization_extra")
    @ApiModelProperty(value = "所属组织")
    private String organizationId;

    /**
     *  数据键,所属表字段为t_rbac_organization_extra.ATTR_KEY
     */
    @MyBatisColumnAnnotation(name = "ATTR_KEY", value = "t_rbac_organization_extra_ATTR_KEY", chineseNote = "数据键", tableAlias = "t_rbac_organization_extra")
    @ApiModelProperty(value = "数据键")
    private String attrKey;

    /**
     *  数据值,所属表字段为t_rbac_organization_extra.ATTR_VALUE
     */
    @MyBatisColumnAnnotation(name = "ATTR_VALUE", value = "t_rbac_organization_extra_ATTR_VALUE", chineseNote = "数据值", tableAlias = "t_rbac_organization_extra")
    @ApiModelProperty(value = "数据值")
    private String attrValue;

    /**
     *  数据备注字段,所属表字段为t_rbac_organization_extra.ATTR_REMARK
     */
    @MyBatisColumnAnnotation(name = "ATTR_REMARK", value = "t_rbac_organization_extra_ATTR_REMARK", chineseNote = "数据备注字段", tableAlias = "t_rbac_organization_extra")
    @ApiModelProperty(value = "数据备注字段")
    private String attrRemark;

    public String getOrganizationExtraId() {
        return organizationExtraId;
    }

    public void setOrganizationExtraId(String organizationExtraId) {
        this.organizationExtraId = organizationExtraId;
    }

    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    public String getAttrKey() {
        return attrKey;
    }

    public void setAttrKey(String attrKey) {
        this.attrKey = attrKey;
    }

    public String getAttrValue() {
        return attrValue;
    }

    public void setAttrValue(String attrValue) {
        this.attrValue = attrValue;
    }

    public String getAttrRemark() {
        return attrRemark;
    }

    public void setAttrRemark(String attrRemark) {
        this.attrRemark = attrRemark;
    }

    @Override
    public String toString() {
        return "OrganizationExtraDTO{" +
                "organizationExtraId='" + organizationExtraId + '\'' +
                ", organizationId='" + organizationId + '\'' +
                ", attrKey='" + attrKey + '\'' +
                ", attrValue='" + attrValue + '\'' +
                ", attrRemark='" + attrRemark + '\'' +
                '}';
    }
}
