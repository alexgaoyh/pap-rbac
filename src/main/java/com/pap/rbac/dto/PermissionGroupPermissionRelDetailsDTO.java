package com.pap.rbac.dto;

import com.pap.base.entity.PapBaseEntity;
import io.swagger.annotations.ApiModel;

import java.io.Serializable;
import java.util.List;

@ApiModel(value = "PermissionGroupPermissionRelDetailsDTO", description = "权限组权限关联关系")
public class PermissionGroupPermissionRelDetailsDTO extends PapBaseEntity implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * 权限组编号
     */
    private String permissionGroupId;

    /**
     * 权限集合
     */
    private List<String> details;

    public String getPermissionGroupId() {
        return permissionGroupId;
    }

    public void setPermissionGroupId(String permissionGroupId) {
        this.permissionGroupId = permissionGroupId;
    }

    public List<String> getDetails() {
        return details;
    }

    public void setDetails(List<String> details) {
        this.details = details;
    }

    @Override
    public String toString() {
        return "PermissionGroupPermissionRelDetailsDTO{" +
                "permissionGroupId='" + permissionGroupId + '\'' +
                ", details=" + details +
                '}';
    }
}