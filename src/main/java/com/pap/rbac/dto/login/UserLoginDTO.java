package com.pap.rbac.dto.login;

import com.pap.base.mybatis.plugin.annotation.MyBatisColumnAnnotation;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(value = "UserLoginDTO", description = "用户登录属性")
public class UserLoginDTO implements Serializable {

    /**
     *  登录名,所属表字段为t_rbac_user.USER_LOGIN_NAME
     */
    @MyBatisColumnAnnotation(name = "USER_LOGIN_NAME", value = "t_rbac_user_USER_LOGIN_NAME", chineseNote = "登录名", tableAlias = "t_rbac_user")
    @ApiModelProperty(value = "登录名")
    private String userLoginName;

    /**
     *  密码,所属表字段为t_rbac_user.USER_PASSWORDS
     */
    @MyBatisColumnAnnotation(name = "USER_PASSWORDS", value = "t_rbac_user_USER_PASSWORDS", chineseNote = "密码", tableAlias = "t_rbac_user")
    @ApiModelProperty(value = "密码")
    private String userPasswords;

    public String getUserLoginName() {
        return userLoginName;
    }

    public void setUserLoginName(String userLoginName) {
        this.userLoginName = userLoginName;
    }

    public String getUserPasswords() {
        return userPasswords;
    }

    public void setUserPasswords(String userPasswords) {
        this.userPasswords = userPasswords;
    }

    @Override
    public String toString() {
        return "UserLoginDTO{" +
                "userLoginName='" + userLoginName + '\'' +
                ", userPasswords='" + userPasswords + '\'' +
                '}';
    }
}
