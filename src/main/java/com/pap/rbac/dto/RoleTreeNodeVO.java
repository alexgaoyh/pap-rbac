package com.pap.rbac.dto;

import java.util.List;

import com.pap.rbac.role.entity.Role;
import io.swagger.annotations.ApiModel;

@ApiModel(value = "RoleTreeNodeVO", description = "角色对象树形结构")
public class RoleTreeNodeVO extends Role {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<RoleTreeNodeVO> children = null;

	public List<RoleTreeNodeVO> getChildren() {
		if(children == null || children.size() == 0) {
			return null;
		} else {
			return children;
		}
	}

	public void setChildren(List<RoleTreeNodeVO> children) {
		this.children = children;
	}

	@Override
	public String toString() {
		return "RoleTreeNodeVO [children=" + children + ", toString()=" + super.toString() + "]";
	}

}
