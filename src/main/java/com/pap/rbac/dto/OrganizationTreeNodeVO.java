package com.pap.rbac.dto;

import java.util.List;

import io.swagger.annotations.ApiModel;

@ApiModel(value = "OrganizationTreeNodeVO", description = "组织机构属性结构")
public class OrganizationTreeNodeVO extends OrganizationDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	
	private List<OrganizationTreeNodeVO> children = null;

	/**
	 * add 
	 * 为防止在json格式化的时候，这里会出现一个空的children节点， children=[]
	 * 需要多一步判断，如果为空数组的话，默认设置为null
	 * @return
	 */
	public List<OrganizationTreeNodeVO> getChildren() {
		if(children == null || children.size() == 0) {
			return null;
		} else {
			return children;
		}
	}

	public void setChildren(List<OrganizationTreeNodeVO> children) {
		this.children = children;
	}

	@Override
	public String toString() {
		return "OrganizationTreeNodeVO [children=" + children + ", toString()=" + super.toString() + "]";
	}
	
}
