package com.pap.rbac.dto;

import com.pap.base.entity.PapBaseEntity;
import com.pap.base.mybatis.plugin.annotation.MyBatisColumnAnnotation;
import com.pap.base.mybatis.plugin.annotation.MyBatisTableAnnotation;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(value = "PermissionDTO", description = "权限对象")
public class PermissionDTO extends PapBaseEntity implements Serializable {
    /**
     *  权限编号,所属表字段为t_rbac_permission.PERMISSION_ID
     */
    @MyBatisColumnAnnotation(name = "PERMISSION_ID", value = "t_rbac_permission_PERMISSION_ID", chineseNote = "权限编号", tableAlias = "t_rbac_permission")
    @ApiModelProperty(value = "权限编号")
    private String permissionId;

    /**
     *  权限编码,所属表字段为t_rbac_permission.PERMISSION_CODE
     */
    @MyBatisColumnAnnotation(name = "PERMISSION_CODE", value = "t_rbac_permission_PERMISSION_CODE", chineseNote = "权限编码", tableAlias = "t_rbac_permission")
    @ApiModelProperty(value = "权限编码")
    private String permissionCode;

    /**
     *  权限名称,所属表字段为t_rbac_permission.PERMISSION_NAME
     */
    @MyBatisColumnAnnotation(name = "PERMISSION_NAME", value = "t_rbac_permission_PERMISSION_NAME", chineseNote = "权限名称", tableAlias = "t_rbac_permission")
    @ApiModelProperty(value = "权限名称")
    private String permissionName;

    /**
     *  备注,所属表字段为t_rbac_permission.REMARK
     */
    @MyBatisColumnAnnotation(name = "REMARK", value = "t_rbac_permission_REMARK", chineseNote = "备注", tableAlias = "t_rbac_permission")
    @ApiModelProperty(value = "备注")
    private String remark;

    @Override
    public String getDynamicTableName() {
        return "t_rbac_permission";
    }

    private static final long serialVersionUID = 1L;

    public String getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(String permissionId) {
        this.permissionId = permissionId;
    }

    public String getPermissionCode() {
        return permissionCode;
    }

    public void setPermissionCode(String permissionCode) {
        this.permissionCode = permissionCode;
    }

    public String getPermissionName() {
        return permissionName;
    }

    public void setPermissionName(String permissionName) {
        this.permissionName = permissionName;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", permissionId=").append(permissionId);
        sb.append(", permissionCode=").append(permissionCode);
        sb.append(", permissionName=").append(permissionName);
        sb.append(", remark=").append(remark);
        sb.append("]");
        return sb.toString();
    }
}