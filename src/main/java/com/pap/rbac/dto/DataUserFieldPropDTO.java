package com.pap.rbac.dto;

import com.pap.base.entity.PapBaseEntity;
import com.pap.base.mybatis.plugin.annotation.MyBatisColumnAnnotation;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * @Auther: alexgaoyh
 * @Date: 2018/12/25 15:37
 * @Description:
 */
@ApiModel(value = "DataUserFieldPropDTO", description = "数据权限用户属性")
public class DataUserFieldPropDTO extends PapBaseEntity implements Serializable {
    /**
     *  编号,所属表字段为t_rbac_data_user_field_prop.USER_FIELD_ID
     */
    @MyBatisColumnAnnotation(name = "USER_FIELD_ID", value = "t_rbac_data_user_field_prop_USER_FIELD_ID", chineseNote = "编号", tableAlias = "t_rbac_data_user_field_prop")
    @ApiModelProperty(value = "编号")
    private String dataUserFieldId;

    /**
     *  编码,所属表字段为t_rbac_data_user_field_prop.USER_FIELD_CODE
     */
    @MyBatisColumnAnnotation(name = "USER_FIELD_CODE", value = "t_rbac_data_user_field_prop_USER_FIELD_CODE", chineseNote = "编码", tableAlias = "t_rbac_data_user_field_prop")
    @ApiModelProperty(value = "编码")
    private String userFieldCode;

    /**
     *  所属用户,所属表字段为t_rbac_data_user_field_prop.USER_ID
     */
    @MyBatisColumnAnnotation(name = "USER_ID", value = "t_rbac_data_user_field_prop_USER_ID", chineseNote = "所属用户", tableAlias = "t_rbac_data_user_field_prop")
    @ApiModelProperty(value = "所属用户")
    private String userId;

    /**
     *  所属项目编号,所属表字段为t_rbac_data_user_field_prop.DATA_PROJECT_ID
     */
    @MyBatisColumnAnnotation(name = "DATA_PROJECT_ID", value = "t_rbac_data_user_field_prop_DATA_PROJECT_ID", chineseNote = "所属项目编号", tableAlias = "t_rbac_data_user_field_prop")
    @ApiModelProperty(value = "所属项目编号")
    private String dataProjectId;

    /**
     *  所属模块编号,所属表字段为t_rbac_data_user_field_prop.DATA_MODULE_ID
     */
    @MyBatisColumnAnnotation(name = "DATA_MODULE_ID", value = "t_rbac_data_user_field_prop_DATA_MODULE_ID", chineseNote = "所属模块编号", tableAlias = "t_rbac_data_user_field_prop")
    @ApiModelProperty(value = "所属模块编号")
    private String dataModuleId;

    /**
     *  所属类路径编号,所属表字段为t_rbac_data_user_field_prop.DATA_CLAZZ_ID
     */
    @MyBatisColumnAnnotation(name = "DATA_CLAZZ_ID", value = "t_rbac_data_user_field_prop_DATA_CLAZZ_ID", chineseNote = "所属类路径编号", tableAlias = "t_rbac_data_user_field_prop")
    @ApiModelProperty(value = "所属类路径编号")
    private String dataClazzId;

    /**
     *  所属权限字段,所属表字段为t_rbac_data_user_field_prop.DATA_FIELD_ID
     */
    @MyBatisColumnAnnotation(name = "DATA_FIELD_ID", value = "t_rbac_data_user_field_prop_DATA_FIELD_ID", chineseNote = "所属权限字段", tableAlias = "t_rbac_data_user_field_prop")
    @ApiModelProperty(value = "所属权限字段")
    private String dataFieldId;

    /**
     *  属性操作符,所属表字段为t_rbac_data_user_field_prop.PROP_OPER
     */
    @MyBatisColumnAnnotation(name = "PROP_OPER", value = "t_rbac_data_user_field_prop_PROP_OPER", chineseNote = "属性操作符", tableAlias = "t_rbac_data_user_field_prop")
    @ApiModelProperty(value = "属性操作符")
    private String propOper;

    /**
     *  属性值,所属表字段为t_rbac_data_user_field_prop.PROP_VALUE
     */
    @MyBatisColumnAnnotation(name = "PROP_VALUE", value = "t_rbac_data_user_field_prop_PROP_VALUE", chineseNote = "属性值", tableAlias = "t_rbac_data_user_field_prop")
    @ApiModelProperty(value = "属性值")
    private String propValue;

    /**
     *  备注,所属表字段为t_rbac_data_user_field_prop.REMARK
     */
    @MyBatisColumnAnnotation(name = "REMARK", value = "t_rbac_data_user_field_prop_REMARK", chineseNote = "备注", tableAlias = "t_rbac_data_user_field_prop")
    @ApiModelProperty(value = "备注")
    private String remark;

    public String getDynamicTableName() {
        return "t_rbac_data_user_field_prop";
    }

    private static final long serialVersionUID = 1L;

    public String getDataUserFieldId() {
        return dataUserFieldId;
    }

    public void setDataUserFieldId(String dataUserFieldId) {
        this.dataUserFieldId = dataUserFieldId;
    }

    public String getUserFieldCode() {
        return userFieldCode;
    }

    public void setUserFieldCode(String userFieldCode) {
        this.userFieldCode = userFieldCode;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDataProjectId() {
        return dataProjectId;
    }

    public void setDataProjectId(String dataProjectId) {
        this.dataProjectId = dataProjectId;
    }

    public String getDataModuleId() {
        return dataModuleId;
    }

    public void setDataModuleId(String dataModuleId) {
        this.dataModuleId = dataModuleId;
    }

    public String getDataClazzId() {
        return dataClazzId;
    }

    public void setDataClazzId(String dataClazzId) {
        this.dataClazzId = dataClazzId;
    }

    public String getDataFieldId() {
        return dataFieldId;
    }

    public void setDataFieldId(String dataFieldId) {
        this.dataFieldId = dataFieldId;
    }

    public String getPropOper() {
        return propOper;
    }

    public void setPropOper(String propOper) {
        this.propOper = propOper;
    }

    public String getPropValue() {
        return propValue;
    }

    public void setPropValue(String propValue) {
        this.propValue = propValue;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", dataUserFieldId=").append(dataUserFieldId);
        sb.append(", userFieldCode=").append(userFieldCode);
        sb.append(", userId=").append(userId);
        sb.append(", dataProjectId=").append(dataProjectId);
        sb.append(", dataModuleId=").append(dataModuleId);
        sb.append(", dataClazzId=").append(dataClazzId);
        sb.append(", dataFieldId=").append(dataFieldId);
        sb.append(", propOper=").append(propOper);
        sb.append(", propValue=").append(propValue);
        sb.append(", remark=").append(remark);
        sb.append("]");
        return sb.toString();
    }

}
