package com.pap.rbac.dto;

import com.pap.base.entity.PapBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

@ApiModel(value = "RolePermissionRelDetailsDTO", description = "角色权限关联对象")
public class RolePermissionRelDetailsDTO extends PapBaseEntity implements Serializable {

    /**
     *  所属角色编号,所属表字段为t_rbac_role_permission_rel.ROLE_ID
     */
    @ApiModelProperty(value = "所属角色编号")
    private String roleId;

    @ApiModelProperty(value = "权限组编号集合")
    private List<String> details;

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public List<String> getDetails() {
        return details;
    }

    public void setDetails(List<String> details) {
        this.details = details;
    }

    @Override
    public String toString() {
        return "RolePermissionRelDetailsDTO{" +
                "roleId='" + roleId + '\'' +
                ", details=" + details +
                '}';
    }
}