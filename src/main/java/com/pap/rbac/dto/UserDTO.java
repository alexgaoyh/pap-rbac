package com.pap.rbac.dto;

import com.pap.base.entity.PapBaseEntity;
import com.pap.base.mybatis.plugin.annotation.MyBatisColumnAnnotation;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * @Auther: alexgaoyh
 * @Date: 2018/12/25 15:44
 * @Description:
 */
@ApiModel(value = "UserDTO", description = "用户属性")
public class UserDTO extends PapBaseEntity implements Serializable {
    /**
     *  编号,所属表字段为t_rbac_user.USER_ID
     */
    @MyBatisColumnAnnotation(name = "USER_ID", value = "t_rbac_user_USER_ID", chineseNote = "编号", tableAlias = "t_rbac_user")
    @ApiModelProperty(value = "编号")
    private String userId;

    /**
     *  编码,所属表字段为t_rbac_user.USER_CODE
     */
    @MyBatisColumnAnnotation(name = "USER_CODE", value = "t_rbac_user_USER_CODE", chineseNote = "编码", tableAlias = "t_rbac_user")
    @ApiModelProperty(value = "编码")
    private String userCode;

    /**
     *  姓名,所属表字段为t_rbac_user.USER_NAME
     */
    @MyBatisColumnAnnotation(name = "USER_NAME", value = "t_rbac_user_USER_NAME", chineseNote = "姓名", tableAlias = "t_rbac_user")
    @ApiModelProperty(value = "姓名")
    private String userName;

    /**
     *  邮箱,所属表字段为t_rbac_user.EMAIL
     */
    @MyBatisColumnAnnotation(name = "EMAIL", value = "t_rbac_user_EMAIL", chineseNote = "邮箱", tableAlias = "t_rbac_user")
    @ApiModelProperty(value = "邮箱")
    private String email;

    /**
     *  登录名,所属表字段为t_rbac_user.USER_LOGIN_NAME
     */
    @MyBatisColumnAnnotation(name = "USER_LOGIN_NAME", value = "t_rbac_user_USER_LOGIN_NAME", chineseNote = "登录名", tableAlias = "t_rbac_user")
    @ApiModelProperty(value = "登录名")
    private String userLoginName;

    /**
     *  密码,所属表字段为t_rbac_user.USER_PASSWORDS
     */
    @MyBatisColumnAnnotation(name = "USER_PASSWORDS", value = "t_rbac_user_USER_PASSWORDS", chineseNote = "密码", tableAlias = "t_rbac_user")
    @ApiModelProperty(value = "密码")
    private String userPasswords;

    /**
     *  电话,所属表字段为t_rbac_user.MOBILE
     */
    @MyBatisColumnAnnotation(name = "MOBILE", value = "t_rbac_user_MOBILE", chineseNote = "电话", tableAlias = "t_rbac_user")
    @ApiModelProperty(value = "电话")
    private String mobile;

    /**
     *  可用状态标识,所属表字段为t_rbac_user.DISABLE_FLAG
     */
    @MyBatisColumnAnnotation(name = "DISABLE_FLAG", value = "t_rbac_user_DISABLE_FLAG", chineseNote = "可用状态标识", tableAlias = "t_rbac_user")
    @ApiModelProperty(value = "可用状态标识")
    private String disableFlag;

    /**
     *  删除状态标识,所属表字段为t_rbac_user.DELETE_FLAG
     */
    @MyBatisColumnAnnotation(name = "DELETE_FLAG", value = "t_rbac_user_DELETE_FLAG", chineseNote = "删除状态标识", tableAlias = "t_rbac_user")
    @ApiModelProperty(value = "删除状态标识")
    private String deleteFlag;

    /**
     *  图标,所属表字段为t_rbac_user.ICON
     */
    @MyBatisColumnAnnotation(name = "ICON", value = "t_rbac_user_ICON", chineseNote = "图标", tableAlias = "t_rbac_user")
    @ApiModelProperty(value = "图标")
    private String icon;

    /**
     *  最近一次登录时间,所属表字段为t_rbac_user.LAST_LOGIN_TIME
     */
    @MyBatisColumnAnnotation(name = "LAST_LOGIN_TIME", value = "t_rbac_user_LAST_LOGIN_TIME", chineseNote = "最近一次登录时间", tableAlias = "t_rbac_user")
    @ApiModelProperty(value = "最近一次登录时间")
    private String lastLoginTime;

    /**
     *  备注,所属表字段为t_rbac_user.REMARK
     */
    @MyBatisColumnAnnotation(name = "REMARK", value = "t_rbac_user_REMARK", chineseNote = "备注", tableAlias = "t_rbac_user")
    @ApiModelProperty(value = "备注")
    private String remark;

    /**
     *  用户类型,所属表字段为t_rbac_user.USER_TYPE
     */
    @MyBatisColumnAnnotation(name = "USER_TYPE", value = "t_rbac_user_USER_TYPE", chineseNote = "用户类型", tableAlias = "t_rbac_user")
    @ApiModelProperty(value = "用户类型")
    private String userType;
    
    // 
    @ApiModelProperty(value = "用户唯一标示")
    private String uuid;

    @ApiModelProperty(value = "用户登录信息")
    private String papToken;

    public String getDynamicTableName() {
        return "t_rbac_user";
    }

    private static final long serialVersionUID = 1L;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserLoginName() {
        return userLoginName;
    }

    public void setUserLoginName(String userLoginName) {
        this.userLoginName = userLoginName;
    }

    public String getUserPasswords() {
        return userPasswords;
    }

    public void setUserPasswords(String userPasswords) {
        this.userPasswords = userPasswords;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getDisableFlag() {
        return disableFlag;
    }

    public void setDisableFlag(String disableFlag) {
        this.disableFlag = disableFlag;
    }

    public String getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(String deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(String lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getPapToken() {
        return papToken;
    }

    public void setPapToken(String papToken) {
        this.papToken = papToken;
    }

    @Override
    public String toString() {
        return "UserDTO{" +
                "userId='" + userId + '\'' +
                ", userCode='" + userCode + '\'' +
                ", userName='" + userName + '\'' +
                ", email='" + email + '\'' +
                ", userLoginName='" + userLoginName + '\'' +
                ", userPasswords='" + userPasswords + '\'' +
                ", mobile='" + mobile + '\'' +
                ", disableFlag='" + disableFlag + '\'' +
                ", deleteFlag='" + deleteFlag + '\'' +
                ", icon='" + icon + '\'' +
                ", lastLoginTime='" + lastLoginTime + '\'' +
                ", remark='" + remark + '\'' +
                ", userType='" + userType + '\'' +
                ", uuid='" + uuid + '\'' +
                ", papToken='" + papToken + '\'' +
                '}';
    }

}
