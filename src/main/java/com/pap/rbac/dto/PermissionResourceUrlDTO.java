package com.pap.rbac.dto;

import com.pap.base.entity.PapBaseEntity;
import com.pap.base.mybatis.plugin.annotation.MyBatisColumnAnnotation;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * @Auther: alexgaoyh
 * @Date: 2019/2/14 14:53
 * @Description:
 */
@ApiModel(value = "PermissionResourceUrlDTO", description = "HTTP请求协议权限对象")
public class PermissionResourceUrlDTO extends PapBaseEntity implements Serializable {
    /**
     *  权限编号,所属表字段为t_rbac_permission.PERMISSION_ID
     */
    @MyBatisColumnAnnotation(name = "PERMISSION_ID", value = "t_rbac_permission_PERMISSION_ID", chineseNote = "权限编号", tableAlias = "t_rbac_permission")
    @ApiModelProperty(value = "权限编号")
    private String permissionId;

    /**
     *  权限编码,所属表字段为t_rbac_permission.PERMISSION_CODE
     */
    @MyBatisColumnAnnotation(name = "PERMISSION_CODE", value = "t_rbac_permission_PERMISSION_CODE", chineseNote = "权限编码", tableAlias = "t_rbac_permission")
    @ApiModelProperty(value = "权限编码")
    private String permissionCode;

    /**
     *  权限名称,所属表字段为t_rbac_permission.PERMISSION_NAME
     */
    @MyBatisColumnAnnotation(name = "PERMISSION_NAME", value = "t_rbac_permission_PERMISSION_NAME", chineseNote = "权限名称", tableAlias = "t_rbac_permission")
    @ApiModelProperty(value = "权限名称")
    private String permissionName;

    /**
     *  备注,所属表字段为t_rbac_permission.REMARK
     */
    @MyBatisColumnAnnotation(name = "REMARK", value = "t_rbac_permission_REMARK", chineseNote = "备注", tableAlias = "t_rbac_permission")
    @ApiModelProperty(value = "备注")
    private String remark;

    /**
     *  权限编号,所属表字段为t_rbac_resource_url.RESOURCE_URL_ID
     */
    @MyBatisColumnAnnotation(name = "RESOURCE_URL_ID", value = "t_rbac_resource_url_RESOURCE_URL_ID", chineseNote = "权限编号", tableAlias = "t_rbac_resource_url")
    @ApiModelProperty(value = "权限编号")
    private String resourceUrlId;

    /**
     *  请求链接(支持正则),所属表字段为t_rbac_resource_url.REQUEST_URL
     */
    @MyBatisColumnAnnotation(name = "REQUEST_URL", value = "t_rbac_resource_url_REQUEST_URL", chineseNote = "请求链接(支持正则)", tableAlias = "t_rbac_resource_url")
    @ApiModelProperty(value = "请求链接(支持正则)")
    private String requestUrl;

    /**
     *  黑名单链接(相对于请求链接),所属表字段为t_rbac_resource_url.BLACK_URL
     */
    @MyBatisColumnAnnotation(name = "BLACK_URL", value = "t_rbac_resource_url_BLACK_URL", chineseNote = "黑名单链接(相对于请求链接)", tableAlias = "t_rbac_resource_url")
    @ApiModelProperty(value = "黑名单链接(相对于请求链接)")
    private String blackUrl;

    /**
     *  所属系统操作编号,所属表字段为t_rbac_resource_url.OPERATION_ID
     */
    @MyBatisColumnAnnotation(name = "OPERATION_ID", value = "t_rbac_resource_url_OPERATION_ID", chineseNote = "所属系统操作编号", tableAlias = "t_rbac_resource_url")
    @ApiModelProperty(value = "所属系统操作编号")
    private String operationId;

    /**
     *  请求协议,所属表字段为t_rbac_resource_url.HTTP_METHOD
     */
    @MyBatisColumnAnnotation(name = "HTTP_METHOD", value = "t_rbac_resource_url_HTTP_METHOD", chineseNote = "请求协议", tableAlias = "t_rbac_resource_url")
    @ApiModelProperty(value = "请求协议")
    private String httpMethod;

    public String getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(String permissionId) {
        this.permissionId = permissionId;
    }

    public String getPermissionCode() {
        return permissionCode;
    }

    public void setPermissionCode(String permissionCode) {
        this.permissionCode = permissionCode;
    }

    public String getPermissionName() {
        return permissionName;
    }

    public void setPermissionName(String permissionName) {
        this.permissionName = permissionName;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getResourceUrlId() {
        return resourceUrlId;
    }

    public void setResourceUrlId(String resourceUrlId) {
        this.resourceUrlId = resourceUrlId;
    }

    public String getRequestUrl() {
        return requestUrl;
    }

    public void setRequestUrl(String requestUrl) {
        this.requestUrl = requestUrl;
    }

    public String getBlackUrl() {
        return blackUrl;
    }

    public void setBlackUrl(String blackUrl) {
        this.blackUrl = blackUrl;
    }

    public String getOperationId() {
        return operationId;
    }

    public void setOperationId(String operationId) {
        this.operationId = operationId;
    }

    public String getHttpMethod() {
        return httpMethod;
    }

    public void setHttpMethod(String httpMethod) {
        this.httpMethod = httpMethod;
    }

    @Override
    public String toString() {
        return "PermissionResourceUrlDTO{" +
                "permissionId='" + permissionId + '\'' +
                ", permissionCode='" + permissionCode + '\'' +
                ", permissionName='" + permissionName + '\'' +
                ", remark='" + remark + '\'' +
                ", resourceUrlId='" + resourceUrlId + '\'' +
                ", requestUrl='" + requestUrl + '\'' +
                ", blackUrl='" + blackUrl + '\'' +
                ", operationId='" + operationId + '\'' +
                ", httpMethod='" + httpMethod + '\'' +
                '}';
    }
}
