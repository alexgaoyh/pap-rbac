package com.pap.rbac.dto;

import com.pap.base.entity.PapBaseEntity;
import com.pap.base.mybatis.plugin.annotation.MyBatisColumnAnnotation;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * @Auther: alexgaoyh
 * @Date: 2018/12/25 15:49
 * @Description:
 */
@ApiModel(value = "RoleDTO", description = "角色对象")
public class RoleDTO  extends PapBaseEntity implements Serializable {
    /**
     *  编号,所属表字段为t_rbac_role.ROLE_ID
     */
    @MyBatisColumnAnnotation(name = "ROLE_ID", value = "t_rbac_role_ROLE_ID", chineseNote = "编号", tableAlias = "t_rbac_role")
    @ApiModelProperty(value = "编号")
    private String roleId;

    /**
     *  编码,所属表字段为t_rbac_role.ROLE_CODE
     */
    @MyBatisColumnAnnotation(name = "ROLE_CODE", value = "t_rbac_role_ROLE_CODE", chineseNote = "编码", tableAlias = "t_rbac_role")
    @ApiModelProperty(value = "编码")
    private String roleCode;

    /**
     *  名称,所属表字段为t_rbac_role.ROLE_NAME
     */
    @MyBatisColumnAnnotation(name = "ROLE_NAME", value = "t_rbac_role_ROLE_NAME", chineseNote = "名称", tableAlias = "t_rbac_role")
    @ApiModelProperty(value = "名称")
    private String roleName;

    /**
     *  父节点编号,所属表字段为t_rbac_role.ROLE_PARENT_ID
     */
    @MyBatisColumnAnnotation(name = "ROLE_PARENT_ID", value = "t_rbac_role_ROLE_PARENT_ID", chineseNote = "父节点编号", tableAlias = "t_rbac_role")
    @ApiModelProperty(value = "父节点编号")
    private String roleParentId;

    /**
     *  编号上级路径,所属表字段为t_rbac_role.PATH_IDS
     */
    @MyBatisColumnAnnotation(name = "PATH_IDS", value = "t_rbac_role_PATH_IDS", chineseNote = "编号上级路径", tableAlias = "t_rbac_role")
    @ApiModelProperty(value = "编号上级路径")
    private String pathIds;

    /**
     *  编码上级路径,所属表字段为t_rbac_role.PATH_CODES
     */
    @MyBatisColumnAnnotation(name = "PATH_CODES", value = "t_rbac_role_PATH_CODES", chineseNote = "编码上级路径", tableAlias = "t_rbac_role")
    @ApiModelProperty(value = "编码上级路径")
    private String pathCodes;

    /**
     *  名称上级路径,所属表字段为t_rbac_role.PATH_NAMES
     */
    @MyBatisColumnAnnotation(name = "PATH_NAMES", value = "t_rbac_role_PATH_NAMES", chineseNote = "名称上级路径", tableAlias = "t_rbac_role")
    @ApiModelProperty(value = "名称上级路径")
    private String pathNames;

    /**
     *  层级,所属表字段为t_rbac_role.LEVEL
     */
    @MyBatisColumnAnnotation(name = "LEVEL", value = "t_rbac_role_LEVEL", chineseNote = "层级", tableAlias = "t_rbac_role")
    @ApiModelProperty(value = "层级")
    private String level;

    /**
     *  状态(Y启用;N停用),所属表字段为t_rbac_role.STATUS
     */
    @MyBatisColumnAnnotation(name = "STATUS", value = "t_rbac_role_STATUS", chineseNote = "状态(Y启用;N停用)", tableAlias = "t_rbac_role")
    @ApiModelProperty(value = "状态(Y启用;N停用)")
    private String status;

    /**
     *  备注,所属表字段为t_rbac_role.REMARK
     */
    @MyBatisColumnAnnotation(name = "REMARK", value = "t_rbac_role_REMARK", chineseNote = "备注", tableAlias = "t_rbac_role")
    @ApiModelProperty(value = "备注")
    private String remark;

    public String getDynamicTableName() {
        return "t_rbac_role";
    }

    private static final long serialVersionUID = 1L;

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleParentId() {
        return roleParentId;
    }

    public void setRoleParentId(String roleParentId) {
        this.roleParentId = roleParentId;
    }

    public String getPathIds() {
        return pathIds;
    }

    public void setPathIds(String pathIds) {
        this.pathIds = pathIds;
    }

    public String getPathCodes() {
        return pathCodes;
    }

    public void setPathCodes(String pathCodes) {
        this.pathCodes = pathCodes;
    }

    public String getPathNames() {
        return pathNames;
    }

    public void setPathNames(String pathNames) {
        this.pathNames = pathNames;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", roleId=").append(roleId);
        sb.append(", roleCode=").append(roleCode);
        sb.append(", roleName=").append(roleName);
        sb.append(", roleParentId=").append(roleParentId);
        sb.append(", pathIds=").append(pathIds);
        sb.append(", pathCodes=").append(pathCodes);
        sb.append(", pathNames=").append(pathNames);
        sb.append(", level=").append(level);
        sb.append(", status=").append(status);
        sb.append(", remark=").append(remark);
        sb.append("]");
        return sb.toString();
    }

}
