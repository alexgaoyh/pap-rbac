package com.pap.rbac.dto;

import com.pap.base.entity.PapBaseEntity;
import com.pap.base.mybatis.plugin.annotation.MyBatisColumnAnnotation;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * @Auther: alexgaoyh
 * @Date: 2018/12/25 14:18
 * @Description:
 */
@ApiModel(value = "DataProjectDTO", description = "数据权限项目")
public class DataProjectDTO extends PapBaseEntity implements Serializable {
    /**
     *  编号,所属表字段为t_rbac_data_project.DATA_PROJECT_ID
     */
    @ApiModelProperty(value = "编号")
    private String dataProjectId;

    /**
     *  编码,所属表字段为t_rbac_data_project.DATA_PROJECT_CODE
     */
    @ApiModelProperty(value = "编码")
    private String dataProjectCode;

    /**
     *  名称,所属表字段为t_rbac_data_project.DATA_PROJECT_NAME
     */
    @ApiModelProperty(value = "名称")
    private String dataProjectName;

    /**
     *  项目域名,所属表字段为t_rbac_data_project.DATA_PROJECT_DOMAIN
     */
    @ApiModelProperty(value = "项目域名")
    private String dataProjectDomain;

    /**
     *  管理人员,所属表字段为t_rbac_data_project.DATA_PROJECT_MANAGER
     */
    @ApiModelProperty(value = "管理人员")
    private String dataProjectManager;

    /**
     *  电话,所属表字段为t_rbac_data_project.DATA_PROJECT_MOBILE
     */
    @MyBatisColumnAnnotation(name = "DATA_PROJECT_MOBILE", value = "t_rbac_data_project_DATA_PROJECT_MOBILE", chineseNote = "电话", tableAlias = "t_rbac_data_project")
    @ApiModelProperty(value = "电话")
    private String dataProjectMobile;

    /**
     *  可用状态标识,所属表字段为t_rbac_data_project.DISABLE_FLAG
     */
    @ApiModelProperty(value = "可用状态标识")
    private String disableFlag;

    /**
     *  删除状态标识,所属表字段为t_rbac_data_project.DELETE_FLAG
     */
    @ApiModelProperty(value = "删除状态标识")
    private String deleteFlag;

    /**
     *  备注,所属表字段为t_rbac_data_project.REMARK
     */
    @ApiModelProperty(value = "备注")
    private String remark;

    public String getDynamicTableName() {
        return "t_rbac_data_project";
    }

    public String getDataProjectId() {
        return dataProjectId;
    }

    public void setDataProjectId(String dataProjectId) {
        this.dataProjectId = dataProjectId;
    }

    public String getDataProjectCode() {
        return dataProjectCode;
    }

    public void setDataProjectCode(String dataProjectCode) {
        this.dataProjectCode = dataProjectCode;
    }

    public String getDataProjectName() {
        return dataProjectName;
    }

    public void setDataProjectName(String dataProjectName) {
        this.dataProjectName = dataProjectName;
    }

    public String getDataProjectDomain() {
        return dataProjectDomain;
    }

    public void setDataProjectDomain(String dataProjectDomain) {
        this.dataProjectDomain = dataProjectDomain;
    }

    public String getDataProjectManager() {
        return dataProjectManager;
    }

    public void setDataProjectManager(String dataProjectManager) {
        this.dataProjectManager = dataProjectManager;
    }

    public String getDataProjectMobile() {
        return dataProjectMobile;
    }

    public void setDataProjectMobile(String dataProjectMobile) {
        this.dataProjectMobile = dataProjectMobile;
    }

    public String getDisableFlag() {
        return disableFlag;
    }

    public void setDisableFlag(String disableFlag) {
        this.disableFlag = disableFlag;
    }

    public String getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(String deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "DataProjectDTO{" +
                "dataProjectId='" + dataProjectId + '\'' +
                ", dataProjectCode='" + dataProjectCode + '\'' +
                ", dataProjectName='" + dataProjectName + '\'' +
                ", dataProjectDomain='" + dataProjectDomain + '\'' +
                ", dataProjectManager='" + dataProjectManager + '\'' +
                ", dataProjectMobile='" + dataProjectMobile + '\'' +
                ", disableFlag='" + disableFlag + '\'' +
                ", deleteFlag='" + deleteFlag + '\'' +
                ", remark='" + remark + '\'' +
                '}';
    }
}
