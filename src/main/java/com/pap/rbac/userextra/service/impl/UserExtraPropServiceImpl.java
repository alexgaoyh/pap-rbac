package com.pap.rbac.userextra.service.impl;

import com.pap.rbac.userextra.mapper.UserExtraPropMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pap.rbac.userextra.entity.UserExtraProp;
import com.pap.rbac.userextra.service.IUserExtraPropService;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Transactional
@Service("userExtraPropService")
public class UserExtraPropServiceImpl implements IUserExtraPropService {

    @Resource(name = "userExtraPropMapper")
    private UserExtraPropMapper mapper;

    @Override
    public int deleteByPrimaryKey(String id) {
       return mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int selectCountByMap(Map<Object, Object> map) {
      return mapper.selectCountByMap(map);
    }

    @Override
    public List<UserExtraProp> selectListByMap(Map<Object, Object> map) {
        return mapper.selectListByMap(map);
    }

    @Override
    public int insert(UserExtraProp record) {
       return mapper.insert(record);
    }

    @Override
    public int insertSelective(UserExtraProp record) {
       return mapper.insertSelective(record);
    }

    @Override
    public UserExtraProp selectByPrimaryKey(String id) {
       return mapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(UserExtraProp record) {
       return mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(UserExtraProp record) {
      return mapper.updateByPrimaryKey(record);
    }
}