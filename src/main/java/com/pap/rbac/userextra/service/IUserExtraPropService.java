package com.pap.rbac.userextra.service;

import com.pap.rbac.userextra.entity.UserExtraProp;

import java.util.List;
import java.util.Map;

public interface IUserExtraPropService {

    int deleteByPrimaryKey(String id);

    int selectCountByMap(Map<Object, Object> map);

    List<UserExtraProp> selectListByMap(Map<Object, Object> map);

    int insert(UserExtraProp record);

    int insertSelective(UserExtraProp record);

    UserExtraProp selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(UserExtraProp record);

    int updateByPrimaryKey(UserExtraProp record);
}
