package com.pap.rbac.userextra.controller;

import com.pap.base.util.bean.BeanCopyUtilss;
import com.pap.beans.idworker.IdWorker;
import com.pap.obj.vo.logineduser.LoginedUser;
import com.pap.obj.vo.logineduser.LoginedUserVO;
import com.pap.obj.vo.page.Page;
import com.pap.obj.vo.response.ResponseVO;
import com.pap.rbac.dto.UserExtraPropDTO;
import com.pap.rbac.userextra.entity.UserExtraProp;
import com.pap.rbac.userextra.service.IUserExtraPropService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/userExtraProp")
@Api(value = "用户属性管理", tags = "用户属性管理", description="用户属性管理")
public class UserExtraPropController {

	private static Logger logger  =  LoggerFactory.getLogger(UserExtraPropController.class);
	
	@Resource(name = "userExtraPropService")
	private IUserExtraPropService userExtraPropService;

	@Resource(name = "idWorker")
	private IdWorker idWorker;

	@ApiOperation("创建")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "userExtraPropDTO", value = "", required = true, dataType = "UserExtraPropDTO", paramType="body")
	})
	@PostMapping(value = "")
	public ResponseVO<UserExtraPropDTO> create(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
											   @RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
											   @RequestBody UserExtraPropDTO userExtraPropDTO,
											   @LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){
		String idStr = idWorker.nextIdStr();
		userExtraPropDTO.setUserExtraId(idStr);

		UserExtraProp databaseObj = new UserExtraProp();
		BeanCopyUtilss.myCopyProperties(userExtraPropDTO, databaseObj, loginedUserVO, true);
		int i = userExtraPropService.insertSelective(databaseObj);

		return ResponseVO.successdata(databaseObj);
	}

	@ApiOperation("更新")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "userExtraPropDTO", value = "", required = true, dataType = "UserExtraPropDTO", paramType="body")
	})
	@PutMapping(value = "")
	public ResponseVO<UserExtraPropDTO> update(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@RequestBody UserExtraPropDTO userExtraPropDTO,
											   @LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){

		UserExtraProp databaseObj = new UserExtraProp();
		BeanCopyUtilss.myCopyProperties(userExtraPropDTO, databaseObj, loginedUserVO, false);
		int i = userExtraPropService.updateByPrimaryKeySelective(databaseObj);

		databaseObj = userExtraPropService.selectByPrimaryKey(databaseObj.getUserExtraId());

		return ResponseVO.successdata(databaseObj);
	}


	@ApiOperation("删除")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "userExtraPropDTO", value = "", required = true, dataType = "UserExtraPropDTO", paramType="body")
	})
	@DeleteMapping(value = "")
	public ResponseVO<String> delete(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@RequestBody UserExtraPropDTO userExtraPropDTO){

		int i = userExtraPropService.deleteByPrimaryKey(userExtraPropDTO.getUserExtraId());

		return ResponseVO.successdata("操作成功");
	}

	@ApiOperation("详情")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "id", value = "编号", required = true, dataType = "String", paramType="path")
	})
	@GetMapping(value = "/{id}")
	public ResponseVO<UserExtraPropDTO> selectOne(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@PathVariable("id") String id){

		UserExtraProp databaseObj = userExtraPropService.selectByPrimaryKey(id);

		return ResponseVO.successdata(databaseObj);
	}

	@ApiOperation("查询")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "userExtraPropDTO", value = "应用查询参数", required = false, dataType = "UserExtraPropDTO", paramType="body"),
		@ApiImplicitParam(name = "pageNo", value = "页码", required = true, dataType = "int", paramType="query"),
		@ApiImplicitParam(name = "pageSize", value = "每页记录数", required = true, dataType = "int", paramType="query"),
		@ApiImplicitParam(name = "sortBy", value = "按照哪个属性排序", required = false, dataType = "String", paramType="query"),
		@ApiImplicitParam(name = "order", value = "排序顺序", required = false, dataType = "String", paramType="query")
	})
	@PostMapping(value = "/query")
	public ResponseVO<UserExtraPropDTO>query(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
												@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
												@RequestBody UserExtraPropDTO userExtraPropDTO,
												@RequestParam Integer pageNo, @RequestParam Integer pageSize,
												@RequestParam(defaultValue = "create_time") String sortBy, @RequestParam(defaultValue = "desc") String order,
											 @LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){
		Map<Object, Object> map = BeanCopyUtilss.beanToMap(userExtraPropDTO, loginedUserVO);
		if(pageSize != 0) {
			Page newPage = new Page(pageNo + "", pageSize + "");
			map.put("page", newPage);
		}
		List<UserExtraProp> list = userExtraPropService.selectListByMap(map);

		//
		List<UserExtraPropDTO> dtoList = toDTO(list);

		if(pageSize != 0) {
			Page newPage = new Page(pageNo + "", pageSize + "");
			int countNum = userExtraPropService.selectCountByMap(map);
			newPage.setCount(countNum);
			return ResponseVO.successdatas(dtoList, newPage);
		} else {
			return ResponseVO.successdatas(dtoList, null);
		}

	}

	private List<UserExtraPropDTO> toDTO(List<UserExtraProp> databaseList) {
		List<UserExtraPropDTO> returnList = new ArrayList<UserExtraPropDTO>();
		if(databaseList != null) {
			for(UserExtraProp dbEntity : databaseList) {
				UserExtraPropDTO dtoTemp = new UserExtraPropDTO();
				BeanCopyUtilss.myCopyProperties(dbEntity, dtoTemp);
				returnList.add(dtoTemp);
			}
		}
		return returnList;
	}

	private List<UserExtraProp> toDB(List<UserExtraPropDTO> dtoList) {
		List<UserExtraProp> returnList = new ArrayList<UserExtraProp>();
		if(dtoList != null) {
			for(UserExtraPropDTO dtoEntity : dtoList) {
				UserExtraProp dbTemp = new UserExtraProp();
				BeanCopyUtilss.myCopyProperties(dtoEntity, dbTemp);
				returnList.add(dbTemp);
			}
		}
		return returnList;
	}
}
