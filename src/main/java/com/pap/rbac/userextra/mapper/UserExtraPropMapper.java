package com.pap.rbac.userextra.mapper;

import com.pap.base.mapper.PapBaseMapper;
import com.pap.rbac.userextra.entity.UserExtraProp;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;;

@Mapper
public interface UserExtraPropMapper extends PapBaseMapper<UserExtraProp> {
    int deleteByPrimaryKey(String userExtraId);

    int selectCountByMap(Map<Object, Object> map);

    List<UserExtraProp> selectListByMap(Map<Object, Object> map);

    UserExtraProp selectByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int deleteByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int insert(UserExtraProp record);

    int insertSelective(UserExtraProp record);

    UserExtraProp selectByPrimaryKey(String userExtraId);

    int updateByPrimaryKeySelective(UserExtraProp record);

    int updateByPrimaryKey(UserExtraProp record);
}