package com.pap.rbac.userextra.entity;

import com.pap.base.entity.PapBaseEntity;
import com.pap.base.mybatis.plugin.annotation.*;
import java.io.Serializable;

@MyBatisTableAnnotation(name = "t_rbac_user_extra_prop", namespace = "com.pap.rbac.userextra.mapper.UserExtraPropMapper", remarks = " 修改点 ", aliasName = "t_rbac_user_extra_prop t_rbac_user_extra_prop" )
public class UserExtraProp extends PapBaseEntity implements Serializable {
    /**
     *  编号,所属表字段为t_rbac_user_extra_prop.USER_EXTRA_ID
     */
    @MyBatisColumnAnnotation(name = "USER_EXTRA_ID", value = "t_rbac_user_extra_prop_USER_EXTRA_ID", chineseNote = "编号", tableAlias = "t_rbac_user_extra_prop")
    @MyApiModelPropertyAnnotation(value = "编号")
    private String userExtraId;

    /**
     *  编码,所属表字段为t_rbac_user_extra_prop.USER_EXTRA_CODE
     */
    @MyBatisColumnAnnotation(name = "USER_EXTRA_CODE", value = "t_rbac_user_extra_prop_USER_EXTRA_CODE", chineseNote = "编码", tableAlias = "t_rbac_user_extra_prop")
    @MyApiModelPropertyAnnotation(value = "编码")
    private String userExtraCode;

    /**
     *  所属用户,所属表字段为t_rbac_user_extra_prop.USER_ID
     */
    @MyBatisColumnAnnotation(name = "USER_ID", value = "t_rbac_user_extra_prop_USER_ID", chineseNote = "所属用户", tableAlias = "t_rbac_user_extra_prop")
    @MyApiModelPropertyAnnotation(value = "所属用户")
    private String userId;

    /**
     *  属性键,所属表字段为t_rbac_user_extra_prop.PROP_KEY
     */
    @MyBatisColumnAnnotation(name = "PROP_KEY", value = "t_rbac_user_extra_prop_PROP_KEY", chineseNote = "属性键", tableAlias = "t_rbac_user_extra_prop")
    @MyApiModelPropertyAnnotation(value = "属性键")
    private String propKey;

    /**
     *  属性操作符,所属表字段为t_rbac_user_extra_prop.PROP_OPER
     */
    @MyBatisColumnAnnotation(name = "PROP_OPER", value = "t_rbac_user_extra_prop_PROP_OPER", chineseNote = "属性操作符", tableAlias = "t_rbac_user_extra_prop")
    @MyApiModelPropertyAnnotation(value = "属性操作符")
    private String propOper;

    /**
     *  属性值,所属表字段为t_rbac_user_extra_prop.PROP_VALUE
     */
    @MyBatisColumnAnnotation(name = "PROP_VALUE", value = "t_rbac_user_extra_prop_PROP_VALUE", chineseNote = "属性值", tableAlias = "t_rbac_user_extra_prop")
    @MyApiModelPropertyAnnotation(value = "属性值")
    private String propValue;

    /**
     *  备注,所属表字段为t_rbac_user_extra_prop.REMARK
     */
    @MyBatisColumnAnnotation(name = "REMARK", value = "t_rbac_user_extra_prop_REMARK", chineseNote = "备注", tableAlias = "t_rbac_user_extra_prop")
    @MyApiModelPropertyAnnotation(value = "备注")
    private String remark;

    /**
     *  用户类型,所属表字段为t_rbac_user_extra_prop.USER_TYPE
     */
    @MyBatisColumnAnnotation(name = "USER_TYPE", value = "t_rbac_user_extra_prop_USER_TYPE", chineseNote = "用户类型", tableAlias = "t_rbac_user_extra_prop")
    @MyApiModelPropertyAnnotation(value = "用户类型")
    private String userType;

    @Override
    public String getDynamicTableName() {
        return "t_rbac_user_extra_prop";
    }

    private static final long serialVersionUID = 1L;

    public String getUserExtraId() {
        return userExtraId;
    }

    public void setUserExtraId(String userExtraId) {
        this.userExtraId = userExtraId;
    }

    public String getUserExtraCode() {
        return userExtraCode;
    }

    public void setUserExtraCode(String userExtraCode) {
        this.userExtraCode = userExtraCode;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPropKey() {
        return propKey;
    }

    public void setPropKey(String propKey) {
        this.propKey = propKey;
    }

    public String getPropOper() {
        return propOper;
    }

    public void setPropOper(String propOper) {
        this.propOper = propOper;
    }

    public String getPropValue() {
        return propValue;
    }

    public void setPropValue(String propValue) {
        this.propValue = propValue;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", userExtraId=").append(userExtraId);
        sb.append(", userExtraCode=").append(userExtraCode);
        sb.append(", userId=").append(userId);
        sb.append(", propKey=").append(propKey);
        sb.append(", propOper=").append(propOper);
        sb.append(", propValue=").append(propValue);
        sb.append(", remark=").append(remark);
        sb.append(", userType=").append(userType);
        sb.append("]");
        return sb.toString();
    }
}