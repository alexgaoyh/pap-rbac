package com.pap.rbac.datafield.service.impl;

import com.pap.rbac.datafield.mapper.DataFieldMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pap.rbac.datafield.entity.DataField;
import com.pap.rbac.datafield.service.IDataFieldService;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Transactional
@Service("dataFieldService")
public class DataFieldServiceImpl implements IDataFieldService {

    @Resource(name = "dataFieldMapper")
    private DataFieldMapper mapper;

    @Override
    public int deleteByPrimaryKey(String id) {
       return mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int selectCountByMap(Map<Object, Object> map) {
      return mapper.selectCountByMap(map);
    }

    @Override
    public List<DataField> selectListByMap(Map<Object, Object> map) {
        return mapper.selectListByMap(map);
    }

    @Override
    public int insert(DataField record) {
       return mapper.insert(record);
    }

    @Override
    public int insertSelective(DataField record) {
       return mapper.insertSelective(record);
    }

    @Override
    public DataField selectByPrimaryKey(String id) {
       return mapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(DataField record) {
       return mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(DataField record) {
      return mapper.updateByPrimaryKey(record);
    }
}