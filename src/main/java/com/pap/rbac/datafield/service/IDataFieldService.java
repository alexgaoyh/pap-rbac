package com.pap.rbac.datafield.service;

import com.pap.rbac.datafield.entity.DataField;

import java.util.List;
import java.util.Map;

public interface IDataFieldService {

    int deleteByPrimaryKey(String id);

    int selectCountByMap(Map<Object, Object> map);

    List<DataField> selectListByMap(Map<Object, Object> map);

    int insert(DataField record);

    int insertSelective(DataField record);

    DataField selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(DataField record);

    int updateByPrimaryKey(DataField record);
}
