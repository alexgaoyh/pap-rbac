package com.pap.rbac.datafield.controller;

import com.pap.base.util.bean.BeanCopyUtilss;
import com.pap.beans.idworker.IdWorker;
import com.pap.obj.vo.logineduser.LoginedUser;
import com.pap.obj.vo.logineduser.LoginedUserVO;
import com.pap.obj.vo.page.Page;
import com.pap.obj.vo.response.ResponseVO;
import com.pap.rbac.datafield.entity.DataField;
import com.pap.rbac.datafield.service.IDataFieldService;
import com.pap.rbac.dto.DataFieldDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/dataField")
@Api(value = "数据权限归属字段", tags = "数据权限归属字段", description="数据权限归属字段")
public class DataFieldController {

	private static Logger logger  =  LoggerFactory.getLogger(DataFieldController.class);
	
	@Resource(name = "dataFieldService")
	private IDataFieldService dataFieldService;

	@Resource(name = "idWorker")
	private IdWorker idWorker;

	@ApiOperation("创建")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "dataFieldDTO", value = "", required = true, dataType = "DataFieldDTO", paramType="body")
	})
	@PostMapping(value = "")
	public ResponseVO<DataFieldDTO> create(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
										   @RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
										   @RequestBody DataFieldDTO dataFieldDTO,
										   @LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){
		String idStr = idWorker.nextIdStr();
		dataFieldDTO.setDataFieldId(idStr);

		DataField databaseObj = new DataField();
		BeanCopyUtilss.myCopyProperties(dataFieldDTO, databaseObj, loginedUserVO, true);
		int i = dataFieldService.insertSelective(databaseObj);

		return ResponseVO.successdata(databaseObj);
	}

	@ApiOperation("更新")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "dataFieldDTO", value = "", required = true, dataType = "DataFieldDTO", paramType="body")
	})
	@PutMapping(value = "")
	public ResponseVO<DataFieldDTO> update(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@RequestBody DataFieldDTO dataFieldDTO,
										   @LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){

		DataField databaseObj = new DataField();
		BeanCopyUtilss.myCopyProperties(dataFieldDTO, databaseObj, loginedUserVO, false);
		int i = dataFieldService.updateByPrimaryKeySelective(databaseObj);

		databaseObj = dataFieldService.selectByPrimaryKey(databaseObj.getDataFieldId());

		return ResponseVO.successdata(databaseObj);
	}


	@ApiOperation("删除")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "dataFieldDTO", value = "", required = true, dataType = "DataFieldDTO", paramType="body")
	})
	@DeleteMapping(value = "")
	public ResponseVO<String> delete(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@RequestBody DataFieldDTO dataFieldDTO){

		int i = dataFieldService.deleteByPrimaryKey(dataFieldDTO.getDataFieldId());

		return ResponseVO.successdata("操作成功");
	}

	@ApiOperation("详情")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "id", value = "编号", required = true, dataType = "String", paramType="path")
	})
	@GetMapping(value = "/{id}")
	public ResponseVO<DataFieldDTO> selectOne(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@PathVariable("id") String id){

		DataField databaseObj = dataFieldService.selectByPrimaryKey(id);

		return ResponseVO.successdata(databaseObj);
	}

	@ApiOperation("查询")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "dataFieldDTO", value = "应用查询参数", required = false, dataType = "DataFieldDTO", paramType="body"),
		@ApiImplicitParam(name = "pageNo", value = "页码", required = true, dataType = "int", paramType="query"),
		@ApiImplicitParam(name = "pageSize", value = "每页记录数", required = true, dataType = "int", paramType="query"),
		@ApiImplicitParam(name = "sortBy", value = "按照哪个属性排序", required = false, dataType = "String", paramType="query"),
		@ApiImplicitParam(name = "order", value = "排序顺序", required = false, dataType = "String", paramType="query")
	})
	@PostMapping(value = "/query")
	public ResponseVO<DataFieldDTO>query(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
												@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
												@RequestBody DataFieldDTO dataFieldDTO,
												@RequestParam Integer pageNo, @RequestParam Integer pageSize,
												@RequestParam(defaultValue = "create_time") String sortBy, @RequestParam(defaultValue = "desc") String order,
										 @LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){
		Map<Object, Object> map = BeanCopyUtilss.beanToMap(dataFieldDTO, loginedUserVO);
		if(pageSize != 0) {
			Page newPage = new Page(pageNo + "", pageSize + "");
			map.put("page", newPage);
		}
		List<DataField> list = dataFieldService.selectListByMap(map);

		//
		List<DataFieldDTO> dtoList = toDTO(list);

		if(pageSize != 0) {
			Page newPage = new Page(pageNo + "", pageSize + "");
			int countNum = dataFieldService.selectCountByMap(map);
			newPage.setCount(countNum);
			return ResponseVO.successdatas(dtoList, newPage);
		} else {
			return ResponseVO.successdatas(dtoList, null);
		}

	}

	private List<DataFieldDTO> toDTO(List<DataField> databaseList) {
		List<DataFieldDTO> returnList = new ArrayList<DataFieldDTO>();
		if(databaseList != null) {
			for(DataField dbEntity : databaseList) {
				DataFieldDTO dtoTemp = new DataFieldDTO();
				BeanCopyUtilss.myCopyProperties(dbEntity, dtoTemp);
				returnList.add(dtoTemp);
			}
		}
		return returnList;
	}

	private List<DataField> toDB(List<DataFieldDTO> dtoList) {
		List<DataField> returnList = new ArrayList<DataField>();
		if(dtoList != null) {
			for(DataFieldDTO dtoEntity : dtoList) {
				DataField dbTemp = new DataField();
				BeanCopyUtilss.myCopyProperties(dtoEntity, dbTemp);
				returnList.add(dbTemp);
			}
		}
		return returnList;
	}
}
