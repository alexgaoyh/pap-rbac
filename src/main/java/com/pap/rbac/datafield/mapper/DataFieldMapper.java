package com.pap.rbac.datafield.mapper;

import com.pap.base.mapper.PapBaseMapper;
import com.pap.rbac.datafield.entity.DataField;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;;

@Mapper
public interface DataFieldMapper extends PapBaseMapper<DataField> {
    int deleteByPrimaryKey(String dataFieldId);

    int selectCountByMap(Map<Object, Object> map);

    List<DataField> selectListByMap(Map<Object, Object> map);

    DataField selectByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int deleteByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int insert(DataField record);

    int insertSelective(DataField record);

    DataField selectByPrimaryKey(String dataFieldId);

    int updateByPrimaryKeySelective(DataField record);

    int updateByPrimaryKey(DataField record);
}