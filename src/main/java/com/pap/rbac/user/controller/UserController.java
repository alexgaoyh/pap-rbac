package com.pap.rbac.user.controller;

import com.pap.base.util.bean.BeanCopyUtilss;
import com.pap.base.util.string.StringUtilss;
import com.pap.beans.idworker.IdWorker;
import com.pap.obj.vo.logineduser.LoginedUser;
import com.pap.obj.vo.logineduser.LoginedUserVO;
import com.pap.obj.vo.page.Page;
import com.pap.obj.vo.response.ResponseVO;
import com.pap.rbac.dto.UserDTO;
import com.pap.rbac.dto.login.UserInfoDTO;
import com.pap.rbac.dto.login.UserLoginDTO;
import com.pap.rbac.user.entity.User;
import com.pap.rbac.user.service.IUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/user")
@Api(value = "用户管理", tags = "用户管理", description="用户管理")
public class UserController {

	private static Logger logger  =  LoggerFactory.getLogger(UserController.class);
	
	@Resource(name = "userService")
	private IUserService userService;

	@Resource(name = "idWorker")
	private IdWorker idWorker;

	@ApiOperation("创建")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "userDTO", value = "", required = true, dataType = "UserDTO", paramType="body")
	})
	@PostMapping(value = "")
	public ResponseVO<UserDTO> create(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
									  @RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
									  @RequestBody UserDTO userDTO,
									  @LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){
		String idStr = idWorker.nextIdStr();
		userDTO.setUserId(idStr);

		User databaseObj = new User();
		BeanCopyUtilss.myCopyProperties(userDTO, databaseObj, loginedUserVO);

		int i = userService.insertSelective(databaseObj);

		return ResponseVO.successdata(userDTO);
	}

	@ApiOperation("更新")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "userDTO", value = "", required = true, dataType = "UserDTO", paramType="body")
	})
	@PutMapping(value = "")
	public ResponseVO<UserDTO> update(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@RequestBody UserDTO userDTO,
													@LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){

		User databaseObj = new User();
		BeanCopyUtilss.myCopyProperties(userDTO, databaseObj, loginedUserVO, false);
		int i = userService.updateByPrimaryKeySelective(databaseObj);

		databaseObj = userService.selectByPrimaryKey(databaseObj.getUserId());

		return ResponseVO.successdata(databaseObj);
	}


	@ApiOperation("删除")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "userDTO", value = "", required = true, dataType = "UserDTO", paramType="body")
	})
	@DeleteMapping(value = "")
	public ResponseVO<String> delete(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@RequestBody UserDTO userDTO,
													@LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){

		int i = userService.deleteByPrimaryKey(userDTO.getUserId());

		return ResponseVO.successdata("操作成功");
	}

	@ApiOperation("详情")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "id", value = "编号", required = true, dataType = "String", paramType="path")
	})
	@GetMapping(value = "/{id}")
	public ResponseVO<UserDTO> selectOne(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@PathVariable("id") String id){

		User databaseObj = userService.selectByPrimaryKey(id);

		return ResponseVO.successdata(databaseObj);
	}

	@ApiOperation("可用状态批量更新操作")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "id", value = "编号", required = true, dataType = "String", paramType="path")
	})
	@GetMapping(value = "disableFlag/{disableFlag}/{userIds}")
	public ResponseVO<UserDTO> modifyDisableFlag(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
										 @RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
										 @PathVariable("disableFlag") String disableFlag,
										 @PathVariable("userIds") String userIds){
		if (StringUtilss.isNotEmpty(userIds)) {
			String[] userIdArray = userIds.split(",");
			for (int idx = 0; idx < userIdArray.length; idx++) {
				User user = new User();
				user.setUserId(userIdArray[idx]);
				user.setDisableFlag(disableFlag);
				userService.updateByPrimaryKeySelective(user);
			}
		}
		return ResponseVO.successdata("操作成功!");
	}

	@ApiOperation("查询")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "userDTO", value = "应用查询参数", required = false, dataType = "UserDTO", paramType="body"),
		@ApiImplicitParam(name = "pageNo", value = "页码", required = true, dataType = "int", paramType="query"),
		@ApiImplicitParam(name = "pageSize", value = "每页记录数", required = true, dataType = "int", paramType="query"),
		@ApiImplicitParam(name = "sortBy", value = "按照哪个属性排序", required = false, dataType = "String", paramType="query"),
		@ApiImplicitParam(name = "order", value = "排序顺序", required = false, dataType = "String", paramType="query")
	})
	@PostMapping(value = "/query")
	public ResponseVO<UserDTO>query(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
												@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
												@RequestBody UserDTO userDTO,
												@RequestParam Integer pageNo, @RequestParam Integer pageSize,
												@RequestParam(defaultValue = "create_time") String sortBy, @RequestParam(defaultValue = "desc") String order,
												@LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){
		Map<Object, Object> map = BeanCopyUtilss.beanToMap(userDTO, loginedUserVO);
		if(pageSize != 0) {
			Page newPage = new Page(pageNo + "", pageSize + "");
			map.put("page", newPage);
		}
		List<User> list = userService.selectListByMap(map);

		//
		List<UserDTO> dtoList = toDTO(list);

		if(pageSize != 0) {
			Page newPage = new Page(pageNo + "", pageSize + "");
			int countNum = userService.selectCountByMap(map);
			newPage.setCount(countNum);
			return ResponseVO.successdatas(dtoList, newPage);
		} else {
			return ResponseVO.successdatas(dtoList, null);
		}

	}

	@ApiOperation("用户名密码登录(返回含权限集合)")
	@ApiImplicitParams({
						@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "userLoginDTO", value = "", required = true, dataType = "UserLoginDTO", paramType="body")
	})
	@RequestMapping(value = "/loginbyloginname", method = {RequestMethod.POST} )
	public ResponseVO<UserInfoDTO> loginByLoginName(@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
												@RequestBody UserLoginDTO userLoginDTO) {

		if(userLoginDTO != null && StringUtilss.isNotEmpty(userLoginDTO.getUserLoginName())
				&& StringUtilss.isNotEmpty(userLoginDTO.getUserPasswords())) {
			Map<Object, Object> loginMap = new HashMap<Object, Object>();
			loginMap.put("userLoginName", userLoginDTO.getUserLoginName());
			loginMap.put("userPasswords", userLoginDTO.getUserPasswords());
			List<User> loginList = userService.selectListByMap(loginMap);
			if(loginList != null && loginList.size() > 0) {
				User user = loginList.get(0);

				UserInfoDTO userInfoDTO = formatLoginUserInfoByUser(user);
				return ResponseVO.successdata(userInfoDTO);
			} else {
				return ResponseVO.validfail("用户名密码错误，请检验数据!");
			}
		} else {
			return ResponseVO.validfail("用户名密码为空，请检查数据!");
		}
	}

	/**
	 * 根据数据库user对象，重新封装含权限信息的用户对象
	 * @param user
	 * @return
	 */
	private UserInfoDTO formatLoginUserInfoByUser(User user) {
		UserInfoDTO userInfoDTO = new UserInfoDTO();
		userInfoDTO.setUserId(user.getUserId());
		userInfoDTO.setUserName(user.getUserName());
		userInfoDTO.setUserMobile(user.getMobile());
		userInfoDTO.setTenantId(user.getClientLicenseId());
		userInfoDTO.setPermissionCodeList(userService.selectPermissionCodesByUserId(user.getUserId()));
		return userInfoDTO;
	}

	private List<UserDTO> toDTO(List<User> databaseList) {
		List<UserDTO> returnList = new ArrayList<UserDTO>();
		if(databaseList != null) {
			for(User dbEntity : databaseList) {
				UserDTO dtoTemp = new UserDTO();
				BeanCopyUtilss.myCopyProperties(dbEntity, dtoTemp);
				returnList.add(dtoTemp);
			}
		}
		return returnList;
	}

	private List<User> toDB(List<UserDTO> dtoList) {
		List<User> returnList = new ArrayList<User>();
		if(dtoList != null) {
			for(UserDTO dtoEntity : dtoList) {
				User dbTemp = new User();
				BeanCopyUtilss.myCopyProperties(dtoEntity, dbTemp);
				returnList.add(dbTemp);
			}
		}
		return returnList;
	}
}
