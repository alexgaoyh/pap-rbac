package com.pap.rbac.user.service;

import com.pap.rbac.dto.UserDTO;
import com.pap.rbac.user.entity.User;

import java.util.List;
import java.util.Map;

public interface IUserService {

    int deleteByPrimaryKey(String id);

    int selectCountByMap(Map<Object, Object> map);

    List<User> selectListByMap(Map<Object, Object> map);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    // alexgaoyh

    UserDTO selectDTOByPrimaryKey(String id);

    /**
     * 根据用户编号查询权限编码集合
     * @param userId    用户编号
     * @return
     */
    List<String> selectPermissionCodesByUserId(String userId);

}
