package com.pap.rbac.user.service.impl;

import com.pap.base.util.bean.BeanCopyUtilss;
import com.pap.base.util.string.StringUtilss;
import com.pap.rbac.dto.UserDTO;
import com.pap.rbac.permission.entity.Permission;
import com.pap.rbac.permission.mapper.PermissionGroupPermissionRelMapper;
import com.pap.rbac.permission.mapper.PermissionMapper;
import com.pap.rbac.role.mapper.RolePermissionRelMapper;
import com.pap.rbac.user.entity.User;
import com.pap.rbac.user.mapper.UserMapper;
import com.pap.rbac.user.service.IUserService;
import com.pap.rbac.userrolerel.mapper.UserRoleRelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;

@Transactional
@Service("userService")
public class UserServiceImpl implements IUserService {

    @Resource(name = "userMapper")
    private UserMapper mapper;

    @Resource(name = "userRoleRelMapper")
    private UserRoleRelMapper userRoleRelMapper;

    @Resource(name = "rolePermissionRelMapper")
    private RolePermissionRelMapper rolePermissionRelMapper;

    @Resource(name = "permissionGroupPermissionRelMapper")
    private PermissionGroupPermissionRelMapper permissionGroupPermissionRelMapper;

    @Resource(name = "permissionMapper")
    private PermissionMapper permissionMapper;

    @Override
    public int deleteByPrimaryKey(String id) {
       return mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int selectCountByMap(Map<Object, Object> map) {
      return mapper.selectCountByMap(map);
    }

    @Override
    public List<User> selectListByMap(Map<Object, Object> map) {
        return mapper.selectListByMap(map);
    }

    @Override
    public int insert(User record) {
       return mapper.insert(record);
    }

    @Override
    public int insertSelective(User record) {
       return mapper.insertSelective(record);
    }

    @Override
    public User selectByPrimaryKey(String id) {
       return mapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(User record) {
       return mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(User record) {
      return mapper.updateByPrimaryKey(record);
    }


    @Override
    public UserDTO selectDTOByPrimaryKey(String id) {
        User userTemp = mapper.selectByPrimaryKey(id);
        UserDTO returnDTO = new UserDTO();
        BeanCopyUtilss.myCopyProperties(userTemp, returnDTO);

        return returnDTO;
    }

    @Override
    public List<String> selectPermissionCodesByUserId(String userId) {
        Set<String> permissionCodesList = new HashSet<String>();

        List<String> roleIdsList = userRoleRelMapper.selectRoleIdsByUserId(userId);
        if(roleIdsList != null && roleIdsList.size() > 0) {
            for(String roleId : roleIdsList) {
                List<String> permissionGroupIdsListTemp = rolePermissionRelMapper.selectPermissionGroupIdsByRoleId(roleId);
                if(permissionGroupIdsListTemp != null && permissionGroupIdsListTemp.size() > 0) {
                    for(String permissionGroupId : permissionGroupIdsListTemp) {
                        List<String> permissionIdsList = permissionGroupPermissionRelMapper.selectPermissionIdsByPermissionGroupId(permissionGroupId);
                        if(permissionIdsList != null && permissionIdsList.size() > 0) {
                            for(String permissionId : permissionIdsList) {
                                Permission permission = permissionMapper.selectByPrimaryKey(permissionId);
                                if(permission != null && StringUtilss.isNotEmpty(permission.getPermissionId())) {
                                    permissionCodesList.add(permission.getPermissionCode());
                                }
                            }
                        }
                    }
                }
            }
        }

        List<String> returnPermissionCodesList = new ArrayList<String>();
        returnPermissionCodesList.addAll(permissionCodesList);
        return returnPermissionCodesList;
    }
}