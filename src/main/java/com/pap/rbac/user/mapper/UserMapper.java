package com.pap.rbac.user.mapper;

import com.pap.base.mapper.PapBaseMapper;
import com.pap.rbac.user.entity.User;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;;

@Mapper
public interface UserMapper extends PapBaseMapper<User> {
    int deleteByPrimaryKey(String userId);

    int selectCountByMap(Map<Object, Object> map);

    List<User> selectListByMap(Map<Object, Object> map);

    User selectByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int deleteByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(String userId);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    // alexgaoyh add  activiti delete method
    /**
     * activiti 工作流 删除用户和组的关系
     * @return
     */
    int deleteActivitiAllUser();

    /**
     * activiti 工作流 删除用户和组的关系
     * @return
     */
    int deleteActivitiAllRole();

    /**
     * activiti 工作流 删除用户和组的关系
     * @return
     */
    int deleteActivitiAllMemerShip();
}