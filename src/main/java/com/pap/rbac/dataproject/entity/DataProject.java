package com.pap.rbac.dataproject.entity;

import com.pap.base.entity.PapBaseEntity;
import com.pap.base.mybatis.plugin.annotation.*;
import java.io.Serializable;

@MyBatisTableAnnotation(name = "t_rbac_data_project", namespace = "com.pap.rbac.dataproject.mapper.DataProjectMapper", remarks = "数据权限-所属项目", aliasName = "t_rbac_data_project t_rbac_data_project" )
public class DataProject extends PapBaseEntity implements Serializable {
    /**
     *  编号,所属表字段为t_rbac_data_project.DATA_PROJECT_ID
     */
    @MyBatisColumnAnnotation(name = "DATA_PROJECT_ID", value = "t_rbac_data_project_DATA_PROJECT_ID", chineseNote = "编号", tableAlias = "t_rbac_data_project")
    @MyApiModelPropertyAnnotation(value = "编号")
    private String dataProjectId;

    /**
     *  编码,所属表字段为t_rbac_data_project.DATA_PROJECT_CODE
     */
    @MyBatisColumnAnnotation(name = "DATA_PROJECT_CODE", value = "t_rbac_data_project_DATA_PROJECT_CODE", chineseNote = "编码", tableAlias = "t_rbac_data_project")
    @MyApiModelPropertyAnnotation(value = "编码")
    private String dataProjectCode;

    /**
     *  名称,所属表字段为t_rbac_data_project.DATA_PROJECT_NAME
     */
    @MyBatisColumnAnnotation(name = "DATA_PROJECT_NAME", value = "t_rbac_data_project_DATA_PROJECT_NAME", chineseNote = "名称", tableAlias = "t_rbac_data_project")
    @MyApiModelPropertyAnnotation(value = "名称")
    private String dataProjectName;

    /**
     *  项目域名,所属表字段为t_rbac_data_project.DATA_PROJECT_DOMAIN
     */
    @MyBatisColumnAnnotation(name = "DATA_PROJECT_DOMAIN", value = "t_rbac_data_project_DATA_PROJECT_DOMAIN", chineseNote = "项目域名", tableAlias = "t_rbac_data_project")
    @MyApiModelPropertyAnnotation(value = "项目域名")
    private String dataProjectDomain;

    /**
     *  管理人员,所属表字段为t_rbac_data_project.DATA_PROJECT_MANAGER
     */
    @MyBatisColumnAnnotation(name = "DATA_PROJECT_MANAGER", value = "t_rbac_data_project_DATA_PROJECT_MANAGER", chineseNote = "管理人员", tableAlias = "t_rbac_data_project")
    @MyApiModelPropertyAnnotation(value = "管理人员")
    private String dataProjectManager;

    /**
     *  电话,所属表字段为t_rbac_data_project.DATA_PROJECT_MOBILE
     */
    @MyBatisColumnAnnotation(name = "DATA_PROJECT_MOBILE", value = "t_rbac_data_project_DATA_PROJECT_MOBILE", chineseNote = "电话", tableAlias = "t_rbac_data_project")
    @MyApiModelPropertyAnnotation(value = "电话")
    private String dataProjectMobile;

    /**
     *  可用状态标识,所属表字段为t_rbac_data_project.DISABLE_FLAG
     */
    @MyBatisColumnAnnotation(name = "DISABLE_FLAG", value = "t_rbac_data_project_DISABLE_FLAG", chineseNote = "可用状态标识", tableAlias = "t_rbac_data_project")
    @MyApiModelPropertyAnnotation(value = "可用状态标识")
    private String disableFlag;

    /**
     *  删除状态标识,所属表字段为t_rbac_data_project.DELETE_FLAG
     */
    @MyBatisColumnAnnotation(name = "DELETE_FLAG", value = "t_rbac_data_project_DELETE_FLAG", chineseNote = "删除状态标识", tableAlias = "t_rbac_data_project")
    @MyApiModelPropertyAnnotation(value = "删除状态标识")
    private String deleteFlag;

    /**
     *  备注,所属表字段为t_rbac_data_project.REMARK
     */
    @MyBatisColumnAnnotation(name = "REMARK", value = "t_rbac_data_project_REMARK", chineseNote = "备注", tableAlias = "t_rbac_data_project")
    @MyApiModelPropertyAnnotation(value = "备注")
    private String remark;

    @Override
    public String getDynamicTableName() {
        return "t_rbac_data_project";
    }

    private static final long serialVersionUID = 1L;

    public String getDataProjectId() {
        return dataProjectId;
    }

    public void setDataProjectId(String dataProjectId) {
        this.dataProjectId = dataProjectId;
    }

    public String getDataProjectCode() {
        return dataProjectCode;
    }

    public void setDataProjectCode(String dataProjectCode) {
        this.dataProjectCode = dataProjectCode;
    }

    public String getDataProjectName() {
        return dataProjectName;
    }

    public void setDataProjectName(String dataProjectName) {
        this.dataProjectName = dataProjectName;
    }

    public String getDataProjectDomain() {
        return dataProjectDomain;
    }

    public void setDataProjectDomain(String dataProjectDomain) {
        this.dataProjectDomain = dataProjectDomain;
    }

    public String getDataProjectManager() {
        return dataProjectManager;
    }

    public void setDataProjectManager(String dataProjectManager) {
        this.dataProjectManager = dataProjectManager;
    }

    public String getDataProjectMobile() {
        return dataProjectMobile;
    }

    public void setDataProjectMobile(String dataProjectMobile) {
        this.dataProjectMobile = dataProjectMobile;
    }

    public String getDisableFlag() {
        return disableFlag;
    }

    public void setDisableFlag(String disableFlag) {
        this.disableFlag = disableFlag;
    }

    public String getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(String deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", dataProjectId=").append(dataProjectId);
        sb.append(", dataProjectCode=").append(dataProjectCode);
        sb.append(", dataProjectName=").append(dataProjectName);
        sb.append(", dataProjectDomain=").append(dataProjectDomain);
        sb.append(", dataProjectManager=").append(dataProjectManager);
        sb.append(", dataProjectMobile=").append(dataProjectMobile);
        sb.append(", disableFlag=").append(disableFlag);
        sb.append(", deleteFlag=").append(deleteFlag);
        sb.append(", remark=").append(remark);
        sb.append("]");
        return sb.toString();
    }
}