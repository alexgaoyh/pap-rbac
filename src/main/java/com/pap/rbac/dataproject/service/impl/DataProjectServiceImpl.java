package com.pap.rbac.dataproject.service.impl;

import com.pap.rbac.dataproject.mapper.DataProjectMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pap.rbac.dataproject.entity.DataProject;
import com.pap.rbac.dataproject.service.IDataProjectService;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Transactional
@Service("dataProjectService")
public class DataProjectServiceImpl implements IDataProjectService {

    @Resource(name = "dataProjectMapper")
    private DataProjectMapper mapper;

    @Override
    public int deleteByPrimaryKey(String id) {
       return mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int selectCountByMap(Map<Object, Object> map) {
      return mapper.selectCountByMap(map);
    }

    @Override
    public List<DataProject> selectListByMap(Map<Object, Object> map) {
        return mapper.selectListByMap(map);
    }

    @Override
    public int insert(DataProject record) {
       return mapper.insert(record);
    }

    @Override
    public int insertSelective(DataProject record) {
       return mapper.insertSelective(record);
    }

    @Override
    public DataProject selectByPrimaryKey(String id) {
       return mapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(DataProject record) {
       return mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(DataProject record) {
      return mapper.updateByPrimaryKey(record);
    }
}