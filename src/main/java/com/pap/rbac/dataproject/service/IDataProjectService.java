package com.pap.rbac.dataproject.service;

import com.pap.rbac.dataproject.entity.DataProject;

import java.util.List;
import java.util.Map;

public interface IDataProjectService {

    int deleteByPrimaryKey(String id);

    int selectCountByMap(Map<Object, Object> map);

    List<DataProject> selectListByMap(Map<Object, Object> map);

    int insert(DataProject record);

    int insertSelective(DataProject record);

    DataProject selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(DataProject record);

    int updateByPrimaryKey(DataProject record);
}
