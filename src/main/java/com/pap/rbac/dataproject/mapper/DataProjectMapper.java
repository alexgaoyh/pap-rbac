package com.pap.rbac.dataproject.mapper;

import com.pap.base.mapper.PapBaseMapper;
import com.pap.rbac.dataproject.entity.DataProject;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;;

@Mapper
public interface DataProjectMapper extends PapBaseMapper<DataProject> {
    int deleteByPrimaryKey(String dataProjectId);

    int selectCountByMap(Map<Object, Object> map);

    List<DataProject> selectListByMap(Map<Object, Object> map);

    DataProject selectByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int deleteByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int insert(DataProject record);

    int insertSelective(DataProject record);

    DataProject selectByPrimaryKey(String dataProjectId);

    int updateByPrimaryKeySelective(DataProject record);

    int updateByPrimaryKey(DataProject record);
}