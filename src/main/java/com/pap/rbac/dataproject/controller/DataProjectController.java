package com.pap.rbac.dataproject.controller;

import com.pap.base.util.bean.BeanCopyUtilss;
import com.pap.beans.idworker.IdWorker;
import com.pap.obj.vo.logineduser.LoginedUser;
import com.pap.obj.vo.logineduser.LoginedUserVO;
import com.pap.obj.vo.page.Page;
import com.pap.obj.vo.response.ResponseVO;
import com.pap.rbac.dataproject.entity.DataProject;
import com.pap.rbac.dataproject.service.IDataProjectService;
import com.pap.rbac.dto.DataProjectDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/dataProject")
@Api(value = "数据权限项目组", tags = "数据权限项目组", description="数据权限项目组")
public class DataProjectController {

	private static Logger logger  =  LoggerFactory.getLogger(DataProjectController.class);
	
	@Resource(name = "dataProjectService")
	private IDataProjectService dataProjectService;

	@Resource(name = "idWorker")
	private IdWorker idWorker;

	@ApiOperation("创建")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "dataProjectDTO", value = "", required = true, dataType = "DataProjectDTO", paramType="body")
	})
	@PostMapping(value = "")
	public ResponseVO<DataProjectDTO> create(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
											 @RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
											 @RequestBody DataProjectDTO dataProjectDTO,
											 @LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){
		String idStr = idWorker.nextIdStr();
		dataProjectDTO.setDataProjectId(idStr);

		DataProject databaseObj = new DataProject();
		BeanCopyUtilss.myCopyProperties(dataProjectDTO, databaseObj, loginedUserVO, true);
		int i = dataProjectService.insertSelective(databaseObj);

		return ResponseVO.successdata(databaseObj);
	}

	@ApiOperation("更新")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "dataProjectDTO", value = "", required = true, dataType = "DataProjectDTO", paramType="body")
	})
	@PutMapping(value = "")
	public ResponseVO<DataProjectDTO> update(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@RequestBody DataProjectDTO dataProjectDTO,
											 @LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){

		DataProject databaseObj = new DataProject();
		BeanCopyUtilss.myCopyProperties(dataProjectDTO, databaseObj, loginedUserVO, false);
		int i = dataProjectService.updateByPrimaryKeySelective(databaseObj);

		databaseObj = dataProjectService.selectByPrimaryKey(databaseObj.getDataProjectId());

		return ResponseVO.successdata(databaseObj);
	}


	@ApiOperation("删除")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "dataProjectDTO", value = "", required = true, dataType = "DataProjectDTO", paramType="body")
	})
	@DeleteMapping(value = "")
	public ResponseVO<String> delete(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@RequestBody DataProjectDTO dataProjectDTO){

		int i = dataProjectService.deleteByPrimaryKey(dataProjectDTO.getDataProjectId());

		return ResponseVO.successdata("操作成功");
	}

	@ApiOperation("详情")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "id", value = "编号", required = true, dataType = "String", paramType="path")
	})
	@GetMapping(value = "/{id}")
	public ResponseVO<DataProjectDTO> selectOne(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@PathVariable("id") String id){

		DataProject databaseObj = dataProjectService.selectByPrimaryKey(id);

		return ResponseVO.successdata(databaseObj);
	}

	@ApiOperation("分页条件检索应用")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "dataProjectDTO", value = "应用查询参数", required = false, dataType = "DataProjectDTO", paramType="body"),
		@ApiImplicitParam(name = "pageNo", value = "页码", required = false, dataType = "int", paramType="query"),
		@ApiImplicitParam(name = "pageSize", value = "每页记录数", required = false, dataType = "int", paramType="query"),
		@ApiImplicitParam(name = "sortBy", value = "按照哪个属性排序", required = false, dataType = "String", paramType="query"),
		@ApiImplicitParam(name = "order", value = "排序顺序", required = false, dataType = "String", paramType="query")
	})
	@PostMapping(value = "/query")
	public ResponseVO<DataProjectDTO>query(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
												@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
												@RequestBody DataProjectDTO dataProjectDTO,
												@RequestParam Integer pageNo, @RequestParam Integer pageSize,
												@RequestParam(defaultValue = "create_time") String sortBy, @RequestParam(defaultValue = "desc") String order,
										   @LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){
		Map<Object, Object> map = BeanCopyUtilss.beanToMap(dataProjectDTO, loginedUserVO);
		if(pageSize != 0) {
			Page newPage = new Page(pageNo + "", pageSize + "");
			map.put("page", newPage);
		}
		List<DataProject> list = dataProjectService.selectListByMap(map);

		//
		List<DataProjectDTO> dtoList = toDTO(list);

		if(pageSize != 0) {
			Page newPage = new Page(pageNo + "", pageSize + "");
			int countNum = dataProjectService.selectCountByMap(map);
			newPage.setCount(countNum);
			return ResponseVO.successdatas(dtoList, newPage);
		} else {
			return ResponseVO.successdatas(dtoList, null);
		}

	}

	private List<DataProjectDTO> toDTO(List<DataProject> databaseList) {
		List<DataProjectDTO> returnList = new ArrayList<DataProjectDTO>();
		if(databaseList != null) {
			for(DataProject dbEntity : databaseList) {
				DataProjectDTO dtoTemp = new DataProjectDTO();
				BeanCopyUtilss.myCopyProperties(dbEntity, dtoTemp);
				returnList.add(dtoTemp);
			}
		}
		return returnList;
	}

	private List<DataProject> toDB(List<DataProjectDTO> dtoList) {
		List<DataProject> returnList = new ArrayList<DataProject>();
		if(dtoList != null) {
			for(DataProjectDTO dtoEntity : dtoList) {
				DataProject dbTemp = new DataProject();
				BeanCopyUtilss.myCopyProperties(dtoEntity, dbTemp);
				returnList.add(dbTemp);
			}
		}
		return returnList;
	}
}
