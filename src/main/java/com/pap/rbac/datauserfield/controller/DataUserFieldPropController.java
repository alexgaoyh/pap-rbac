package com.pap.rbac.datauserfield.controller;

import com.pap.base.util.bean.BeanCopyUtilss;
import com.pap.beans.idworker.IdWorker;
import com.pap.obj.vo.logineduser.LoginedUser;
import com.pap.obj.vo.logineduser.LoginedUserVO;
import com.pap.obj.vo.page.Page;
import com.pap.obj.vo.response.ResponseVO;
import com.pap.rbac.datauserfield.entity.DataUserFieldProp;
import com.pap.rbac.datauserfield.service.IDataUserFieldPropService;
import com.pap.rbac.dto.DataUserFieldPropDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/dataUserFieldProp")
@Api(value = "数据权限用户属性", tags = "数据权限用户属性", description="数据权限用户属性")
public class DataUserFieldPropController {

	private static Logger logger  =  LoggerFactory.getLogger(DataUserFieldPropController.class);
	
	@Resource(name = "dataUserFieldPropService")
	private IDataUserFieldPropService dataUserFieldPropService;

	@Resource(name = "idWorker")
	private IdWorker idWorker;

	@ApiOperation("创建")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "dataUserFieldPropDTO", value = "", required = true, dataType = "DataUserFieldPropDTO", paramType="body")
	})
	@PostMapping(value = "")
	public ResponseVO<DataUserFieldPropDTO> create(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
												   @RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
												   @RequestBody DataUserFieldPropDTO dataUserFieldPropDTO,
												   @LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){
		String idStr = idWorker.nextIdStr();
		dataUserFieldPropDTO.setDataUserFieldId(idStr);

		DataUserFieldProp databaseObj = new DataUserFieldProp();
		BeanCopyUtilss.myCopyProperties(dataUserFieldPropDTO, databaseObj, loginedUserVO, true);
		int i = dataUserFieldPropService.insertSelective(databaseObj);

		return ResponseVO.successdata(databaseObj);
	}

	@ApiOperation("更新")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "dataUserFieldPropDTO", value = "", required = true, dataType = "DataUserFieldPropDTO", paramType="body")
	})
	@PutMapping(value = "")
	public ResponseVO<DataUserFieldPropDTO> update(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@RequestBody DataUserFieldPropDTO dataUserFieldPropDTO,
												   @LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){

		DataUserFieldProp databaseObj = new DataUserFieldProp();
		BeanCopyUtilss.myCopyProperties(dataUserFieldPropDTO, databaseObj, loginedUserVO, false);
		int i = dataUserFieldPropService.updateByPrimaryKeySelective(databaseObj);

		databaseObj = dataUserFieldPropService.selectByPrimaryKey(databaseObj.getDataUserFieldId());

		return ResponseVO.successdata(databaseObj);
	}


	@ApiOperation("删除")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "dataUserFieldPropDTO", value = "", required = true, dataType = "DataUserFieldPropDTO", paramType="body")
	})
	@DeleteMapping(value = "")
	public ResponseVO<String> delete(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@RequestBody DataUserFieldPropDTO dataUserFieldPropDTO){

		int i = dataUserFieldPropService.deleteByPrimaryKey(dataUserFieldPropDTO.getDataUserFieldId());

		return ResponseVO.successdata("操作成功");
	}

	@ApiOperation("详情")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "id", value = "编号", required = true, dataType = "String", paramType="path")
	})
	@GetMapping(value = "/{id}")
	public ResponseVO<DataUserFieldPropDTO> selectOne(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@PathVariable("id") String id){

		DataUserFieldProp databaseObj = dataUserFieldPropService.selectByPrimaryKey(id);

		return ResponseVO.successdata(databaseObj);
	}

	@ApiOperation("查询")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "dataUserFieldPropDTO", value = "应用查询参数", required = false, dataType = "DataUserFieldPropDTO", paramType="body"),
		@ApiImplicitParam(name = "pageNo", value = "页码", required = true, dataType = "int", paramType="query"),
		@ApiImplicitParam(name = "pageSize", value = "每页记录数", required = true, dataType = "int", paramType="query"),
		@ApiImplicitParam(name = "sortBy", value = "按照哪个属性排序", required = false, dataType = "String", paramType="query"),
		@ApiImplicitParam(name = "order", value = "排序顺序", required = false, dataType = "String", paramType="query")
	})
	@PostMapping(value = "/query")
	public ResponseVO<DataUserFieldPropDTO>query(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
												@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
												@RequestBody DataUserFieldPropDTO dataUserFieldPropDTO,
												@RequestParam Integer pageNo, @RequestParam Integer pageSize,
												@RequestParam(defaultValue = "create_time") String sortBy, @RequestParam(defaultValue = "desc") String order,
												 @LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){
		Map<Object, Object> map = BeanCopyUtilss.beanToMap(dataUserFieldPropDTO, loginedUserVO);
		if(pageSize != 0) {
			Page newPage = new Page(pageNo + "", pageSize + "");
			map.put("page", newPage);
		}
		List<DataUserFieldProp> list = dataUserFieldPropService.selectListByMap(map);

		//
		List<DataUserFieldPropDTO> dtoList = toDTO(list);

		if(pageSize != 0) {
			Page newPage = new Page(pageNo + "", pageSize + "");
			int countNum = dataUserFieldPropService.selectCountByMap(map);
			newPage.setCount(countNum);
			return ResponseVO.successdatas(dtoList, newPage);
		} else {
			return ResponseVO.successdatas(dtoList, null);
		}

	}

	private List<DataUserFieldPropDTO> toDTO(List<DataUserFieldProp> databaseList) {
		List<DataUserFieldPropDTO> returnList = new ArrayList<DataUserFieldPropDTO>();
		if(databaseList != null) {
			for(DataUserFieldProp dbEntity : databaseList) {
				DataUserFieldPropDTO dtoTemp = new DataUserFieldPropDTO();
				BeanCopyUtilss.myCopyProperties(dbEntity, dtoTemp);
				returnList.add(dtoTemp);
			}
		}
		return returnList;
	}

	private List<DataUserFieldProp> toDB(List<DataUserFieldPropDTO> dtoList) {
		List<DataUserFieldProp> returnList = new ArrayList<DataUserFieldProp>();
		if(dtoList != null) {
			for(DataUserFieldPropDTO dtoEntity : dtoList) {
				DataUserFieldProp dbTemp = new DataUserFieldProp();
				BeanCopyUtilss.myCopyProperties(dtoEntity, dbTemp);
				returnList.add(dbTemp);
			}
		}
		return returnList;
	}
}
