package com.pap.rbac.datauserfield.mapper;

import com.pap.base.mapper.PapBaseMapper;
import com.pap.rbac.datauserfield.entity.DataUserFieldProp;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;;

@Mapper
public interface DataUserFieldPropMapper extends PapBaseMapper<DataUserFieldProp> {
    int deleteByPrimaryKey(String dataUserFieldId);

    int selectCountByMap(Map<Object, Object> map);

    List<DataUserFieldProp> selectListByMap(Map<Object, Object> map);

    DataUserFieldProp selectByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int deleteByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int insert(DataUserFieldProp record);

    int insertSelective(DataUserFieldProp record);

    DataUserFieldProp selectByPrimaryKey(String dataUserFieldId);

    int updateByPrimaryKeySelective(DataUserFieldProp record);

    int updateByPrimaryKey(DataUserFieldProp record);
}