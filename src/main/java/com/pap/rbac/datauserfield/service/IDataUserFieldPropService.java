package com.pap.rbac.datauserfield.service;

import com.pap.rbac.datauserfield.entity.DataUserFieldProp;

import java.util.List;
import java.util.Map;

public interface IDataUserFieldPropService {

    int deleteByPrimaryKey(String id);

    int selectCountByMap(Map<Object, Object> map);

    List<DataUserFieldProp> selectListByMap(Map<Object, Object> map);

    int insert(DataUserFieldProp record);

    int insertSelective(DataUserFieldProp record);

    DataUserFieldProp selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(DataUserFieldProp record);

    int updateByPrimaryKey(DataUserFieldProp record);
}
