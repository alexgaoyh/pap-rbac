package com.pap.rbac.datauserfield.service.impl;

import com.pap.rbac.datauserfield.mapper.DataUserFieldPropMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pap.rbac.datauserfield.entity.DataUserFieldProp;
import com.pap.rbac.datauserfield.service.IDataUserFieldPropService;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Transactional
@Service("dataUserFieldPropService")
public class DataUserFieldPropServiceImpl implements IDataUserFieldPropService {

    @Resource(name = "dataUserFieldPropMapper")
    private DataUserFieldPropMapper mapper;

    @Override
    public int deleteByPrimaryKey(String id) {
       return mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int selectCountByMap(Map<Object, Object> map) {
      return mapper.selectCountByMap(map);
    }

    @Override
    public List<DataUserFieldProp> selectListByMap(Map<Object, Object> map) {
        return mapper.selectListByMap(map);
    }

    @Override
    public int insert(DataUserFieldProp record) {
       return mapper.insert(record);
    }

    @Override
    public int insertSelective(DataUserFieldProp record) {
       return mapper.insertSelective(record);
    }

    @Override
    public DataUserFieldProp selectByPrimaryKey(String id) {
       return mapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(DataUserFieldProp record) {
       return mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(DataUserFieldProp record) {
      return mapper.updateByPrimaryKey(record);
    }
}