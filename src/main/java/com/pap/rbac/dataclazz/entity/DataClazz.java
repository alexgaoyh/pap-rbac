package com.pap.rbac.dataclazz.entity;

import com.pap.base.entity.PapBaseEntity;
import com.pap.base.mybatis.plugin.annotation.*;
import java.io.Serializable;

@MyBatisTableAnnotation(name = "t_rbac_data_clazz", namespace = "com.pap.rbac.dataclazz.mapper.DataClazzMapper", remarks = " 修改点 ", aliasName = "t_rbac_data_clazz t_rbac_data_clazz" )
public class DataClazz extends PapBaseEntity implements Serializable {
    /**
     *  编号,所属表字段为t_rbac_data_clazz.DATA_CLAZZ_ID
     */
    @MyBatisColumnAnnotation(name = "DATA_CLAZZ_ID", value = "t_rbac_data_clazz_DATA_CLAZZ_ID", chineseNote = "编号", tableAlias = "t_rbac_data_clazz")
    @MyApiModelPropertyAnnotation(value = "编号")
    private String dataClazzId;

    /**
     *  编码,所属表字段为t_rbac_data_clazz.DATA_CLAZZ_CODE
     */
    @MyBatisColumnAnnotation(name = "DATA_CLAZZ_CODE", value = "t_rbac_data_clazz_DATA_CLAZZ_CODE", chineseNote = "编码", tableAlias = "t_rbac_data_clazz")
    @MyApiModelPropertyAnnotation(value = "编码")
    private String dataClazzCode;

    /**
     *  所属项目编号,所属表字段为t_rbac_data_clazz.DATA_PROJECT_ID
     */
    @MyBatisColumnAnnotation(name = "DATA_PROJECT_ID", value = "t_rbac_data_clazz_DATA_PROJECT_ID", chineseNote = "所属项目编号", tableAlias = "t_rbac_data_clazz")
    @MyApiModelPropertyAnnotation(value = "所属项目编号")
    private String dataProjectId;

    /**
     *  所属模块编号,所属表字段为t_rbac_data_clazz.DATA_MODULE_ID
     */
    @MyBatisColumnAnnotation(name = "DATA_MODULE_ID", value = "t_rbac_data_clazz_DATA_MODULE_ID", chineseNote = "所属模块编号", tableAlias = "t_rbac_data_clazz")
    @MyApiModelPropertyAnnotation(value = "所属模块编号")
    private String dataModuleId;

    /**
     *  名称,所属表字段为t_rbac_data_clazz.DATA_CLAZZ_NAME
     */
    @MyBatisColumnAnnotation(name = "DATA_CLAZZ_NAME", value = "t_rbac_data_clazz_DATA_CLAZZ_NAME", chineseNote = "名称", tableAlias = "t_rbac_data_clazz")
    @MyApiModelPropertyAnnotation(value = "名称")
    private String dataClazzName;

    /**
     *  别名,所属表字段为t_rbac_data_clazz.DATA_CLAZZ_ALAIS_NAME
     */
    @MyBatisColumnAnnotation(name = "DATA_CLAZZ_ALAIS_NAME", value = "t_rbac_data_clazz_DATA_CLAZZ_ALAIS_NAME", chineseNote = "别名", tableAlias = "t_rbac_data_clazz")
    @MyApiModelPropertyAnnotation(value = "别名")
    private String dataClazzAlaisName;

    /**
     *  类全路径,所属表字段为t_rbac_data_clazz.DATA_CLAZZ_PATH
     */
    @MyBatisColumnAnnotation(name = "DATA_CLAZZ_PATH", value = "t_rbac_data_clazz_DATA_CLAZZ_PATH", chineseNote = "类全路径", tableAlias = "t_rbac_data_clazz")
    @MyApiModelPropertyAnnotation(value = "类全路径")
    private String dataClazzPath;

    /**
     *  可用状态标识,所属表字段为t_rbac_data_clazz.DISABLE_FLAG
     */
    @MyBatisColumnAnnotation(name = "DISABLE_FLAG", value = "t_rbac_data_clazz_DISABLE_FLAG", chineseNote = "可用状态标识", tableAlias = "t_rbac_data_clazz")
    @MyApiModelPropertyAnnotation(value = "可用状态标识")
    private String disableFlag;

    /**
     *  删除状态标识,所属表字段为t_rbac_data_clazz.DELETE_FLAG
     */
    @MyBatisColumnAnnotation(name = "DELETE_FLAG", value = "t_rbac_data_clazz_DELETE_FLAG", chineseNote = "删除状态标识", tableAlias = "t_rbac_data_clazz")
    @MyApiModelPropertyAnnotation(value = "删除状态标识")
    private String deleteFlag;

    /**
     *  备注,所属表字段为t_rbac_data_clazz.REMARK
     */
    @MyBatisColumnAnnotation(name = "REMARK", value = "t_rbac_data_clazz_REMARK", chineseNote = "备注", tableAlias = "t_rbac_data_clazz")
    @MyApiModelPropertyAnnotation(value = "备注")
    private String remark;

    @Override
    public String getDynamicTableName() {
        return "t_rbac_data_clazz";
    }

    private static final long serialVersionUID = 1L;

    public String getDataClazzId() {
        return dataClazzId;
    }

    public void setDataClazzId(String dataClazzId) {
        this.dataClazzId = dataClazzId;
    }

    public String getDataClazzCode() {
        return dataClazzCode;
    }

    public void setDataClazzCode(String dataClazzCode) {
        this.dataClazzCode = dataClazzCode;
    }

    public String getDataProjectId() {
        return dataProjectId;
    }

    public void setDataProjectId(String dataProjectId) {
        this.dataProjectId = dataProjectId;
    }

    public String getDataModuleId() {
        return dataModuleId;
    }

    public void setDataModuleId(String dataModuleId) {
        this.dataModuleId = dataModuleId;
    }

    public String getDataClazzName() {
        return dataClazzName;
    }

    public void setDataClazzName(String dataClazzName) {
        this.dataClazzName = dataClazzName;
    }

    public String getDataClazzAlaisName() {
        return dataClazzAlaisName;
    }

    public void setDataClazzAlaisName(String dataClazzAlaisName) {
        this.dataClazzAlaisName = dataClazzAlaisName;
    }

    public String getDataClazzPath() {
        return dataClazzPath;
    }

    public void setDataClazzPath(String dataClazzPath) {
        this.dataClazzPath = dataClazzPath;
    }

    public String getDisableFlag() {
        return disableFlag;
    }

    public void setDisableFlag(String disableFlag) {
        this.disableFlag = disableFlag;
    }

    public String getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(String deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", dataClazzId=").append(dataClazzId);
        sb.append(", dataClazzCode=").append(dataClazzCode);
        sb.append(", dataProjectId=").append(dataProjectId);
        sb.append(", dataModuleId=").append(dataModuleId);
        sb.append(", dataClazzName=").append(dataClazzName);
        sb.append(", dataClazzAlaisName=").append(dataClazzAlaisName);
        sb.append(", dataClazzPath=").append(dataClazzPath);
        sb.append(", disableFlag=").append(disableFlag);
        sb.append(", deleteFlag=").append(deleteFlag);
        sb.append(", remark=").append(remark);
        sb.append("]");
        return sb.toString();
    }
}