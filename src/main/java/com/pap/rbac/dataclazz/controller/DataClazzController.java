package com.pap.rbac.dataclazz.controller;

import com.pap.base.util.bean.BeanCopyUtilss;
import com.pap.beans.idworker.IdWorker;
import com.pap.obj.vo.logineduser.LoginedUser;
import com.pap.obj.vo.logineduser.LoginedUserVO;
import com.pap.obj.vo.page.Page;
import com.pap.obj.vo.response.ResponseVO;
import com.pap.rbac.dataclazz.entity.DataClazz;
import com.pap.rbac.dataclazz.service.IDataClazzService;
import com.pap.rbac.dto.DataClazzDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/dataClazz")
@Api(value = "数据权限类模块", tags = "数据权限类模块", description="数据权限类模块")
public class DataClazzController {

	private static Logger logger  =  LoggerFactory.getLogger(DataClazzController.class);
	
	@Resource(name = "dataClazzService")
	private IDataClazzService dataClazzService;

	@Resource(name = "idWorker")
	private IdWorker idWorker;

	@ApiOperation("创建")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "dataClazzDTO", value = "", required = true, dataType = "DataClazzDTO", paramType="body")
	})
	@PostMapping(value = "")
	public ResponseVO<DataClazzDTO> create(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
										   @RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
										   @RequestBody DataClazzDTO dataClazzDTO,
										   @LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){
		String idStr = idWorker.nextIdStr();
		dataClazzDTO.setDataClazzId(idStr);

		DataClazz databaseObj = new DataClazz();
		BeanCopyUtilss.myCopyProperties(dataClazzDTO, databaseObj, loginedUserVO, true);
		int i = dataClazzService.insertSelective(databaseObj);

		return ResponseVO.successdata(databaseObj);
	}

	@ApiOperation("更新")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "dataClazzDTO", value = "", required = true, dataType = "DataClazzDTO", paramType="body")
	})
	@PutMapping(value = "")
	public ResponseVO<DataClazzDTO> update(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@RequestBody DataClazzDTO dataClazzDTO,
										   @LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){

		DataClazz databaseObj = new DataClazz();
		BeanCopyUtilss.myCopyProperties(dataClazzDTO, databaseObj, loginedUserVO, false);
		int i = dataClazzService.updateByPrimaryKeySelective(databaseObj);

		databaseObj = dataClazzService.selectByPrimaryKey(databaseObj.getDataClazzId());

		return ResponseVO.successdata(databaseObj);
	}


	@ApiOperation("删除")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "dataClazzDTO", value = "", required = true, dataType = "DataClazzDTO", paramType="body")
	})
	@DeleteMapping(value = "")
	public ResponseVO<String> delete(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@RequestBody DataClazzDTO dataClazzDTO){

		int i = dataClazzService.deleteByPrimaryKey(dataClazzDTO.getDataClazzId());

		return ResponseVO.successdata("操作成功");
	}

	@ApiOperation("详情")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "id", value = "编号", required = true, dataType = "String", paramType="path")
	})
	@GetMapping(value = "/{id}")
	public ResponseVO<DataClazzDTO> selectOne(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@PathVariable("id") String id){

		DataClazz databaseObj = dataClazzService.selectByPrimaryKey(id);

		return ResponseVO.successdata(databaseObj);
	}

	@ApiOperation("查询")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "dataClazzDTO", value = "应用查询参数", required = false, dataType = "DataClazzDTO", paramType="body"),
		@ApiImplicitParam(name = "pageNo", value = "页码", required = true, dataType = "int", paramType="query"),
		@ApiImplicitParam(name = "pageSize", value = "每页记录数", required = true, dataType = "int", paramType="query"),
		@ApiImplicitParam(name = "sortBy", value = "按照哪个属性排序", required = false, dataType = "String", paramType="query"),
		@ApiImplicitParam(name = "order", value = "排序顺序", required = false, dataType = "String", paramType="query")
	})
	@PostMapping(value = "/query")
	public ResponseVO<DataClazzDTO>query(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
												@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
												@RequestBody DataClazzDTO dataClazzDTO,
												@RequestParam Integer pageNo, @RequestParam Integer pageSize,
												@RequestParam(defaultValue = "create_time") String sortBy, @RequestParam(defaultValue = "desc") String order,
										 @LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){

		Map<Object, Object> map = BeanCopyUtilss.beanToMap(dataClazzDTO, loginedUserVO);
		if(pageSize != 0) {
			Page newPage = new Page(pageNo + "", pageSize + "");
			map.put("page", newPage);
		}
		List<DataClazz> list = dataClazzService.selectListByMap(map);

		//
		List<DataClazzDTO> dtoList = toDTO(list);

		if(pageSize != 0) {
			Page newPage = new Page(pageNo + "", pageSize + "");
			int countNum = dataClazzService.selectCountByMap(map);
			newPage.setCount(countNum);
			return ResponseVO.successdatas(dtoList, newPage);
		} else {
			return ResponseVO.successdatas(dtoList, null);
		}

	}

	private List<DataClazzDTO> toDTO(List<DataClazz> databaseList) {
		List<DataClazzDTO> returnList = new ArrayList<DataClazzDTO>();
		if(databaseList != null) {
			for(DataClazz dbEntity : databaseList) {
				DataClazzDTO dtoTemp = new DataClazzDTO();
				BeanCopyUtilss.myCopyProperties(dbEntity, dtoTemp);
				returnList.add(dtoTemp);
			}
		}
		return returnList;
	}

	private List<DataClazz> toDB(List<DataClazzDTO> dtoList) {
		List<DataClazz> returnList = new ArrayList<DataClazz>();
		if(dtoList != null) {
			for(DataClazzDTO dtoEntity : dtoList) {
				DataClazz dbTemp = new DataClazz();
				BeanCopyUtilss.myCopyProperties(dtoEntity, dbTemp);
				returnList.add(dbTemp);
			}
		}
		return returnList;
	}
}
