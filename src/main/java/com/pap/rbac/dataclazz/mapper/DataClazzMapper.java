package com.pap.rbac.dataclazz.mapper;

import com.pap.base.mapper.PapBaseMapper;
import com.pap.rbac.dataclazz.entity.DataClazz;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;;

@Mapper
public interface DataClazzMapper extends PapBaseMapper<DataClazz> {
    int deleteByPrimaryKey(String dataClazzId);

    int selectCountByMap(Map<Object, Object> map);

    List<DataClazz> selectListByMap(Map<Object, Object> map);

    DataClazz selectByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int deleteByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int insert(DataClazz record);

    int insertSelective(DataClazz record);

    DataClazz selectByPrimaryKey(String dataClazzId);

    int updateByPrimaryKeySelective(DataClazz record);

    int updateByPrimaryKey(DataClazz record);
}