package com.pap.rbac.dataclazz.service.impl;

import com.pap.rbac.dataclazz.mapper.DataClazzMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pap.rbac.dataclazz.entity.DataClazz;
import com.pap.rbac.dataclazz.service.IDataClazzService;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Transactional
@Service("dataClazzService")
public class DataClazzServiceImpl implements IDataClazzService {

    @Resource(name = "dataClazzMapper")
    private DataClazzMapper mapper;

    @Override
    public int deleteByPrimaryKey(String id) {
       return mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int selectCountByMap(Map<Object, Object> map) {
      return mapper.selectCountByMap(map);
    }

    @Override
    public List<DataClazz> selectListByMap(Map<Object, Object> map) {
        return mapper.selectListByMap(map);
    }

    @Override
    public int insert(DataClazz record) {
       return mapper.insert(record);
    }

    @Override
    public int insertSelective(DataClazz record) {
       return mapper.insertSelective(record);
    }

    @Override
    public DataClazz selectByPrimaryKey(String id) {
       return mapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(DataClazz record) {
       return mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(DataClazz record) {
      return mapper.updateByPrimaryKey(record);
    }
}