package com.pap.rbac.dataclazz.service;

import com.pap.rbac.dataclazz.entity.DataClazz;

import java.util.List;
import java.util.Map;

public interface IDataClazzService {

    int deleteByPrimaryKey(String id);

    int selectCountByMap(Map<Object, Object> map);

    List<DataClazz> selectListByMap(Map<Object, Object> map);

    int insert(DataClazz record);

    int insertSelective(DataClazz record);

    DataClazz selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(DataClazz record);

    int updateByPrimaryKey(DataClazz record);
}
