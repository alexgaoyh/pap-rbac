package com.pap.rbac.datamodule.controller;

import com.pap.base.util.bean.BeanCopyUtilss;
import com.pap.beans.idworker.IdWorker;
import com.pap.obj.vo.logineduser.LoginedUser;
import com.pap.obj.vo.logineduser.LoginedUserVO;
import com.pap.obj.vo.page.Page;
import com.pap.obj.vo.response.ResponseVO;
import com.pap.rbac.datamodule.entity.DataModule;
import com.pap.rbac.datamodule.service.IDataModuleService;
import com.pap.rbac.dto.DataModuleDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/dataModule")
@Api(value = "数据权限模块", tags = "数据权限模块", description="数据权限模块")
public class DataModuleController {

	private static Logger logger  =  LoggerFactory.getLogger(DataModuleController.class);
	
	@Resource(name = "dataModuleService")
	private IDataModuleService dataModuleService;

	@Resource(name = "idWorker")
	private IdWorker idWorker;

	@ApiOperation("创建")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "dataModuleDTO", value = "", required = true, dataType = "DataModuleDTO", paramType="body")
	})
	@PostMapping(value = "")
	public ResponseVO<DataModuleDTO> create(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
											@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
											@RequestBody DataModuleDTO dataModuleDTO,
											@LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){
		String idStr = idWorker.nextIdStr();
		dataModuleDTO.setDataModuleId(idStr);

		DataModule databaseObj = new DataModule();
		BeanCopyUtilss.myCopyProperties(dataModuleDTO, databaseObj, loginedUserVO, true);
		int i = dataModuleService.insertSelective(databaseObj);

		return ResponseVO.successdata(databaseObj);
	}

	@ApiOperation("更新")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "dataModuleDTO", value = "", required = true, dataType = "DataModuleDTO", paramType="body")
	})
	@PutMapping(value = "")
	public ResponseVO<DataModuleDTO> update(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@RequestBody DataModuleDTO dataModuleDTO,
											@LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){

		DataModule databaseObj = new DataModule();
		BeanCopyUtilss.myCopyProperties(dataModuleDTO, databaseObj, loginedUserVO, false);
		int i = dataModuleService.updateByPrimaryKeySelective(databaseObj);

		databaseObj = dataModuleService.selectByPrimaryKey(databaseObj.getDataModuleId());

		return ResponseVO.successdata(databaseObj);
	}


	@ApiOperation("删除")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "dataModuleDTO", value = "", required = true, dataType = "DataModuleDTO", paramType="body")
	})
	@DeleteMapping(value = "")
	public ResponseVO<String> delete(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@RequestBody DataModuleDTO dataModuleDTO){

		int i = dataModuleService.deleteByPrimaryKey(dataModuleDTO.getDataModuleId());

		return ResponseVO.successdata("操作成功");
	}

	@ApiOperation("详情")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "id", value = "编号", required = true, dataType = "String", paramType="path")
	})
	@GetMapping(value = "/{id}")
	public ResponseVO<DataModuleDTO> selectOne(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@PathVariable("id") String id){

		DataModule databaseObj = dataModuleService.selectByPrimaryKey(id);

		return ResponseVO.successdata(databaseObj);
	}

	@ApiOperation("查询")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "dataModuleDTO", value = "应用查询参数", required = false, dataType = "DataModuleDTO", paramType="body"),
		@ApiImplicitParam(name = "pageNo", value = "页码", required = true, dataType = "int", paramType="query"),
		@ApiImplicitParam(name = "pageSize", value = "每页记录数", required = true, dataType = "int", paramType="query"),
		@ApiImplicitParam(name = "sortBy", value = "按照哪个属性排序", required = false, dataType = "String", paramType="query"),
		@ApiImplicitParam(name = "order", value = "排序顺序", required = false, dataType = "String", paramType="query")
	})
	@PostMapping(value = "/query")
	public ResponseVO<DataModuleDTO>query(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
												@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
												@RequestBody DataModuleDTO dataModuleDTO,
												@RequestParam Integer pageNo, @RequestParam Integer pageSize,
												@RequestParam(defaultValue = "create_time") String sortBy, @RequestParam(defaultValue = "desc") String order,
										  @LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){
		Map<Object, Object> map = BeanCopyUtilss.beanToMap(dataModuleDTO, loginedUserVO);
		if(pageSize != 0) {
			Page newPage = new Page(pageNo + "", pageSize + "");
			map.put("page", newPage);
		}
		List<DataModule> list = dataModuleService.selectListByMap(map);

		//
		List<DataModuleDTO> dtoList = toDTO(list);

		if(pageSize != 0) {
			Page newPage = new Page(pageNo + "", pageSize + "");
			int countNum = dataModuleService.selectCountByMap(map);
			newPage.setCount(countNum);
			return ResponseVO.successdatas(dtoList, newPage);
		} else {
			return ResponseVO.successdatas(dtoList, null);
		}

	}

	private List<DataModuleDTO> toDTO(List<DataModule> databaseList) {
		List<DataModuleDTO> returnList = new ArrayList<DataModuleDTO>();
		if(databaseList != null) {
			for(DataModule dbEntity : databaseList) {
				DataModuleDTO dtoTemp = new DataModuleDTO();
				BeanCopyUtilss.myCopyProperties(dbEntity, dtoTemp);
				returnList.add(dtoTemp);
			}
		}
		return returnList;
	}

	private List<DataModule> toDB(List<DataModuleDTO> dtoList) {
		List<DataModule> returnList = new ArrayList<DataModule>();
		if(dtoList != null) {
			for(DataModuleDTO dtoEntity : dtoList) {
				DataModule dbTemp = new DataModule();
				BeanCopyUtilss.myCopyProperties(dtoEntity, dbTemp);
				returnList.add(dbTemp);
			}
		}
		return returnList;
	}
}
