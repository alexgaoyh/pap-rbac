package com.pap.rbac.datamodule.entity;

import com.pap.base.entity.PapBaseEntity;
import com.pap.base.mybatis.plugin.annotation.*;
import java.io.Serializable;

@MyBatisTableAnnotation(name = "t_rbac_data_module", namespace = "com.pap.rbac.datamodule.mapper.DataModuleMapper", remarks = " 修改点 ", aliasName = "t_rbac_data_module t_rbac_data_module" )
public class DataModule extends PapBaseEntity implements Serializable {
    /**
     *  编号,所属表字段为t_rbac_data_module.DATA_MODULE_ID
     */
    @MyBatisColumnAnnotation(name = "DATA_MODULE_ID", value = "t_rbac_data_module_DATA_MODULE_ID", chineseNote = "编号", tableAlias = "t_rbac_data_module")
    @MyApiModelPropertyAnnotation(value = "编号")
    private String dataModuleId;

    /**
     *  编码,所属表字段为t_rbac_data_module.DATA_MODULE_CODE
     */
    @MyBatisColumnAnnotation(name = "DATA_MODULE_CODE", value = "t_rbac_data_module_DATA_MODULE_CODE", chineseNote = "编码", tableAlias = "t_rbac_data_module")
    @MyApiModelPropertyAnnotation(value = "编码")
    private String dataModuleCode;

    /**
     *  所属项目编号,所属表字段为t_rbac_data_module.DATA_PROJECT_ID
     */
    @MyBatisColumnAnnotation(name = "DATA_PROJECT_ID", value = "t_rbac_data_module_DATA_PROJECT_ID", chineseNote = "所属项目编号", tableAlias = "t_rbac_data_module")
    @MyApiModelPropertyAnnotation(value = "所属项目编号")
    private String dataProjectId;

    /**
     *  姓名,所属表字段为t_rbac_data_module.DATA_MODULE_NAME
     */
    @MyBatisColumnAnnotation(name = "DATA_MODULE_NAME", value = "t_rbac_data_module_DATA_MODULE_NAME", chineseNote = "姓名", tableAlias = "t_rbac_data_module")
    @MyApiModelPropertyAnnotation(value = "姓名")
    private String dataModuleName;

    /**
     *  模块域名前缀,所属表字段为t_rbac_data_module.DATA_MODULE__DOMAIN
     */
    @MyBatisColumnAnnotation(name = "DATA_MODULE__DOMAIN", value = "t_rbac_data_module_DATA_MODULE__DOMAIN", chineseNote = "模块域名前缀", tableAlias = "t_rbac_data_module")
    @MyApiModelPropertyAnnotation(value = "模块域名前缀")
    private String dataModuleDomain;

    /**
     *  管理人员,所属表字段为t_rbac_data_module.DATA_MODULE_MANAGER
     */
    @MyBatisColumnAnnotation(name = "DATA_MODULE_MANAGER", value = "t_rbac_data_module_DATA_MODULE_MANAGER", chineseNote = "管理人员", tableAlias = "t_rbac_data_module")
    @MyApiModelPropertyAnnotation(value = "管理人员")
    private String dataModuleManager;

    /**
     *  管理人员电话,所属表字段为t_rbac_data_module.DATA_MODULE_MOBILE
     */
    @MyBatisColumnAnnotation(name = "DATA_MODULE_MOBILE", value = "t_rbac_data_module_DATA_MODULE_MOBILE", chineseNote = "管理人员电话", tableAlias = "t_rbac_data_module")
    @MyApiModelPropertyAnnotation(value = "管理人员电话")
    private String dataModuleMobile;

    /**
     *  可用状态标识,所属表字段为t_rbac_data_module.DISABLE_FLAG
     */
    @MyBatisColumnAnnotation(name = "DISABLE_FLAG", value = "t_rbac_data_module_DISABLE_FLAG", chineseNote = "可用状态标识", tableAlias = "t_rbac_data_module")
    @MyApiModelPropertyAnnotation(value = "可用状态标识")
    private String disableFlag;

    /**
     *  删除状态标识,所属表字段为t_rbac_data_module.DELETE_FLAG
     */
    @MyBatisColumnAnnotation(name = "DELETE_FLAG", value = "t_rbac_data_module_DELETE_FLAG", chineseNote = "删除状态标识", tableAlias = "t_rbac_data_module")
    @MyApiModelPropertyAnnotation(value = "删除状态标识")
    private String deleteFlag;

    /**
     *  备注,所属表字段为t_rbac_data_module.REMARK
     */
    @MyBatisColumnAnnotation(name = "REMARK", value = "t_rbac_data_module_REMARK", chineseNote = "备注", tableAlias = "t_rbac_data_module")
    @MyApiModelPropertyAnnotation(value = "备注")
    private String remark;

    @Override
    public String getDynamicTableName() {
        return "t_rbac_data_module";
    }

    private static final long serialVersionUID = 1L;

    public String getDataModuleId() {
        return dataModuleId;
    }

    public void setDataModuleId(String dataModuleId) {
        this.dataModuleId = dataModuleId;
    }

    public String getDataModuleCode() {
        return dataModuleCode;
    }

    public void setDataModuleCode(String dataModuleCode) {
        this.dataModuleCode = dataModuleCode;
    }

    public String getDataProjectId() {
        return dataProjectId;
    }

    public void setDataProjectId(String dataProjectId) {
        this.dataProjectId = dataProjectId;
    }

    public String getDataModuleName() {
        return dataModuleName;
    }

    public void setDataModuleName(String dataModuleName) {
        this.dataModuleName = dataModuleName;
    }

    public String getDataModuleDomain() {
        return dataModuleDomain;
    }

    public void setDataModuleDomain(String dataModuleDomain) {
        this.dataModuleDomain = dataModuleDomain;
    }

    public String getDataModuleManager() {
        return dataModuleManager;
    }

    public void setDataModuleManager(String dataModuleManager) {
        this.dataModuleManager = dataModuleManager;
    }

    public String getDataModuleMobile() {
        return dataModuleMobile;
    }

    public void setDataModuleMobile(String dataModuleMobile) {
        this.dataModuleMobile = dataModuleMobile;
    }

    public String getDisableFlag() {
        return disableFlag;
    }

    public void setDisableFlag(String disableFlag) {
        this.disableFlag = disableFlag;
    }

    public String getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(String deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", dataModuleId=").append(dataModuleId);
        sb.append(", dataModuleCode=").append(dataModuleCode);
        sb.append(", dataProjectId=").append(dataProjectId);
        sb.append(", dataModuleName=").append(dataModuleName);
        sb.append(", dataModuleDomain=").append(dataModuleDomain);
        sb.append(", dataModuleManager=").append(dataModuleManager);
        sb.append(", dataModuleMobile=").append(dataModuleMobile);
        sb.append(", disableFlag=").append(disableFlag);
        sb.append(", deleteFlag=").append(deleteFlag);
        sb.append(", remark=").append(remark);
        sb.append("]");
        return sb.toString();
    }
}