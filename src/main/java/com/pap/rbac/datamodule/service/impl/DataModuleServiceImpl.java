package com.pap.rbac.datamodule.service.impl;

import com.pap.rbac.datamodule.mapper.DataModuleMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pap.rbac.datamodule.entity.DataModule;
import com.pap.rbac.datamodule.service.IDataModuleService;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Transactional
@Service("dataModuleService")
public class DataModuleServiceImpl implements IDataModuleService {

    @Resource(name = "dataModuleMapper")
    private DataModuleMapper mapper;

    @Override
    public int deleteByPrimaryKey(String id) {
       return mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int selectCountByMap(Map<Object, Object> map) {
      return mapper.selectCountByMap(map);
    }

    @Override
    public List<DataModule> selectListByMap(Map<Object, Object> map) {
        return mapper.selectListByMap(map);
    }

    @Override
    public int insert(DataModule record) {
       return mapper.insert(record);
    }

    @Override
    public int insertSelective(DataModule record) {
       return mapper.insertSelective(record);
    }

    @Override
    public DataModule selectByPrimaryKey(String id) {
       return mapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(DataModule record) {
       return mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(DataModule record) {
      return mapper.updateByPrimaryKey(record);
    }
}