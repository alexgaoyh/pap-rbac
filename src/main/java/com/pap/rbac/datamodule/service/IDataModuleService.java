package com.pap.rbac.datamodule.service;

import com.pap.rbac.datamodule.entity.DataModule;

import java.util.List;
import java.util.Map;

public interface IDataModuleService {

    int deleteByPrimaryKey(String id);

    int selectCountByMap(Map<Object, Object> map);

    List<DataModule> selectListByMap(Map<Object, Object> map);

    int insert(DataModule record);

    int insertSelective(DataModule record);

    DataModule selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(DataModule record);

    int updateByPrimaryKey(DataModule record);
}
