package com.pap.rbac.datamodule.mapper;

import com.pap.base.mapper.PapBaseMapper;
import com.pap.rbac.datamodule.entity.DataModule;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;;

@Mapper
public interface DataModuleMapper extends PapBaseMapper<DataModule> {
    int deleteByPrimaryKey(String dataModuleId);

    int selectCountByMap(Map<Object, Object> map);

    List<DataModule> selectListByMap(Map<Object, Object> map);

    DataModule selectByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int deleteByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int insert(DataModule record);

    int insertSelective(DataModule record);

    DataModule selectByPrimaryKey(String dataModuleId);

    int updateByPrimaryKeySelective(DataModule record);

    int updateByPrimaryKey(DataModule record);
}