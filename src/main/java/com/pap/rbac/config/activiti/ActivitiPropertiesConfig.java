//package com.pap.rbac.config.activiti;
//
//import java.io.IOException;
//import java.io.InputStream;
//import java.util.Properties;
//
///**
// * activiti 工作流是否开启的状态处理
// * 
// * @author alexgaoyh
// *
// */
//public class ActivitiPropertiesConfig {
//
//	private static ActivitiPropertiesConfig instance = new ActivitiPropertiesConfig();
//
//	public static ActivitiPropertiesConfig getInstance() {
//		return instance;
//	}
//
//	private String activitiOpenFlag;// 用来存放配置文件中参数A的值
//	
//	public String getActivitiOpenFlag() {
//		return activitiOpenFlag;
//	}
//
//	private ActivitiPropertiesConfig() {
//		readConfig();// 调用读取配置文件的方法
//	}
//
//	// 读取配置文件，将配置文件中的内容读取出来设置到属性上
//	private void readConfig() {
//		Properties p = new Properties();
//		InputStream in = null;
//		try {
//			in = ActivitiPropertiesConfig.class.getResourceAsStream("config/activiti.properties");
//			p.load(in);
//			this.activitiOpenFlag = p.getProperty("activiti.open.flag");
//		} catch (IOException e) {
//			System.out.println("装载配置文件出错了，具体堆栈信息如下：");
//			e.printStackTrace();
//		} finally {
//			try {
//				in.close();
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
//		}
//	}
//	
//}
