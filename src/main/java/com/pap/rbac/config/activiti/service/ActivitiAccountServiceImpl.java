//package com.pap.rbac.config.activiti.service;
//
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import org.activiti.engine.ProcessEngine;
//import org.activiti.engine.identity.Group;
//import org.activiti.engine.identity.UserQuery;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import com.pap.rbac.config.activiti.ActivitiPropertiesConfig;
//import com.pap.rbac.login.controller.LoginController;
//import com.pap.rbac.role.auto.entity.Role;
//import com.pap.rbac.role.auto.mapper.RoleMapper;
//import com.pap.rbac.user.auto.entity.User;
//import com.pap.rbac.user.auto.mapper.UserMapper;
//import com.pap.rbac.userrole.auto.entity.UserRoleRel;
//import com.pap.rbac.userrole.auto.mapper.UserRoleRelMapper;
//
//@Transactional
//@Service("activitiAccountService")
//public class ActivitiAccountServiceImpl implements IActivitiAccountService {
//
//	private static Logger logger = LoggerFactory.getLogger(ActivitiAccountServiceImpl.class);
//
//	@Autowired
//	private UserMapper userMapper;
//
//	@Autowired
//	private RoleMapper roleMapper;
//
//	@Autowired
//	private UserRoleRelMapper userRoleRelMapper;
//
//	@Autowired
//	private ProcessEngine processEngine;
//
//	@Override
//	public void synAllUserAndRoleToActiviti() throws Exception {
//		// 清空工作流用户、角色以及关系
//		deleteAllActivitiIdentifyData();
//
//		// 复制角色数据
//		synRoleToActiviti();
//		
//		// 复制user数据
//		synUserToActiviti();
//
//		// 复制用户以及关系数据
//		synUserWithRoleToActiviti();
//	}
//
//	/**
//	 * 复制用户以及关系数据
//	 */
//	private void synUserWithRoleToActiviti() {
//		// 角色和用户的关系
//		List<UserRoleRel> userRoleRelList = userRoleRelMapper.selectListByMap(null);
//		for (UserRoleRel userRoleRel : userRoleRelList) {
//			processEngine.getIdentityService().createMembership(userRoleRel.getUserId(), userRoleRel.getRoleId());
//			logger.debug("add membership {user: {}, role: {}}", userRoleRel.getUserId(), userRoleRel.getRoleId());
//		}
//	}
//
//	/**
//	 * 同步所有角色数据到{@link Group}
//	 */
//	private void synRoleToActiviti() {
//		List<Role> allRole = roleMapper.selectListByMap(null);
//		for (Role role : allRole) {
//			String groupId = role.getId();
//			Group group = processEngine.getIdentityService().newGroup(groupId);
//			group.setName(role.getName());
//			group.setType(role.getClientLicenseId());
//			processEngine.getIdentityService().saveGroup(group);
//		}
//	}
//	
//	/**
//	 * 同步所有user数据
//	 */
//	private void synUserToActiviti() {
//		List<User> allUser = userMapper.selectListByMap(null);
//		for (User user : allUser) {
//			org.activiti.engine.identity.User identityUser = processEngine.getIdentityService().newUser(user.getId());
//			identityUser.setFirstName(user.getLoginName());
//			identityUser.setLastName(user.getName());
//			identityUser.setEmail(user.getEmail());
//			identityUser.setPassword(user.getPasswords());
//			processEngine.getIdentityService().saveUser(identityUser);
//		}
//	}
//
//	@Override
//	public void deleteAllActivitiIdentifyData() throws Exception {
//		// first delete rel table
//		userMapper.deleteActivitiAllMemerShip();
//		userMapper.deleteActivitiAllUser();
//		userMapper.deleteActivitiAllRole();
//	}
//
//	@Override
//	public void save(User user, String orgId, List<String> roleIds) throws Exception {
//		String userId = user.getId();
//
//		// 同步数据到Activiti Identify模块
//		if (Boolean.parseBoolean(ActivitiPropertiesConfig.getInstance().getActivitiOpenFlag())) {
//			UserQuery userQuery = processEngine.getIdentityService().createUserQuery();
//			List<org.activiti.engine.identity.User> activitiUsers = userQuery.userId(userId).list();
//
//			if (activitiUsers.size() == 1) {
//				updateActivitiData(user, roleIds, activitiUsers.get(0));
//			} else if (activitiUsers.size() > 1) {
//				String errorMsg = "发现重复用户：id=" + userId;
//				logger.error(errorMsg);
//				throw new RuntimeException(errorMsg);
//			} else {
//				newActivitiUser(user, roleIds);
//			}
//		}
//	}
//	/** 
//     * 添加工作流用户以及角色 
//     * @param user      用户对象{@link User} 
//     * @param roleIds   用户拥有的角色ID集合 
//     */  
//    private void newActivitiUser(User user, List<String> roleIds) {  
//        String userId = user.getId().toString();  
//   
//        // 添加用户  
//        saveActivitiUser(user);  
//   
//        // 添加membership  
//        addMembershipToIdentify(roleIds, userId);  
//    }  
//    
//    /** 
//     * 添加一个用户到Activiti {@link org.activiti.engine.identity.User} 
//     * @param user  用户对象, {@link User} 
//     */  
//    private void saveActivitiUser(User user) {  
//        String userId = user.getId().toString();  
//        org.activiti.engine.identity.User activitiUser = processEngine.getIdentityService().newUser(userId);  
//        cloneAndSaveActivitiUser(user, activitiUser);  
//        logger.debug("add activiti user: {}", activitiUser.getId());  
//    } 
//    
//    /** 
//     * 添加Activiti Identify的用户于组关系 
//     * @param roleIds   角色ID集合 
//     * @param userId    用户ID 
//     */  
//    private void addMembershipToIdentify(List<String> roleIds, String userId) {  
//        for(String roleId : roleIds) {  
//            Role role = roleMapper.selectByPrimaryKey(roleId);
//            logger.debug("add role to activit: {}", role);  
//            processEngine.getIdentityService().createMembership(userId, role.getName());  
//        }  
//    } 
//	
//	/** 
//     * 更新工作流用户以及角色 
//     * @param user          用户对象{@link User} 
//     * @param roleIds       用户拥有的角色ID集合 
//     * @param activitiUser  Activiti引擎的用户对象，{@link org.activiti.engine.identity.User} 
//     */  
//    private void updateActivitiData(User user, List<String> roleIds, org.activiti.engine.identity.User activitiUser) {  
//   
//        String userId = user.getId().toString();  
//   
//        // 更新用户主体信息  
//        cloneAndSaveActivitiUser(user, activitiUser);  
//   
//        // 删除用户的membership  
//        List<Group> activitiGroups = processEngine.getIdentityService().createGroupQuery().groupMember(userId).list();  
//        for(Group group : activitiGroups) {  
//            logger.debug("delete group from activit: {}", group.getId());  
//            processEngine.getIdentityService().deleteMembership(userId, group.getId());  
//        }  
//   
//        // 添加membership  
//        addMembershipToIdentify(roleIds, userId);  
//    }  
//    
//    /** 
//     * 使用系统用户对象属性设置到Activiti User对象中 
//     * @param user          系统用户对象 
//     * @param activitiUser  Activiti User 
//     */  
//    private void cloneAndSaveActivitiUser(User user, org.activiti.engine.identity.User activitiUser) {  
//        activitiUser.setFirstName(user.getName());  
//        activitiUser.setLastName(user.getName());  
//        activitiUser.setPassword(user.getName());  
//        activitiUser.setEmail(user.getEmail());  
//        processEngine.getIdentityService().saveUser(activitiUser);  
//    }  
//
//	@Override
//	public void delete(String userId) throws Exception {
//		/**
//		 * 删除Activiti User Group
//		 */
//		if (Boolean.parseBoolean(ActivitiPropertiesConfig.getInstance().getActivitiOpenFlag())) {
//			// 同步删除Activiti User
//			Map<Object, Object> userRoleRelMap = new HashMap<Object, Object>();
//			userRoleRelMap.put("userId", userId);
//			List<UserRoleRel> userRoleRelList = userRoleRelMapper.selectListByMap(userRoleRelMap);
//			for (UserRoleRel userRoleRel : userRoleRelList) {
//				processEngine.getIdentityService().deleteMembership(userRoleRel.getUserId(), userRoleRel.getRoleId());
//			}
//
//			// 同步删除Activiti User
//			processEngine.getIdentityService().deleteUser(userId.toString());
//		}
//
//	}
//}
