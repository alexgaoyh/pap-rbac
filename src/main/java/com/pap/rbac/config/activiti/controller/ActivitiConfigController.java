//package com.pap.rbac.config.activiti.controller;
//
//import java.util.List;
//import java.util.Map;
//
//import org.activiti.engine.HistoryService;
//import org.activiti.engine.ProcessEngine;
//import org.activiti.engine.RuntimeService;
//import org.activiti.engine.history.HistoricProcessInstance;
//import org.activiti.engine.history.HistoricTaskInstance;
//import org.activiti.engine.runtime.ProcessInstance;
//import org.activiti.engine.task.Comment;
//import org.activiti.engine.task.Task;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import com.pap.rbac.config.activiti.service.IActivitiAccountService;
//
///**
// * 操作审批流相关的逻辑
// * 	同步用户、用户组信息
// * 
// * 	开启流程，审批流程，查询相关流程的功能
// * @author alexgaoyh
// *
// */
//@RestController
//@RequestMapping("/v1/activiti/config")
//public class ActivitiConfigController {
//
//	private static Logger logger = LoggerFactory.getLogger(ActivitiConfigController.class);
//
//	@Autowired
//	private IActivitiAccountService activitiAccountService;
//
//	@Autowired
//	private ProcessEngine processEngine;
//
//	/**
//	 * 从RBAC模型中，同步用户，角色，用户角色关联数据到Activiti工作流中
//	 * @return
//	 * @throws Exception
//	 */
//	@RequestMapping(value = "/synAllUserAndRoleToActiviti")
//	public String synAllUserAndRoleToActiviti() throws Exception {
//		activitiAccountService.synAllUserAndRoleToActiviti();
//		return "success";
//	}
//
//	/**
//	 * 删除Activiti工作流中的相关数据，匹配操作 act_id_membership act_id_user act_id_group	三张表的数据
//	 * @return
//	 * @throws Exception
//	 */
//	@RequestMapping(value = "/deleteAllActivitiIdentifyData")
//	public String deleteAllActivitiIdentifyData() throws Exception {
//		activitiAccountService.deleteAllActivitiIdentifyData();
//		return "success";
//	}
//
//	/**
//	 * 启动流程 注意流程定义部分，在 pap-activitiy项目中的单元测试类中LineActivityProcessTest
//	 * 	启动线性操作的流程(line_process)，需要注意 businessKey 部分
//	 *   同时可以设定额外的variable到当前操作的task中
//	 * @return
//	 * @throws Exception
//	 */
//	@RequestMapping(value = "/startProcessInstance")
//	public String startProcessInstance() throws Exception {
//		String userId = "14693732217267200";
//		
//		RuntimeService runtimeService = processEngine.getRuntimeService();
//
//		// 设置流程发起用户信息
//		processEngine.getIdentityService().setAuthenticatedUserId(userId);
//		// businessKey a key that uniquely identifies the process instance in the context or the given process definition.
//		ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("line_process", "businessKey", null);
//		
//		// 调用setVariableLocal方法  
//        Task task = processEngine.getTaskService().createTaskQuery().processInstanceId(processInstance.getId()).singleResult();
//        processEngine.getTaskService().setVariableLocal(task.getId(), "userId", userId);  
//        processEngine.getTaskService().setVariableLocal(task.getId(), "extraParam", "{'name':'alexgaoyh','remark':'startProcessInstance'}");  
//		
//        return "success" + processInstance.getId();
//	}
//
//	/**
//	 * 查询指定角色的当前任务
//	 * 
//	 * @return
//	 * @throws Exception
//	 */
//	@RequestMapping(value = "/findMyRoleTask")
//	public String findMyRoleTask() throws Exception {
//		String candidateGroup = "财务经理";
//		List<Task> list = processEngine.getTaskService()// 与正在执行的任务管理相关的Service
//				.createTaskQuery()// 创建任务查询对象
//				/** 查询条件（where部分） */
//				.taskCandidateGroup(candidateGroup) // 任务办理组查询
//				/** 排序 */
//				.orderByTaskCreateTime().asc()// 使用创建时间的升序排列
//				/** 返回结果集 */
//				.list();// 返回列表
//		if (list != null && list.size() > 0) {
//			for (Task task : list) {
//				
//				String processVariablesStr = "";
//				task.getTaskLocalVariables();
//				Map<String, Object> processVariablesMap = processEngine.getTaskService().getVariables(task.getId());
//				for (Map.Entry<String, Object> entry : processVariablesMap.entrySet()) {
//					String key = entry.getKey().toString();
//					String value = entry.getValue().toString();
//					processVariablesStr += ("key=" + key + " value=" + value);
//				}
//				
//				System.out.println("任务ID:" + task.getId());
//				System.out.println("任务名称:" + task.getName());
//				System.out.println("任务的创建时间:" + task.getCreateTime());
//				System.out.println("任务的办理人:" + task.getAssignee());
//				System.out.println("流程实例ID：" + task.getProcessInstanceId());
//				System.out.println("执行对象ID:" + task.getExecutionId());
//				System.out.println("流程定义ID:" + task.getProcessDefinitionId());
//
//				System.out.println("获取流程变量:" + processVariablesStr);
//				System.out.println("########################################################");
//			}
//		}
//		return "success";
//	}
//
//	/**
//	 * 查询我的历史流程
//	 * 	并且查询批注信息： act_hi_comment 表结构
//	 * @return
//	 * @throws Exception
//	 */
//	@RequestMapping(value = "/getHistoricProcess")
//	public String getHistoricProcess() throws Exception {
//		String userId = "14693732217267200";
//		
//		HistoryService historyService = processEngine.getHistoryService();
//
//		List<HistoricProcessInstance> list = historyService.createHistoricProcessInstanceQuery()
//				// 业务键值, 需要在开启流程的过程中进行定义
//				.processInstanceBusinessKey("businessKey")
//				.startedBy(userId).list();
//		
//		if (list != null && list.size() > 0) {
//			for (HistoricProcessInstance historicProcessInstance : list) {
//				System.out.println("StartUserId:" + historicProcessInstance.getStartUserId());
//				System.out.println("ProcessDefinitionId:" + historicProcessInstance.getProcessDefinitionId());
//				
//				List<HistoricTaskInstance> htiList = historyService.createHistoricTaskInstanceQuery()//历史任务表查询
//						.processInstanceId(historicProcessInstance.getId())//使用流程实例ID查询
//						.list();
//				
//				//遍历集合，获取每个任务ID
//				if(htiList!=null && htiList.size()>0){
//					for(HistoricTaskInstance hti : htiList){
//						//任务ID
//						String htaskId = hti.getId();
//						//获取批注信息
//						List<Comment> taskCommentList = processEngine.getTaskService().getTaskComments(htaskId);//对用历史完成后的任务ID
//						if(taskCommentList != null && taskCommentList.size() > 0) {
//							for (Comment comment : taskCommentList) {
//								System.out.println("-------------" + comment.getFullMessage());
//							}
//						}
//					}
//				}
//			}
//		}
//		
//		return "success";
//	}
//	
//	/**
//	 * 根据当前角色，执行当前角色下面的任务
//	 * 	执行流程节点过程中，增加批注人和批注信息(注释) 部分的功能
//	 * 	在中间节点的执行过程中，可以通过 setVariableLocal 设定对应的参数信息
//	 * @param candidateGroup:  财务经理  CFO	CEO
//	 * @return
//	 * @throws Exception
//	 */
//	@RequestMapping(value = "/executeMyRoleTask/{candidateGroup}")
//	public String executeMyRoleTask(@PathVariable String candidateGroup) throws Exception {
//		// 这里的 candidateGroup currentOperUserId 两个地方，可以用来判断权限，用户编码和角色名称是否匹配
//		// 当前角色下面的某个用户
//		String currentOperUserId = "14693732217267200";
//		// 根据传入的用户组，并且增加 userId 的查询条件，查询当前用户组下面的代办任务
//		List<Task> list = processEngine.getTaskService()// 与正在执行的任务管理相关的Service
//				.createTaskQuery()// 创建任务查询对象
//				/** 查询条件（where部分） */
//				.taskCandidateGroup(candidateGroup) // 任务办理组查询
//				.taskVariableValueEquals("userId", currentOperUserId)// 增加额外参数的判断
//				/** 排序 */
//				.orderByTaskCreateTime().asc()// 使用创建时间的升序排列
//				/** 返回结果集 */
//				.list();// 返回列表
//		if (list != null && list.size() > 0) {
//			// 获取当前需要审批执行的任务
//			Task currentTask = list.get(0);
//			
//			// 增加批注人
//			processEngine.getIdentityService().setAuthenticatedUserId(currentOperUserId);
//			// 添加批注信息
//			processEngine.getTaskService().addComment(currentTask.getId(), null, "我是备注：" + candidateGroup + currentOperUserId); 
//	        // 完成任务
//			processEngine.getTaskService().complete(currentTask.getId(), null);
//			
//		
//			// 完成任务之后，重新设置变量数据， 调用setVariableLocal方法  
//	        Task nextTask = processEngine.getTaskService().createTaskQuery().processInstanceId(currentTask.getProcessInstanceId()).singleResult();
//	        // 如果没有任务，则上面当前审批流已经执行完毕
//	        if(nextTask != null) {
//	        	processEngine.getTaskService().setVariableLocal(nextTask.getId(), "userId", currentOperUserId);  
//	        	processEngine.getTaskService().setVariableLocal(nextTask.getId(), "extraParam", "{'operGroup':'"+ candidateGroup +"','remark':'executeMyRoleTask'}");  
//	        	return "success";
//	        }else {
//	        	return "success, and task is finished";
//	        }
//		} else {
//			return "no task";
//		}
//	}
//	
//}
