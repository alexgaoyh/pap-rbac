//package com.pap.rbac.config.activiti.service;
//
//import java.util.List;
//
//import com.pap.rbac.user.auto.entity.User;
//
///**
// * 维护用户、角色、权限接口 
// * @author alexgaoyh
// *
// */
//public interface IActivitiAccountService {
//	
//    /** 
//     * 添加用户并[同步其他数据库] 
//     * <ul> 
//     * <li>step 1: 保存系统用户，同时设置和部门的关系</li> 
//     * <li>step 2: 同步用户信息到activiti的identity.User，同时设置角色</li> 
//     * </ul> 
//     * 
//     * @param user              用户对象 
//     * @param orgId             部门ID 
//     * @param roleIds           角色ID集合 
//     * @throws  Exception                       其他未知异常 
//     */  
//    public void save(User user, String orgId, List<String> roleIds)  
//            throws Exception;  
//       
//    /** 
//     * 删除用户 
//     * @param userId        用户ID 
//     * @throws Exception 
//     */  
//    public void delete(String userId) throws Exception; 
//
//	/**
//	 * 同步用户、角色数据到工作流
//	 * 
//	 * @throws Exception
//	 */
//	public void synAllUserAndRoleToActiviti() throws Exception;
//
//	/**
//	 * 删除工作流引擎Activiti的用户、角色以及关系
//	 * 
//	 * @throws Exception
//	 */
//	public void deleteAllActivitiIdentifyData() throws Exception;
//
//}
