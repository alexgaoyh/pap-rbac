package com.pap.rbac.config.resolver;

import com.pap.base.annotation.ExtraParamsAnnotation;
import com.pap.base.annotation.vo.ExtraParam;
import com.pap.base.annotation.vo.ExtraParamsVO;
import com.pap.base.util.jackson.JacksonUtilss;
import com.pap.base.util.string.StringUtilss;
import org.springframework.core.MethodParameter;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @Auther: alexgaoyh
 * @Date: 2018/12/28 15:08
 * @Description:
 */
@Component
public class ExtraParamsArgumentsResolver implements HandlerMethodArgumentResolver {

    /**
     * 检查解析器是否支持解析该参数
     */
    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        if(
            //如果该参数注解有@Logined
                parameter.getParameterAnnotation(ExtraParamsAnnotation.class)!=null&&
                        //如果该参数的类型为User
                        parameter.getParameterType()== ExtraParamsVO.class
        ){
            //支持解析该参数
            return true;
        }
        return false;
    }

    @Override
    public Object resolveArgument(MethodParameter parameter,
                                  ModelAndViewContainer mavContainer, NativeWebRequest webRequest,
                                  WebDataBinderFactory binderFactory) throws Exception {

        ExtraParamsVO voList = new ExtraParamsVO();
        List<ExtraParam> details = new ArrayList<ExtraParam>();

        HttpServletRequest request= (HttpServletRequest) webRequest.getNativeRequest();
        String ExtraParam = request.getParameter("extraParams");
        if(StringUtilss.isNotEmpty(ExtraParam)) {
            Map<String, String> mapValues = JacksonUtilss.jsonToBean(ExtraParam, Map.class);
            if(!mapValues.isEmpty()) {
                Iterator<Map.Entry<String, String>> entries = mapValues.entrySet().iterator();
                while (entries.hasNext()) {
                    Map.Entry<String, String> entry = entries.next();
                    ExtraParam vo = new ExtraParam();
                    vo.setAttrKey(entry.getKey());
                    vo.setAttrValue(entry.getValue());
                    details.add(vo);
                }
            }

        }
        voList.setDetails(details);
        return voList;
    }
}
