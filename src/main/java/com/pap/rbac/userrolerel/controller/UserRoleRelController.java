package com.pap.rbac.userrolerel.controller;

import com.pap.obj.vo.logineduser.LoginedUser;
import com.pap.obj.vo.logineduser.LoginedUserVO;
import com.pap.obj.vo.response.ResponseVO;
import com.pap.rbac.dto.UserRoleRelDTO;
import com.pap.rbac.userrolerel.entity.UserRoleRel;
import com.pap.rbac.userrolerel.service.IUserRoleRelService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import java.util.List;


@RestController
@RequestMapping("/userRoleRel")
@Api(value = "用户角色管理", tags = "用户角色管理", description="用户角色管理")
public class UserRoleRelController {

	private static Logger logger  =  LoggerFactory.getLogger(UserRoleRelController.class);
	
	@Resource(name = "userRoleRelService")
	private IUserRoleRelService userRoleRelService;

	@ApiOperation("保存用户角色关系")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "userRoleRelDTO", value = "", required = true, dataType = "UserRoleRelDTO", paramType="body")
	})
	@PostMapping(value = "/saverels")
	public ResponseVO<UserRoleRel> saveRels(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
											@RequestHeader(value = "papVersion", defaultValue = "-1") String papVersion,
											@RequestBody UserRoleRelDTO userRoleRelDTO,
											@LoginedUser @ApiIgnore LoginedUserVO loginedUserVO) throws Exception {
		String clientLicenseId = loginedUserVO != null ? loginedUserVO.getClientLicenseId() : "";
		userRoleRelService.updateUserRoleRel(clientLicenseId, userRoleRelDTO.getUserId(), userRoleRelDTO.getRoleIds());
		return ResponseVO.successdata("操作成功");
	}

	@ApiOperation("根据用户查询角色信息")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "userId", value = "用户编号", required = true, dataType = "String", paramType="query")
	})
	@GetMapping(value = "/selectroleidsbyuserid")
	public ResponseVO<UserRoleRel> selectRoleIdsByUserId(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
														 @RequestHeader(value = "papVersion", defaultValue = "-1") String papVersion,
														 @RequestParam String userId) throws Exception {

		List<String> roleIdsList = userRoleRelService.selectUserRoleIds(userId);

		return ResponseVO.successdatas(roleIdsList, null);
	}

}
