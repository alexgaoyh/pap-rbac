package com.pap.rbac.userrolerel.service.impl;

import com.pap.base.util.date.DateUtils;
import com.pap.beans.idworker.IdWorker;
import com.pap.rbac.userrolerel.entity.UserRoleRel;
import com.pap.rbac.userrolerel.mapper.UserRoleRelMapper;
import com.pap.rbac.userrolerel.service.IUserRoleRelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Transactional
@Service("userRoleRelService")
public class UserRoleRelServiceImpl implements IUserRoleRelService {

    @Resource(name = "userRoleRelMapper")
    private UserRoleRelMapper mapper;

    @Autowired
    private UserRoleRelMapper userRoleRelMapper;

    @Resource(name = "idWorker")
    private IdWorker idWorker;

    @Override
    public void updateUserRoleRel(String clientLicenseId, String userId, List<String> roleLists) {
        userRoleRelMapper.deleteByUserId(userId);
        if(roleLists != null) {
            for (String string : roleLists) {
                UserRoleRel userRoleRel = new UserRoleRel();
                userRoleRel.setUserRoleId(idWorker.nextIdStr());
                userRoleRel.setUserId(userId);
                userRoleRel.setRoleId(string);
                userRoleRel.setCreateIp("0.0.0.0");
                userRoleRel.setCreateTime(DateUtils.getCurrDateTimeStr());
                userRoleRel.setCreateUser(userId);
                userRoleRel.setClientLicenseId(clientLicenseId);
                userRoleRelMapper.insertSelective(userRoleRel);
            }
        }
    }

    @Override
    public List<String> selectUserRoleIds(String userId) {
        List<String> returnRoleIdsList = new ArrayList<String>();
        Map<Object, Object> selectUserIdMap = new HashMap<Object, Object>();
        selectUserIdMap.put("userId", userId);
        List<UserRoleRel> userRoleRelList = userRoleRelMapper.selectListByMap(selectUserIdMap);
        if (userRoleRelList != null) {
            for (UserRoleRel userRoleRel : userRoleRelList) {
                returnRoleIdsList.add(userRoleRel.getRoleId());
            }
        }
        return returnRoleIdsList;
    }

    @Override
    public int deleteByPrimaryKey(String id) {
       return mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int selectCountByMap(Map<Object, Object> map) {
      return mapper.selectCountByMap(map);
    }

    @Override
    public List<UserRoleRel> selectListByMap(Map<Object, Object> map) {
        return mapper.selectListByMap(map);
    }

    @Override
    public int insert(UserRoleRel record) {
       return mapper.insert(record);
    }

    @Override
    public int insertSelective(UserRoleRel record) {
       return mapper.insertSelective(record);
    }

    @Override
    public UserRoleRel selectByPrimaryKey(String id) {
       return mapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(UserRoleRel record) {
       return mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(UserRoleRel record) {
      return mapper.updateByPrimaryKey(record);
    }
}