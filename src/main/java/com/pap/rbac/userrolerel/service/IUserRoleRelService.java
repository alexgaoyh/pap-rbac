package com.pap.rbac.userrolerel.service;

import com.pap.rbac.userrolerel.entity.UserRoleRel;

import java.util.List;
import java.util.Map;

public interface IUserRoleRelService {

    int deleteByPrimaryKey(String id);

    int selectCountByMap(Map<Object, Object> map);

    List<UserRoleRel> selectListByMap(Map<Object, Object> map);

    int insert(UserRoleRel record);

    int insertSelective(UserRoleRel record);

    UserRoleRel selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(UserRoleRel record);

    int updateByPrimaryKey(UserRoleRel record);

    /**
     * 根据用户 userId,更新最新的角色集合
     * @param clientLicenseId
     * @param userId
     * @param roleLists
     */
    void updateUserRoleRel(String clientLicenseId, String userId, List<String> roleLists);

    /**
     * 根据用户 userId ,将包含的角色集合进行返回
     * @param userId
     * @return
     */
    List<String> selectUserRoleIds(String userId);
}
