package com.pap.rbac.userrolerel.entity;

import com.pap.base.entity.PapBaseEntity;
import com.pap.base.mybatis.plugin.annotation.*;
import java.io.Serializable;

@MyBatisTableAnnotation(name = "t_rbac_user_role_rel", namespace = "com.pap.rbac.userrolerel.mapper.UserRoleRelMapper", remarks = " 修改点 ", aliasName = "t_rbac_user_role_rel t_rbac_user_role_rel" )
public class UserRoleRel extends PapBaseEntity implements Serializable {
    /**
     *  编号,所属表字段为t_rbac_user_role_rel.USER_ROLE_ID
     */
    @MyBatisColumnAnnotation(name = "USER_ROLE_ID", value = "t_rbac_user_role_rel_USER_ROLE_ID", chineseNote = "编号", tableAlias = "t_rbac_user_role_rel")
    @MyApiModelPropertyAnnotation(value = "编号")
    private String userRoleId;

    /**
     *  用户编号,所属表字段为t_rbac_user_role_rel.USER_ID
     */
    @MyBatisColumnAnnotation(name = "USER_ID", value = "t_rbac_user_role_rel_USER_ID", chineseNote = "用户编号", tableAlias = "t_rbac_user_role_rel")
    @MyApiModelPropertyAnnotation(value = "用户编号")
    private String userId;

    /**
     *  角色编号,所属表字段为t_rbac_user_role_rel.ROLE_ID
     */
    @MyBatisColumnAnnotation(name = "ROLE_ID", value = "t_rbac_user_role_rel_ROLE_ID", chineseNote = "角色编号", tableAlias = "t_rbac_user_role_rel")
    @MyApiModelPropertyAnnotation(value = "角色编号")
    private String roleId;

    /**
     *  角色编号路径集合,所属表字段为t_rbac_user_role_rel.ROLE_PATH_IDS
     */
    @MyBatisColumnAnnotation(name = "ROLE_PATH_IDS", value = "t_rbac_user_role_rel_ROLE_PATH_IDS", chineseNote = "角色编号路径集合", tableAlias = "t_rbac_user_role_rel")
    @MyApiModelPropertyAnnotation(value = "角色编号路径集合")
    private String rolePathIds;

    @Override
    public String getDynamicTableName() {
        return "t_rbac_user_role_rel";
    }

    private static final long serialVersionUID = 1L;

    public String getUserRoleId() {
        return userRoleId;
    }

    public void setUserRoleId(String userRoleId) {
        this.userRoleId = userRoleId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getRolePathIds() {
        return rolePathIds;
    }

    public void setRolePathIds(String rolePathIds) {
        this.rolePathIds = rolePathIds;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", userRoleId=").append(userRoleId);
        sb.append(", userId=").append(userId);
        sb.append(", roleId=").append(roleId);
        sb.append(", rolePathIds=").append(rolePathIds);
        sb.append("]");
        return sb.toString();
    }
}