package com.pap.rbac.userrolerel.mapper;

import com.pap.base.mapper.PapBaseMapper;
import com.pap.rbac.userrolerel.entity.UserRoleRel;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

;

@Mapper
public interface UserRoleRelMapper extends PapBaseMapper<UserRoleRel> {
    int deleteByPrimaryKey(String userRoleId);

    int selectCountByMap(Map<Object, Object> map);

    List<UserRoleRel> selectListByMap(Map<Object, Object> map);

    UserRoleRel selectByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int deleteByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int insert(UserRoleRel record);

    int insertSelective(UserRoleRel record);

    UserRoleRel selectByPrimaryKey(String userRoleId);

    int updateByPrimaryKeySelective(UserRoleRel record);

    int updateByPrimaryKey(UserRoleRel record);


    // add alexgaoyh

    int deleteByUserId(String userId);

    /**
     * 根据用户编号，查询包含哪些角色
     * @param userId    用户编号
     * @return
     */
    List<String> selectRoleIdsByUserId(String userId);
}