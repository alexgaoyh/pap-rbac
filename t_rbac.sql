/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50724
Source Host           : 127.0.0.1:3306
Source Database       : cf

Target Server Type    : MYSQL
Target Server Version : 50724
File Encoding         : 65001

Date: 2018-12-27 12:17:58
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for t_rbac_data_clazz
-- ----------------------------
DROP TABLE IF EXISTS `t_rbac_data_clazz`;
CREATE TABLE `t_rbac_data_clazz` (
  `DATA_CLAZZ_ID` varchar(32) NOT NULL COMMENT '编号',
  `DATA_CLAZZ_CODE` varchar(32) DEFAULT NULL COMMENT '编码',
  `DATA_PROJECT_ID` varchar(255) DEFAULT NULL COMMENT '所属项目编号',
  `DATA_MODULE_ID` varchar(255) DEFAULT NULL COMMENT '所属模块编号',
  `DATA_CLAZZ_NAME` varchar(300) DEFAULT NULL COMMENT '名称',
  `DATA_CLAZZ_ALAIS_NAME` varchar(255) DEFAULT NULL COMMENT '别名',
  `DATA_CLAZZ_PATH` varchar(255) DEFAULT NULL COMMENT '类全路径',
  `DISABLE_FLAG` varchar(32) DEFAULT NULL COMMENT '可用状态标识',
  `DELETE_FLAG` varchar(32) DEFAULT NULL COMMENT '删除状态标识',
  `REMARK` varchar(32) DEFAULT NULL COMMENT '备注',
  `CREATE_USER` varchar(32) DEFAULT NULL,
  `CREATE_TIME` varchar(32) DEFAULT NULL,
  `CREATE_IP` varchar(32) DEFAULT NULL,
  `MODIFY_USER` varchar(32) DEFAULT NULL,
  `MODIFY_TIME` varchar(32) DEFAULT NULL,
  `MODIFY_IP` varchar(32) DEFAULT NULL,
  `CLIENT_LICENSE_ID` varchar(32) DEFAULT NULL COMMENT '授权标示',
  PRIMARY KEY (`DATA_CLAZZ_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='数据权限-所属类文件';

-- ----------------------------
-- Table structure for t_rbac_data_field
-- ----------------------------
DROP TABLE IF EXISTS `t_rbac_data_field`;
CREATE TABLE `t_rbac_data_field` (
  `DATA_FIELD_ID` varchar(32) NOT NULL COMMENT '编号',
  `DATA_FIELD_CODE` varchar(32) DEFAULT NULL COMMENT '编码',
  `DATA_PROJECT_ID` varchar(255) DEFAULT NULL COMMENT '所属项目编号',
  `DATA_MODULE_ID` varchar(255) DEFAULT NULL COMMENT '所属模块编号',
  `DATA_CLAZZ_ID` varchar(255) DEFAULT NULL COMMENT '所属类路径编号',
  `DATA_FIELD_NAME` varchar(300) DEFAULT NULL COMMENT '属性名称',
  `DATA_FIELD_VALUE` varchar(255) DEFAULT NULL COMMENT '属性值',
  `DISABLE_FLAG` varchar(32) DEFAULT NULL COMMENT '可用状态标识',
  `DELETE_FLAG` varchar(32) DEFAULT NULL COMMENT '删除状态标识',
  `REMARK` varchar(32) DEFAULT NULL COMMENT '备注',
  `CREATE_USER` varchar(32) DEFAULT NULL,
  `CREATE_TIME` varchar(32) DEFAULT NULL,
  `CREATE_IP` varchar(32) DEFAULT NULL,
  `MODIFY_USER` varchar(32) DEFAULT NULL,
  `MODIFY_TIME` varchar(32) DEFAULT NULL,
  `MODIFY_IP` varchar(32) DEFAULT NULL,
  `CLIENT_LICENSE_ID` varchar(32) DEFAULT NULL COMMENT '授权标示',
  PRIMARY KEY (`DATA_FIELD_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for t_rbac_data_module
-- ----------------------------
DROP TABLE IF EXISTS `t_rbac_data_module`;
CREATE TABLE `t_rbac_data_module` (
  `DATA_MODULE_ID` varchar(32) NOT NULL COMMENT '编号',
  `DATA_MODULE_CODE` varchar(32) DEFAULT NULL COMMENT '编码',
  `DATA_PROJECT_ID` varchar(255) DEFAULT NULL COMMENT '所属项目编号',
  `DATA_MODULE_NAME` varchar(300) DEFAULT NULL COMMENT '姓名',
  `DATA_MODULE__DOMAIN` varchar(255) DEFAULT NULL COMMENT '模块域名前缀',
  `DATA_MODULE_MANAGER` varchar(300) DEFAULT NULL COMMENT '管理人员',
  `DATA_MODULE_MOBILE` varchar(11) DEFAULT NULL COMMENT '管理人员电话',
  `DISABLE_FLAG` varchar(32) DEFAULT NULL COMMENT '可用状态标识',
  `DELETE_FLAG` varchar(32) DEFAULT NULL COMMENT '删除状态标识',
  `REMARK` varchar(32) DEFAULT NULL COMMENT '备注',
  `CREATE_USER` varchar(32) DEFAULT NULL,
  `CREATE_TIME` varchar(32) DEFAULT NULL,
  `CREATE_IP` varchar(32) DEFAULT NULL,
  `MODIFY_USER` varchar(32) DEFAULT NULL,
  `MODIFY_TIME` varchar(32) DEFAULT NULL,
  `MODIFY_IP` varchar(32) DEFAULT NULL,
  `CLIENT_LICENSE_ID` varchar(32) DEFAULT NULL COMMENT '授权标示',
  PRIMARY KEY (`DATA_MODULE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='数据权限-所属模块';

-- ----------------------------
-- Table structure for t_rbac_data_project
-- ----------------------------
DROP TABLE IF EXISTS `t_rbac_data_project`;
CREATE TABLE `t_rbac_data_project` (
  `DATA_PROJECT_ID` varchar(32) CHARACTER SET utf8 NOT NULL COMMENT '编号',
  `DATA_PROJECT_CODE` varchar(32) CHARACTER SET utf8 DEFAULT NULL COMMENT '编码',
  `DATA_PROJECT_NAME` varchar(300) CHARACTER SET utf8 DEFAULT NULL COMMENT '名称',
  `DATA_PROJECT_DOMAIN` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '项目域名',
  `DATA_PROJECT_MANAGER` varchar(300) CHARACTER SET utf8 DEFAULT NULL COMMENT '管理人员',
  `DATA_PROJECT_MOBILE` varchar(11) CHARACTER SET utf8 DEFAULT NULL COMMENT '电话',
  `DISABLE_FLAG` varchar(32) CHARACTER SET utf8 DEFAULT NULL COMMENT '可用状态标识',
  `DELETE_FLAG` varchar(32) CHARACTER SET utf8 DEFAULT NULL COMMENT '删除状态标识',
  `REMARK` varchar(32) CHARACTER SET utf8 DEFAULT NULL COMMENT '备注',
  `CREATE_USER` varchar(32) CHARACTER SET utf8 DEFAULT NULL,
  `CREATE_TIME` varchar(32) CHARACTER SET utf8 DEFAULT NULL,
  `CREATE_IP` varchar(32) CHARACTER SET utf8 DEFAULT NULL,
  `MODIFY_USER` varchar(32) CHARACTER SET utf8 DEFAULT NULL,
  `MODIFY_TIME` varchar(32) CHARACTER SET utf8 DEFAULT NULL,
  `MODIFY_IP` varchar(32) CHARACTER SET utf8 DEFAULT NULL,
  `CLIENT_LICENSE_ID` varchar(32) CHARACTER SET utf8 DEFAULT NULL COMMENT '授权标示',
  PRIMARY KEY (`DATA_PROJECT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='数据权限-所属项目';

-- ----------------------------
-- Table structure for t_rbac_data_user_field_prop
-- ----------------------------
DROP TABLE IF EXISTS `t_rbac_data_user_field_prop`;
CREATE TABLE `t_rbac_data_user_field_prop` (
  `DATA_USER_FIELD_ID` varchar(32) NOT NULL COMMENT '编号',
  `DATA_USER_FIELD_CODE` varchar(32) DEFAULT NULL COMMENT '编码',
  `USER_ID` varchar(255) DEFAULT NULL COMMENT '所属用户',
  `DATA_PROJECT_ID` varchar(255) DEFAULT NULL COMMENT '所属项目编号',
  `DATA_MODULE_ID` varchar(255) DEFAULT NULL COMMENT '所属模块编号',
  `DATA_CLAZZ_ID` varchar(255) DEFAULT NULL COMMENT '所属类路径编号',
  `DATA_FIELD_ID` varchar(255) DEFAULT NULL COMMENT '所属权限字段',
  `PROP_OPER` varchar(255) DEFAULT NULL COMMENT '属性操作符',
  `PROP_VALUE` varchar(255) DEFAULT NULL COMMENT '属性值',
  `REMARK` varchar(32) DEFAULT NULL COMMENT '备注',
  `CREATE_USER` varchar(32) DEFAULT NULL,
  `CREATE_TIME` varchar(32) DEFAULT NULL,
  `CREATE_IP` varchar(32) DEFAULT NULL,
  `MODIFY_USER` varchar(32) DEFAULT NULL,
  `MODIFY_TIME` varchar(32) DEFAULT NULL,
  `MODIFY_IP` varchar(32) DEFAULT NULL,
  `CLIENT_LICENSE_ID` varchar(32) DEFAULT NULL COMMENT '授权标示',
  PRIMARY KEY (`DATA_USER_FIELD_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for t_rbac_organization
-- ----------------------------
DROP TABLE IF EXISTS `t_rbac_organization`;
CREATE TABLE `t_rbac_organization` (
  `ORGANIZATION_ID` varchar(32) NOT NULL COMMENT '编号，主键',
  `ORGANIZATION_CODE` varchar(32) DEFAULT NULL COMMENT '业务编码',
  `ORGANIZATION_NAME` varchar(32) DEFAULT NULL COMMENT '名称',
  `ORGANIZATION_PARENT_ID` varchar(32) DEFAULT NULL COMMENT '父级节点编号',
  `PATH_IDS` varchar(500) DEFAULT NULL COMMENT '编号路径集合',
  `PATH_CODES` varchar(500) DEFAULT NULL COMMENT '业务编码路径集合',
  `PATH_NAMES` varchar(1000) DEFAULT NULL COMMENT '名称路径集合',
  `PROVINCE_ID` varchar(32) DEFAULT NULL COMMENT '省份编号',
  `PROVINCE_NAME` varchar(32) DEFAULT NULL COMMENT '省份名称',
  `CITY_ID` varchar(32) DEFAULT NULL COMMENT '市级编号',
  `CITY_NAME` varchar(32) DEFAULT NULL COMMENT '市级名称',
  `AREA_ID` varchar(32) DEFAULT NULL COMMENT '区域编号',
  `AREA_NAME` varchar(32) DEFAULT NULL COMMENT '区域名称',
  `DETAIL_ADDRESS` varchar(300) DEFAULT NULL COMMENT '详细地址',
  `DISABLE_FLAG` varchar(32) DEFAULT NULL COMMENT '可用状态标示',
  `REMARK` varchar(500) DEFAULT NULL COMMENT '备注字段',
  `CREATE_USER` varchar(32) DEFAULT NULL COMMENT '创建人',
  `CREATE_TIME` varchar(32) DEFAULT NULL COMMENT '创建日期',
  `CREATE_IP` varchar(32) DEFAULT NULL COMMENT '创建人IP地址',
  `MODIFY_USER` varchar(32) DEFAULT NULL COMMENT '修改人',
  `MODIFY_TIME` varchar(32) DEFAULT NULL COMMENT '修改日期',
  `MODIFY_IP` varchar(32) DEFAULT NULL COMMENT '修改人IP',
  `CLIENT_LICENSE_ID` varchar(32) DEFAULT NULL COMMENT '授权标示',
  PRIMARY KEY (`ORGANIZATION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='组织机构';

-- ----------------------------
-- Table structure for t_rbac_resource
-- ----------------------------
DROP TABLE IF EXISTS `t_rbac_resource`;
CREATE TABLE `t_rbac_resource` (
  `RESOURCE_ID` varchar(32) NOT NULL COMMENT '编号',
  `RESOURCE_CODE` varchar(32) DEFAULT NULL COMMENT '编码',
  `RESOURCE_NAME` varchar(32) DEFAULT NULL COMMENT '名称',
  `RESOURCE_PARENT_ID` varchar(32) DEFAULT NULL COMMENT '父亲编号',
  `TYPE_FLAG` varchar(32) DEFAULT NULL COMMENT '资源类型 菜单 按钮',
  `HREF` varchar(300) DEFAULT NULL COMMENT '链接',
  `COMPONENT` varchar(100) DEFAULT NULL COMMENT '资源文件路径',
  `ORDER_NO` varchar(32) DEFAULT NULL COMMENT '排序号',
  `ICON` varchar(32) DEFAULT NULL COMMENT '图标',
  `HIDE_FLAG` varchar(32) DEFAULT NULL COMMENT '隐藏状态标识',
  `REMARK` varchar(32) DEFAULT NULL COMMENT '备注',
  `LEVEL` varchar(255) DEFAULT NULL COMMENT '层级',
  `STATUS` varchar(255) DEFAULT NULL COMMENT '状态(Y启用;N停用)',
  `INNER_FLAG` varchar(32) DEFAULT NULL COMMENT '内部系统标识',
  `PATH_IDS` varchar(1000) DEFAULT NULL COMMENT '编号路径集合',
  `PATH_CODES` varchar(1000) DEFAULT NULL COMMENT '编码路径集合',
  `PATH_NAMES` varchar(1000) DEFAULT NULL COMMENT '名称路径集合',
  `CREATE_USER` varchar(32) DEFAULT NULL COMMENT '创建人',
  `CREATE_TIME` varchar(32) DEFAULT NULL COMMENT '创建时间',
  `CREATE_IP` varchar(32) DEFAULT NULL COMMENT '创建地址',
  `MODIFY_USER` varchar(32) DEFAULT NULL COMMENT '修改人',
  `MODIFY_TIME` varchar(32) DEFAULT NULL COMMENT '修改时间',
  `MODIFY_IP` varchar(32) DEFAULT NULL COMMENT '修改地址',
  `CLIENT_LICENSE_ID` varchar(32) DEFAULT NULL COMMENT '授权标识',
  PRIMARY KEY (`RESOURCE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for t_rbac_role
-- ----------------------------
DROP TABLE IF EXISTS `t_rbac_role`;
CREATE TABLE `t_rbac_role` (
  `ROLE_ID` varchar(32) NOT NULL COMMENT '编号',
  `ROLE_CODE` varchar(32) DEFAULT NULL COMMENT '编码',
  `ROLE_NAME` varchar(32) DEFAULT NULL COMMENT '名称',
  `ROLE_PARENT_ID` varchar(32) DEFAULT NULL COMMENT '父节点编号',
  `PATH_IDS` varchar(1000) DEFAULT NULL COMMENT '编号上级路径',
  `PATH_CODES` varchar(1000) DEFAULT NULL COMMENT '编码上级路径',
  `PATH_NAMES` varchar(1000) DEFAULT NULL COMMENT '名称上级路径',
  `LEVEL` varchar(255) DEFAULT NULL COMMENT '层级',
  `STATUS` varchar(255) DEFAULT NULL COMMENT '状态(Y启用;N停用)',
  `REMARK` varchar(32) DEFAULT NULL COMMENT '备注',
  `CREATE_USER` varchar(32) DEFAULT NULL COMMENT '创建人',
  `CREATE_TIME` varchar(32) DEFAULT NULL COMMENT '创建时间',
  `CREATE_IP` varchar(32) DEFAULT NULL COMMENT '创建地址',
  `MODIFY_USER` varchar(32) DEFAULT NULL COMMENT '修改人',
  `MODIFY_TIME` varchar(32) DEFAULT NULL COMMENT '修改时间',
  `MODIFY_IP` varchar(32) DEFAULT NULL COMMENT '修改地址',
  `CLIENT_LICENSE_ID` varchar(32) DEFAULT NULL COMMENT '授权标识',
  PRIMARY KEY (`ROLE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for t_rbac_role_resource_rel
-- ----------------------------
DROP TABLE IF EXISTS `t_rbac_role_resource_rel`;
CREATE TABLE `t_rbac_role_resource_rel` (
  `ROLE_RESOURCE_ID` varchar(32) NOT NULL COMMENT '编号',
  `ROLE_ID` varchar(32) DEFAULT NULL COMMENT '角色编号',
  `RESOURCE_ID` varchar(32) DEFAULT NULL COMMENT '资源编号',
  `RESOURCE_PATH_IDS` varchar(32) DEFAULT NULL COMMENT '资源编号路径',
  `CREATE_USER` varchar(32) DEFAULT NULL COMMENT '创建人',
  `CREATE_TIME` varchar(32) DEFAULT NULL COMMENT '创建时间',
  `CREATE_IP` varchar(32) DEFAULT NULL COMMENT '创建地址',
  `MODIFY_USER` varchar(32) DEFAULT NULL COMMENT '修改人',
  `MODIFY_TIME` varchar(32) DEFAULT NULL COMMENT '修改时间',
  `MODIFY_IP` varchar(32) DEFAULT NULL COMMENT '修改地址',
  `CLIENT_LICENSE_ID` varchar(32) DEFAULT NULL COMMENT '授权标识',
  PRIMARY KEY (`ROLE_RESOURCE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for t_rbac_user
-- ----------------------------
DROP TABLE IF EXISTS `t_rbac_user`;
CREATE TABLE `t_rbac_user` (
  `USER_ID` varchar(32) NOT NULL COMMENT '编号',
  `USER_CODE` varchar(32) DEFAULT NULL COMMENT '编码',
  `USER_NAME` varchar(300) DEFAULT NULL COMMENT '姓名',
  `EMAIL` varchar(300) DEFAULT NULL COMMENT '邮箱',
  `USER_LOGIN_NAME` varchar(300) DEFAULT NULL COMMENT '登录名',
  `USER_PASSWORDS` varchar(300) DEFAULT NULL COMMENT '密码',
  `MOBILE` varchar(11) DEFAULT NULL COMMENT '电话',
  `ORGANIZATION_ID` varchar(32) DEFAULT NULL COMMENT '所属组织机构',
  `DISABLE_FLAG` varchar(32) DEFAULT NULL COMMENT '可用状态标识',
  `DELETE_FLAG` varchar(32) DEFAULT NULL COMMENT '删除状态标识',
  `ICON` varchar(32) DEFAULT NULL COMMENT '图标',
  `LAST_LOGIN_TIME` varchar(32) DEFAULT NULL COMMENT '最近一次登录时间',
  `REMARK` varchar(32) DEFAULT NULL COMMENT '备注',
  `CREATE_USER` varchar(32) DEFAULT NULL,
  `CREATE_TIME` varchar(32) DEFAULT NULL,
  `CREATE_IP` varchar(32) DEFAULT NULL,
  `MODIFY_USER` varchar(32) DEFAULT NULL,
  `MODIFY_TIME` varchar(32) DEFAULT NULL,
  `MODIFY_IP` varchar(32) DEFAULT NULL,
  `USER_TYPE` varchar(32) DEFAULT NULL COMMENT '用户类型',
  `CLIENT_LICENSE_ID` varchar(32) DEFAULT NULL COMMENT '授权标示',
  PRIMARY KEY (`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for t_rbac_user_extra_prop
-- ----------------------------
DROP TABLE IF EXISTS `t_rbac_user_extra_prop`;
CREATE TABLE `t_rbac_user_extra_prop` (
  `USER_EXTRA_ID` varchar(32) NOT NULL COMMENT '编号',
  `USER_EXTRA_CODE` varchar(32) DEFAULT NULL COMMENT '编码',
  `USER_ID` varchar(255) DEFAULT NULL COMMENT '所属用户',
  `PROP_KEY` varchar(255) DEFAULT NULL COMMENT '属性键',
  `PROP_OPER` varchar(255) DEFAULT NULL COMMENT '属性操作符',
  `PROP_VALUE` varchar(255) DEFAULT NULL COMMENT '属性值',
  `REMARK` varchar(32) DEFAULT NULL COMMENT '备注',
  `CREATE_USER` varchar(32) DEFAULT NULL,
  `CREATE_TIME` varchar(32) DEFAULT NULL,
  `CREATE_IP` varchar(32) DEFAULT NULL,
  `MODIFY_USER` varchar(32) DEFAULT NULL,
  `MODIFY_TIME` varchar(32) DEFAULT NULL,
  `MODIFY_IP` varchar(32) DEFAULT NULL,
  `USER_TYPE` varchar(32) DEFAULT NULL COMMENT '用户类型',
  `CLIENT_LICENSE_ID` varchar(32) DEFAULT NULL COMMENT '授权标示',
  PRIMARY KEY (`USER_EXTRA_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for t_rbac_user_role_rel
-- ----------------------------
DROP TABLE IF EXISTS `t_rbac_user_role_rel`;
CREATE TABLE `t_rbac_user_role_rel` (
  `USER_ROLE_ID` varchar(32) NOT NULL COMMENT '编号',
  `USER_ID` varchar(32) DEFAULT NULL COMMENT '用户编号',
  `ROLE_ID` varchar(32) DEFAULT NULL COMMENT '角色编号',
  `ROLE_PATH_IDS` varchar(32) DEFAULT NULL COMMENT '角色编号路径集合',
  `CREATE_USER` varchar(32) DEFAULT NULL COMMENT '创建人',
  `CREATE_TIME` varchar(32) DEFAULT NULL COMMENT '创建时间',
  `CREATE_IP` varchar(32) DEFAULT NULL COMMENT '创建地址',
  `MODIFY_USER` varchar(32) DEFAULT NULL COMMENT '修改人',
  `MODIFY_TIME` varchar(32) DEFAULT NULL COMMENT '修改时间',
  `MODIFY_IP` varchar(32) DEFAULT NULL COMMENT '修改地址',
  `CLIENT_LICENSE_ID` varchar(32) DEFAULT NULL COMMENT '授权标识',
  PRIMARY KEY (`USER_ROLE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
